#!/usr/bin/perl -w 
# **************************************
# * ---------------------------------- *
# * Douglas Eduardo Valente Pires      *
# * douglas.pires@minas.fiocruz.br     *
# * Last modification :: 20/06/2018    *
# * ---------------------------------- *
# **************************************

use strict;
use warnings;

# ____________________________________________________________________________________________________________________
# Input parameters
my $infile_mut = $ARGV[0];

if(scalar(@ARGV) != 1){
print "___________________________________________________________________________________
SINTAX:
        perl run_arpeggio_mutation.pl <mutation_file>
___________________________________________________________________________________\n";
        exit;
}

# ____________________________________________________________________________________________________________________

open(MUT,"<$infile_mut") or die "$!Erro ao abrir: $infile_mut\n";
my @mut = <MUT>;
close MUT;


# -------------------------------------------------------------------------------------------
# Parse header
my $header = shift @mut;
chomp($header);

my @columns = split("\t", $header);
my %col_dict;

for(my$i=0; $i<scalar(@columns); $i++){
        $col_dict{$columns[$i]} = $i;
}

print "Clash,Covalent,VdWClash,VdW,Proximal,HydrogenBond,WeakHydrogenBond,HalogenBond,Ionic,MetalComplex,Aromatic,Hydrophobic,Carbonyl,Polar,WeakPolar,PI-PI,Carbon-PI,Cation-PI,Donor-PI,Halogen-PI,MetalSulphur-PI,Amide-Amide,Amide-Ring\n";

foreach my $line (@mut){
        chomp($line);
        my @tokens = split("\t", $line);
        
        my $pdb = $tokens[$col_dict{"PDB"}];
	my $mutation = $tokens[$col_dict{"MUTATION"}];
	my $chain = $tokens[$col_dict{"CHAIN"}];
	
        my $wild_res = substr($mutation,0,1);
        my $pos = substr($mutation,1,length($mutation)-2);
        my $mutated_res = substr($mutation,-1,1);

        # -----------------------------------------------------------------------------------------------------------
        # Run arpeggio

        my $contact_file = $pdb;
        my $atom_ring_file = $pdb;
        my $ring_ring_file = $pdb;
        my $amide_amide_file = $pdb;
        my $amide_ring_file = $pdb;
        
        $contact_file =~ s/pdb$/$mutation.contacts/g;
        $atom_ring_file =~ s/pdb$/$mutation.ari/g;
        $ring_ring_file =~ s/pdb$/$mutation.ri/g;
        $amide_amide_file =~ s/pdb$/$mutation.amam/g;
        $amide_ring_file =~ s/pdb$/$mutation.amri/g;
        
        if(!(-e $contact_file)){
                system("python calc_arpeggio/arpeggio.py -s /$chain/$pos/ -wh -ph 7.4 $pdb -op .$mutation");
        }

        # ------------------------------------------------------------------------------------------------------------
        # Parse contacts
        open(CONTACTS,"<$contact_file") or die "$!Erro ao abrir: $contact_file\n";
        my @contacts = <CONTACTS>;
        close CONTACTS;

        my @contact_count;
        undef @contact_count;

        foreach my $line (@contacts){
                chomp($line);
                
                my @tokens = split(/\t/, $line);
                my $atom1 = shift(@tokens);
                my $atom2 = shift(@tokens);
                my $type = pop(@tokens);
                
                for(my $i=0; $i<scalar(@tokens); $i++){
                        $contact_count[$i] += $tokens[$i];			
                }
        }
        
        for(my $i=0; $i<=$#contact_count; $i++){
                print $contact_count[$i], ",";
        }

        # ------------------------------------------------------------------------------------------------------------
        # Parse pi-pi interactions
        my $pi_pi = 0;
        if(-e $ring_ring_file){
                $pi_pi = `cut -f2,5 $ring_ring_file | grep \"$chain/$pos/\" | sed \"s/$chain\\/$pos\\///g\" | sed \"s/\\t//g\" | sort -u | wc -l`;
                chomp($pi_pi);
        }
                
        print "$pi_pi,";
        
        # ------------------------------------------------------------------------------------------------------------
        # Parse atom-pi interactions
        
        my $carbon_pi = 0;
        my $cation_pi = 0;
        my $donor_pi = 0;
        my $halogen_pi = 0;
        my $metalsulphur_pi = 0;
        
        if(-e $atom_ring_file){
                $carbon_pi = `cut -f1,3,5 $atom_ring_file  | grep CARBONPI | grep \"$chain/$pos/\" | cut -f1,2 | sed \"s/\\t/\\//g\" | cut -d\"/\" -f1,2,4,5 | sed \"s/$chain\\/$pos//g\" | sed \"s/\\///g\" | sort -u | wc -l`;
                $cation_pi = `cut -f1,3,5 $atom_ring_file  | grep CATIONPI | grep \"$chain/$pos/\" | cut -f1,2 | sed \"s/\\t/\\//g\" | cut -d\"/\" -f1,2,4,5 | sed \"s/$chain\\/$pos//g\" | sed \"s/\\///g\" | sort -u | wc -l`;
                $donor_pi = `cut -f1,3,5 $atom_ring_file  | grep DONORPI | grep \"$chain/$pos/\" | cut -f1,2 | sed \"s/\\t/\\//g\" | cut -d\"/\" -f1,2,4,5 | sed \"s/$chain\\/$pos//g\" | sed \"s/\\///g\" | sort -u | wc -l`;
                $halogen_pi = `cut -f1,3,5 $atom_ring_file  | grep HALOGENPI | grep \"$chain/$pos/\" | cut -f1,2 | sed \"s/\\t/\\//g\" | cut -d\"/\" -f1,2,4,5 | sed \"s/$chain\\/$pos//g\" | sed \"s/\\///g\" | sort -u | wc -l`;
                $metalsulphur_pi = `cut -f1,3,5 $atom_ring_file  | grep METSULPHURPI | grep \"$chain/$pos/\" | cut -f1,2 | sed \"s/\\t/\\//g\" | cut -d\"/\" -f1,2,4,5 | sed \"s/$chain\\/$pos//g\" | sed \"s/\\///g\" | sort -u | wc -l`;
                
                chomp($carbon_pi);
                chomp($cation_pi);
                chomp($donor_pi);
                chomp($halogen_pi);
                chomp($metalsulphur_pi);
                
        }
        
        print "$carbon_pi,$cation_pi,$donor_pi,$halogen_pi,$metalsulphur_pi,";
        
        # ------------------------------------------------------------------------------------------------------------
        # Parse amide-amide interactions
        my $amide_amide = 0;
        
        if(-e $amide_amide_file){
                $amide_amide = `cut -f2,5 $amide_amide_file  | grep \"$chain/$pos/\" | sed \"s/$chain\\/$pos\\///g\" | sed \"s/\\t//g\" | sort -u | wc -l`;
                chomp($amide_amide);
        }
        
        print "$amide_amide,";
        
        # ------------------------------------------------------------------------------------------------------------
        # Parse amide-ring interactions
        my $amide_ring = 0;
        
        if(-e $amide_ring_file){
                $amide_ring = `cut -f2,5 $amide_ring_file  | grep \"$chain/$pos/\" | sed \"s/$chain\\/$pos\\///g\" | sed \"s/\\t//g\" | sort -u | wc -l`;
                chomp($amide_ring);
        }
        
        print "$amide_ring\n";
}
exit;

#----------------------------------------------------------------------------------------
