# ********************************************************
# *               University of Melborune                *
# *   ----------------------------------------------     *
# * Yoochan Myung - ymyung@student.unimelb.edu.au        *
# * Last modification :: 08/04/2019                      *
# *   ----------------------------------------------     *
# ********************************************************

import os
import csv
import pandas as pd
import json
import sys

infile = sys.argv[1]
outfile = sys.argv[2]
area_interest = sys.argv[3] # 'INTER' or 'ALL'
remove_redundant = sys.argv[4] # 'True' or 'False' - removes redundant information in Amide-Amide and Ring-Ring interactions (may be repetitive because they interact with each other)

## infile example ## (requires same column names as follows)
###############################################################
# wild_contacts,mutant_contacts_file
# wild_A271K.contacts,mut_A271K.contacts
###############################################################

## [ATOM#1,ATOM#2,clash,covalent,vdwcl,vdw,proximal,Hbond,weakHbond,Halogen,Ionic,Metal,Aromatic,Hydrophobic,Carbonyl,Polar,WeakPolar]
# polar group: 7,8,9,10,15,16
# aromatic group: 12,14

def getTwoGroupedInt(input_file, condition):
    result = []

    # group1
    Hbond = []
    WeakHbond = []
    Halogen = []
    Ionic = []
    Polar = []
    WeakPolar = []

    # group2
    Aromatic = []
    Carbonyl = []

    if condition != 'ALL':
        with open(input_file, 'r') as input:
            for each in input.readlines():
                striped = each.strip().split('\t')

                if striped[17] == 'INTER':
                    Hbond.append(int(striped[7]))
                    WeakHbond.append(int(striped[8]))
                    Halogen.append(int(striped[9]))
                    Ionic.append(int(striped[10]))
                    Polar.append(int(striped[15]))
                    WeakPolar.append(int(striped[16]))

                    Aromatic.append(int(striped[12]))
                    Carbonyl.append(int(striped[14]))

                group1 = sum(Hbond) + sum(WeakHbond) + sum(Halogen) + sum(Ionic) + sum(Polar) + sum(WeakPolar)
                group2 = sum(Aromatic) + sum(Carbonyl)
                filename = input_file.split('/')[-1]

                result = [filename, group1, group2]


    else:

        with open(input_file, 'r') as input:
            for each in input.readlines():
                striped = each.strip().split('\t')
                Hbond.append(int(striped[7]))
                WeakHbond.append(int(striped[8]))
                Halogen.append(int(striped[9]))
                Ionic.append(int(striped[10]))
                Polar.append(int(striped[15]))
                WeakPolar.append(int(striped[16]))

                Aromatic.append(int(striped[12]))
                Carbonyl.append(int(striped[14]))

        group1 = sum(Hbond) + sum(WeakHbond) + sum(Halogen) + sum(Ionic) + sum(Polar) + sum(WeakPolar)
        group2 = sum(Aromatic) + sum(Carbonyl)
        filename = input_file.split('/')[-1]
        result = [filename, group1, group2]

    return result


def getTwoGroupedInt_Folder(input_path, condition):
    output_path = input_path + '/Arpeggio_Contacts_Analysis_result'

    result = [['filename', 'group1', 'group2']]

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    for fname in os.listdir(input_path):

        if not fname.startswith('.') and fname.__contains__('.contacts'):
            contacts_path = os.path.join(input_path, fname)

            result.append(getTwoGroupedInt(contacts_path, condition))

    output = output_path + '/' + 'arpeggio_result.csv'
    with open(output, 'w') as outputcontacts:
        wr = csv.writer(outputcontacts)
        wr.writerows(result)
        outputcontacts.close()

    return "Your Job is done!"

def roundupList(given_list):
    result_list=[]
    for each in given_list.split(','):

        result_list.append(round(float(each),3))

    return ','.join(str(result_list)[1:-1].strip('\"').split(','))

def getALLPI(pdb_file, area_interest, remove_redundant): # for getttinf all PI related results
    # Files
    ari_file=pdb_file[:-3]+"ari" # for carbonpi, cationpi, donorpi, halogenpi, metsulphurpi
    ri_file=pdb_file[:-3]+"ri" # for ring-ring
    amam_file=pdb_file[:-3]+"amam" # for amideamide
    amri_file=pdb_file[:-3]+"amri" # for amidering

    all_interacting_residues=[]
    all_pseudo_coords=[]

    type_of_pi=['carbonpi','cationpi','donorpi','halogenpi','metsulphurpi','pipi','amideamide','amidering']
    all_PIinteractions= dict.fromkeys(type_of_pi,list())

    if os.path.getsize(ari_file)>0:

        # atom_ring(ari_file)
        carbonpi=[]
        cationpi=[]
        donorpi=[]
        halogenpi=[]
        metsulphurpi=[]

        ari_file = pd.read_csv(ari_file, sep='\t', header=None)
        ari_file.index += 1
        ari_file.columns = ['target_atom', 'unknown', 'pseudo_residue', 'pseudo_coord', 'int_type', 'target_type',
                            'int_range']

        try:
            if area_interest == 'INTER':
                ari_file = ari_file.loc[ari_file['int_range']=='INTER']
            else:
                # ari_file = ari_file.loc[ari_file['int_range'] != 'INTER']
                pass

            ari_file['target_residue'] = ari_file.apply(lambda row: changeFormForTarget(row,'target_atom'),axis=1)
            ari_file['target_atom'] = ari_file.apply(lambda row: changeForm(row, 'target_atom'), axis=1)
            ari_file['pseudo_residue'] = ari_file.apply(lambda row: changeForm(row, 'pseudo_residue')[:-1], axis=1)
            ari_file['pseudo_coord'] = ari_file.apply(lambda row: roundupList(','.join(row['pseudo_coord'][1:-1].split(','))), axis=1)
            ari_file['int_type'] = ari_file.apply(lambda row: row['int_type'][2:-2], axis=1)
            ari_file = ari_file.drop(['target_type', 'int_range'], axis=1)

            grouped = ari_file.groupby('int_type')
            for group_name, table in grouped:
                if group_name == 'CARBONPI':
                    for index, row in table.iterrows():
                        carbonpi.append([row.tolist()[0], row.tolist()[3]])

                if group_name == 'CATIONPI':
                    for index, row in table.iterrows():
                        cationpi.append([row.tolist()[0], row.tolist()[3]])

                if group_name == 'DONORPI':
                    for index, row in table.iterrows():
                        donorpi.append([row.tolist()[0], row.tolist()[3]])

                if group_name == 'HALOGENPI':
                    for index, row in table.iterrows():
                        halogenpi.append([row.tolist()[0], row.tolist()[3]])
                if group_name == 'METSULPHURPI':
                    for index, row in table.iterrows():
                        metsulphurpi.append([row.tolist()[0], row.tolist()[3]])
            # save each of interaction into separate list.
            all_PIinteractions['carbonpi'] = carbonpi
            all_PIinteractions['cationpi'] = cationpi
            all_PIinteractions['donorpi'] = donorpi
            all_PIinteractions['halogenpi'] = halogenpi
            all_PIinteractions['metsulphurpi'] = metsulphurpi

            # for showing interacting residues
            all_interacting_residues.extend(list(set(ari_file['target_residue'].tolist())))
            all_interacting_residues.extend(list(set(ari_file['pseudo_residue'].tolist())))
            # print(all_interacting_residues)

            # for drawing pseudo_coord
            # all_pseudo_coords.extend(list(set(ari_file['pseudo_coord'].tolist())))
        except ValueError:
            pass


    if os.path.getsize(ri_file) > 0:
        # ring_ring(ri_file)
        pipi=[]

        ri_file = pd.read_csv(ri_file, sep='\t', header=None)
        ri_file.index += 1
        ri_file.columns = ['ring_num1', 'pseudo_residue1', 'pseudo_coord1','ring_num2','pseudo_residue2','pseudo_coord2', 'int_type', 'target_type',
                           'int_range']

        try:
            if area_interest == 'INTER':
                ri_file = ri_file.loc[ri_file['int_range'] == 'INTER']
            else:
                # ri_file = ri_file.loc[ri_file['int_range'] != 'INTER']
                pass

            if remove_redundant == 'True':
                ri_file['redundancy'] = ri_file['ring_num1'] > ri_file['ring_num2']
                ri_file = ri_file[~ri_file['redundancy']]
            else:
                pass

            ri_file = ri_file.drop(['target_type', 'int_range'], axis=1)
            ri_file['pseudo_residue1'] = ri_file.apply(lambda row: changeForm(row, 'pseudo_residue1')[:-1], axis=1)
            ri_file['pseudo_residue2'] = ri_file.apply(lambda row: changeForm(row, 'pseudo_residue2')[:-1], axis=1)
            ri_file['pseudo_coord1'] = ri_file.apply(lambda row: roundupList(','.join(row['pseudo_coord1'][1:-1].split(','))), axis=1)
            ri_file['pseudo_coord2'] = ri_file.apply(lambda row: roundupList(','.join(row['pseudo_coord2'][1:-1].split(','))), axis=1)

            for index, row in ri_file.iterrows():

                pipi.append([row.tolist()[2],row.tolist()[5]])

            # save interactions into one list
            all_PIinteractions['pipi'] = pipi

            # for showing interacting residues
            all_interacting_residues.extend(list(set(ri_file['pseudo_residue1'].tolist())))
            all_interacting_residues.extend(list(set(ri_file['pseudo_residue2'].tolist())))

            # for drawing pseudo_coord
            # all_pseudo_coords.extend(list(set(ri_file['pseudo_coord1'].tolist())))
            # all_pseudo_coords.extend(list(set(ri_file['pseudo_coord2'].tolist())))

        except ValueError:
            pass


    if os.path.getsize(amam_file)>0:
        # amide_amide(amam_file)
        amideamide=[]

        amam_file = pd.read_csv(amam_file, sep='\t', header=None)

        amam_file.index += 1
        amam_file.columns = ['amide_num1', 'pseudo_residue1', 'pseudo_coord1', 'amide_num2', 'pseudo_residue2', 'pseudo_coord2',
                             'int_type', 'target_type',
                             'int_range']

        try:
            if area_interest == 'INTER':
                amam_file = amam_file.loc[amam_file['int_range'] == 'INTER']
            else:
                # amam_file = amam_file.loc[amam_file['int_range'] != 'INTER']
                pass

            if remove_redundant == 'True':
                amam_file['redundancy'] = amam_file['amide_num1'] > amam_file['amide_num2']
                amam_file = amam_file[~amam_file['redundancy']]

            else:
                pass

            amam_file = amam_file.drop(['target_type', 'int_range'], axis=1)
            amam_file['pseudo_residue1'] = amam_file.apply(lambda row: changeForm(row, 'pseudo_residue1')[:-1], axis=1)
            amam_file['pseudo_residue2'] = amam_file.apply(lambda row: changeForm(row, 'pseudo_residue2')[:-1], axis=1)
            amam_file['pseudo_coord1'] = amam_file.apply(lambda row: roundupList(','.join(row['pseudo_coord1'][1:-1].split(','))), axis=1)
            amam_file['pseudo_coord2'] = amam_file.apply(lambda row: roundupList(','.join(row['pseudo_coord2'][1:-1].split(','))), axis=1)

            for index, row in amam_file.iterrows():
                amideamide.append([row.tolist()[2], row.tolist()[5]])

            # save interactions into one list
            all_PIinteractions['amideamide'] = amideamide

            # for showing interacting residues
            all_interacting_residues.extend(list(set(amam_file['pseudo_residue1'].tolist())))
            all_interacting_residues.extend(list(set(amam_file['pseudo_residue2'].tolist())))

            # for drawing pseudo_coord
            # all_pseudo_coords.extend(list(set(amam_file['pseudo_coord1'].tolist())))
            # all_pseudo_coords.extend(list(set(amam_file['pseudo_coord2'].tolist())))
        except ValueError:
            pass

    if os.path.getsize(amri_file)>0:
        # amide_ring(amri_file)
        amidering=[]
        amri_file = pd.read_csv(amri_file, sep='\t', header=None)

        amri_file.index += 1
        amri_file.columns = ['amidering_num1', 'pseudo_residue1', 'pseudo_coord1', 'amidering_num2', 'pseudo_residue2', 'pseudo_coord2',
                             'int_type', 'target_type',
                             'int_range']

        try:
            if area_interest == 'INTER':
                amri_file = amri_file.loc[amri_file['int_range'] == 'INTER']
            else:
                # amri_file = amri_file.loc[amri_file['int_range'] != 'INTER']
                pass

            amri_file = amri_file.drop(['target_type', 'int_range'], axis=1)
            amri_file['pseudo_residue1'] = amri_file.apply(lambda row: changeForm(row, 'pseudo_residue1')[:-1], axis=1)
            amri_file['pseudo_residue2'] = amri_file.apply(lambda row: changeForm(row, 'pseudo_residue2')[:-1], axis=1)
            amri_file['pseudo_coord1'] = amri_file.apply(lambda row: roundupList(','.join(row['pseudo_coord1'][1:-1].split(','))), axis=1)
            amri_file['pseudo_coord2'] = amri_file.apply(lambda row: roundupList(','.join(row['pseudo_coord2'][1:-1].split(','))), axis=1)

            for index, row in amri_file.iterrows():
                amidering.append([row.tolist()[2],row.tolist()[5]])

            # save interactions into one list
            all_PIinteractions['amidering'] = amidering

            # for showing interacting residues
            all_interacting_residues.extend(list(set(amri_file['pseudo_residue1'].tolist())))
            all_interacting_residues.extend(list(set(amri_file['pseudo_residue2'].tolist())))

            # for drawing pseudo_coord
            # all_pseudo_coords.extend(list(set(amri_file['pseudo_coord1'].tolist())))
            # all_pseudo_coords.extend(list(set(amri_file['pseudo_coord2'].tolist())))
        except ValueError:
            pass


    all_interacting_residues = list(set(all_interacting_residues))
    all_pseudo_coords = list(set(all_pseudo_coords))

    # print(all_interacting_residues)
    # print(all_pseudo_coords)
    # print(all_PIinteractions['amidering'])
    return all_PIinteractions, all_interacting_residues
    # return all_interacting_residues, all_pseudo_coords, all_PIinteractions


def getSumContacts(input_file, condition): # for getting contacts results
    result = []

    Clash = []
    Covalent = []
    VDWClash = []
    VDW = []
    Proximal = []
    Metal = []
    Hydrophobic = []

    # group1=[]
    Hbond = []
    WeakHbond = []
    Halogen = []
    Ionic = []
    Polar = []
    WeakPolar = []

    # group2=[]
    Aromatic = []
    Carbonyl = []

    if condition != 'ALL':
        with open(input_file, 'r') as input:
            for each in input.readlines():
                striped = each.strip().split('\t')

                if striped[17] == 'INTER':
                    Clash.append(int(striped[2]))
                    Covalent.append(int(striped[3]))
                    VDWClash.append(int(striped[4]))
                    VDW.append(int(striped[5]))
                    Proximal.append(int(striped[6]))
                    Metal.append(int(striped[11]))
                    Hydrophobic.append(int(striped[13]))

                    Hbond.append(int(striped[7]))
                    WeakHbond.append(int(striped[8]))
                    Halogen.append(int(striped[9]))
                    Ionic.append(int(striped[10]))
                    Polar.append(int(striped[15]))
                    WeakPolar.append(int(striped[16]))

                    Aromatic.append(int(striped[12]))
                    Carbonyl.append(int(striped[14]))

                filename = input_file.split('/')[-1]

                result = [filename, sum(Clash), sum(Covalent), sum(VDWClash), sum(VDW), sum(Proximal), sum(Hbond),
                          sum(WeakHbond), sum(Halogen), sum(Ionic), sum(Metal), sum(Aromatic), sum(Hydrophobic),
                          sum(Carbonyl), sum(Polar), sum(WeakPolar)]

    else:

        with open(input_file, 'r') as input:
            for each in input.readlines():
                striped = each.strip().split('\t')
                Clash.append(int(striped[2]))
                Covalent.append(int(striped[3]))
                VDWClash.append(int(striped[4]))
                VDW.append(int(striped[5]))
                Proximal.append(int(striped[6]))
                Metal.append(int(striped[11]))
                Hydrophobic.append(int(striped[13]))

                Hbond.append(int(striped[7]))
                WeakHbond.append(int(striped[8]))
                Halogen.append(int(striped[9]))
                Ionic.append(int(striped[10]))
                Polar.append(int(striped[15]))
                WeakPolar.append(int(striped[16]))

                Aromatic.append(int(striped[12]))
                Carbonyl.append(int(striped[14]))

            filename = input_file.split('/')[-1]

            result = [filename, sum(Clash), sum(Covalent), sum(VDWClash), sum(VDW), sum(Proximal), sum(Hbond),
                      sum(WeakHbond), sum(Halogen), sum(Ionic), sum(Metal), sum(Aromatic), sum(Hydrophobic),
                      sum(Carbonyl), sum(Polar), sum(WeakPolar)]

    # print(result)

    column_names = ['Clash', 'Covalent', 'VDWClash', 'VDW', 'Proximal', 'Hbond', 'WeakHbond', 'Halogen', 'Ionic',
                    'Metal', 'Aromatic', 'Hydrophobic', 'Carbonyl', 'Polar', 'WeakPolar']
    pandas_result = pd.DataFrame().append([result[1:]])
    pandas_result.columns = column_names
    # print(pandas_result)

    return pandas_result

def getSumContacts_Folder(input_path, condition):
    output_path = input_path + '/Arpeggio_Contacts_Analysis_result'

    result = [['filename', 'Clash', 'Covalent', 'VDWClash', 'VDW', 'Proximal', 'Hbond', 'WeakHbond', 'Halogen', 'Ionic',
               'Metal', 'Aromatic', 'Hydrophobic', 'Carbonyl', 'Polar', 'WeakPolar']]

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    for fname in os.listdir(input_path):

        if not fname.startswith('.') and fname.__contains__('.contacts'):
            contacts_path = os.path.join(input_path, fname)

            result.append(getSumContacts(contacts_path, condition))

    output = output_path + '/' + 'arpeggio_result.csv'
    with open(output, 'w') as outputcontacts:
        wr = csv.writer(outputcontacts)
        wr.writerows(result)
        outputcontacts.close()

    return "Your Job is done!"


def changeForm(row, column_name):
    resname = row[column_name].split('/')[1]
    chain = row[column_name].split('/')[0]
    atomname = row[column_name].split('/')[2]

    try:
        int(resname)
    except ValueError:
        resname = resname[0:len(resname)-1]+"^"+resname[-1]

    # return row[column_name].split('/')[1] + ':' + row[column_name].split('/')[0] + '.' + row[column_name].split('/')[2]
    return resname + ":" + chain + "." + atomname

def changeFormForTarget(row, column_name):
    resname = row[column_name].split('/')[1]
    chain = row[column_name].split('/')[0]
    atomname = row[column_name].split('/')[2]

    try:
        int(resname)
    except ValueError:
        resname = resname[0:len(resname)-1]+"^"+resname[-1]

    # return row[column_name].split('/')[1] + ':' + row[column_name].split('/')[0] + '.' + row[column_name].split('/')[2]
    return resname + ":" + chain


def getContactsViewer(contacts_file):
    contacts_file = pd.read_csv(contacts_file, sep='\t', header=None)
    contacts_file.index += 1
    contacts_file.columns = ['A', 'B', 'Clash', 'Covalent', 'VDWClash', 'VDW', 'Proximal', 'Hbond', 'WeakHbond',
                             'Halogen', 'Ionic', 'Metal', 'Aromatic', 'Hydrophobic', 'Carbonyl', 'Polar', 'WeakPolar',
                             'type']

    #print(contacts_file['A'])
    contacts_file['A'] = contacts_file.apply(lambda row: changeForm(row, 'A'), axis=1)
    contacts_file['B'] = contacts_file.apply(lambda row: changeForm(row, 'B'), axis=1)


    # Clash = Clash + VDWClash
    clash = []
    for x in range(0, len(contacts_file[contacts_file['Clash'] > 0][['A', 'B']])):
        clash.append(contacts_file[contacts_file['Clash'] > 0][['A', 'B']].iloc[x].tolist())
    for x in range(0, len(contacts_file[contacts_file['VDWClash'] > 0][['A', 'B']])):
        clash.append(contacts_file[contacts_file['VDWClash'] > 0][['A', 'B']].iloc[x].tolist())

    # VDW = VDW
    vdw = []
    for x in range(0, len(contacts_file[contacts_file['VDW'] > 0][['A', 'B']])):
        vdw.append(contacts_file[contacts_file['VDW'] > 0][['A', 'B']].iloc[x].tolist())

    # Hbond = Hbond + WeakHbond
    hbond = []
    for x in range(0, len(contacts_file[contacts_file['Hbond'] > 0][['A', 'B']])):
        hbond.append(contacts_file[contacts_file['Hbond'] > 0][['A', 'B']].iloc[x].tolist())

    for x in range(0, len(contacts_file[contacts_file['WeakHbond'] > 0][['A', 'B']])):
        hbond.append(contacts_file[contacts_file['WeakHbond'] > 0][['A', 'B']].iloc[x].tolist())

    # ionic = Ionic
    ionic = []
    for x in range(0, len(contacts_file[contacts_file['Ionic'] > 0][['A', 'B']])):
        ionic.append(contacts_file[contacts_file['Ionic'] > 0][['A', 'B']].iloc[x].tolist())

    # aromatic = Aromatic
    aromatic = []
    for x in range(0, len(contacts_file[contacts_file['Aromatic'] > 0][['A', 'B']])):
        aromatic.append(contacts_file[contacts_file['Aromatic'] > 0][['A', 'B']].iloc[x].tolist())

    # hydrophobic = Hydrophobic
    hydrophobic = []
    for x in range(0, len(contacts_file[contacts_file['Hydrophobic'] > 0][['A', 'B']])):
        hydrophobic.append(contacts_file[contacts_file['Hydrophobic'] > 0][['A', 'B']].iloc[x].tolist())

    # carbonyl = Carbonyl
    carbonyl = []
    for x in range(0, len(contacts_file[contacts_file['Carbonyl'] > 0][['A', 'B']])):
        carbonyl.append(contacts_file[contacts_file['Carbonyl'] > 0][['A', 'B']].iloc[x].tolist())

    # polar = Polar + WeakPolar
    polar = []
    for x in range(0, len(contacts_file[contacts_file['Polar'] > 0][['A', 'B']])):
        polar.append(contacts_file[contacts_file['Polar'] > 0][['A', 'B']].iloc[x].tolist())
    for x in range(0, len(contacts_file[contacts_file['WeakPolar'] > 0][['A', 'B']])):
        polar.append(contacts_file[contacts_file['WeakPolar'] > 0][['A', 'B']].iloc[x].tolist())

    result = {'vdw': vdw, 'hbond': hbond, 'clash': clash, 'ionic': ionic, 'aromatic': aromatic,
              'hydrophobic': hydrophobic, 'carbonyl': carbonyl, 'polar': polar}
    return result

def main(infile, area_interest, remove_redundant):

    infile = pd.read_csv(infile,header=0)

    result = pd.DataFrame()

    for index, row in infile.iterrows():

        wild_contacts_file = row['wild_contacts']
        mutant_contacts_file = row['mutant_contacts_file']

        ## If Arpeggio calculated without any specific mutation sites, 
        ## following "INTER" is needed to consider interactions only relevant with specific position.

        forward_contacts = getSumContacts(wild_contacts_file,'INTER')
        reverse_contacts = getSumContacts(mutant_contacts_file,'INTER')

        dContacts = forward_contacts - reverse_contacts

        dContacts.rename(columns={"Clash":"d_Clash","Covalent":"d_Covalent","VDWClash":"d_VDWClash","VDW":"d_VDW","Proximal":"d_Proximal","Hbond":"d_Hbond","WeakHbond":"d_WeakHbond","Halogen":"d_Halogen","Ionic":"d_Ionic","Metal":"d_Metal","Aromatic":"d_Aromatic",
            "Hydrophobic":"d_Hydrophobic","Carbonyl":"d_Carbonyl","Polar":"d_Polar","WeakPolar":"d_WeakPolar"}, inplace=True)

        wild_PI_file = wild_contacts_file[:-8]+"ari"
        mutant_PI_file = mutant_contacts_file[:-8]+"ari"

        forward_PI, forward_temp = getALLPI(wild_PI_file, area_interest, remove_redundant)
        # 

        reverse_PI, reverse_temp = getALLPI(mutant_PI_file, area_interest, remove_redundant)
        dict_PI ={'d_PI-PI': len(forward_PI['pipi']) - len(reverse_PI['pipi']),
            'd_Carbon-PI': len(forward_PI['carbonpi']) - len(reverse_PI['carbonpi']),
            'd_Cation-PI': len(forward_PI['cationpi']) - len(reverse_PI['cationpi']),
            'd_Donor-PI': len(forward_PI['donorpi']) - len(reverse_PI['donorpi']),
            'd_MetalSulphur-PI': len(forward_PI['metsulphurpi']) - len(
                reverse_PI['metsulphurpi']),
            'd_Amide-Amide': len(forward_PI['amideamide']) - len(reverse_PI['amideamide']),
            'd_Amide-Ring': len(forward_PI['amidering']) - len(reverse_PI['amidering'])}
        # Contacts = pd.DataFrame([forward_contacts.values()],columns=forward_contacts.keys())
        dict_wt_PI ={'PI-PI': len(forward_PI['pipi']),
            'Carbon-PI': len(forward_PI['carbonpi']),
            'Cation-PI': len(forward_PI['cationpi']),
            'Donor-PI': len(forward_PI['donorpi']),
            'MetalSulphur-PI': len(forward_PI['metsulphurpi']),
            'Amide-Amide': len(forward_PI['amideamide']),
            'Amide-Ring': len(forward_PI['amidering'])}

        dArpeggio_PIs = pd.DataFrame([dict_PI.values()],columns=dict_PI.keys())
        Arpeggio_PIs = pd.DataFrame([dict_wt_PI.values()], columns=dict_wt_PI.keys())
        # print(forward_PI)
        result = result.append(pd.concat([forward_contacts,Arpeggio_PIs,dContacts,dArpeggio_PIs], axis=1)) #Changed to include WT values too

    result.to_csv(outfile,index=False)

if __name__ == '__main__':
    main(infile, area_interest, remove_redundant)