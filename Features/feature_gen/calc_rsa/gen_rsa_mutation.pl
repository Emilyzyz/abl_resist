#!/usr/bin/perl -w
# ***********************************
# * ------------------------------- *
# * Douglas Eduardo Valente Pires   *
# * douglas.pires@cpqrr.fiocruz.br  *
# * ------------------------------- *
# * Last modification :: 24/04/2017 *
# * ------------------------------- *
# ***********************************

use strict;
use warnings;

sub trim;
sub distance;
sub res_cod1_to_res_cod3;

# ____________________________________________________________________________________________________________________
# Input parameters
my $infile_mut = $ARGV[0];

if(scalar(@ARGV) != 1){
print "___________________________________________________________________________________
SINTAX:
	perl check_mutations.pl <mutation_file>
___________________________________________________________________________________\n";
	exit;
}

# ____________________________________________________________________________________________________________________

open(MUT,"<$infile_mut") or die "$!Erro ao abrir: $infile_mut\n";
my @mut = <MUT>;
close MUT;


# -------------------------------------------------------------------------------------------
# Parse header
my $header = shift @mut;
chomp($header);

my @columns = split("\t", $header);
my %col_dict;

for(my$i=0; $i<scalar(@columns); $i++){
        $col_dict{$columns[$i]} = $i;
				#print "$columns[$i]\n";
}

print "RSA\n";

foreach my $line_mut (@mut){
        chomp($line_mut);

	my @tokens = split("\t", $line_mut);
	my $pdb_file = $tokens[$col_dict{"PDB"}];
	my $mutation = $tokens[$col_dict{"MUTATION"}];
	my $chain = $tokens[$col_dict{"CHAIN"}];

	my $wild_res = substr($mutation, 0, 1);
	my $wild_res_pos = substr($mutation, 1, length($mutation)-2);
	my $mutated_res = substr($mutation, length($mutation)-1, 1);

	my @rsa_data = `calc_rsa/./psa $pdb_file -t`;

  #my @rsa_data = `./psa $pdb_file -t`;

	foreach my $rsa_line (@rsa_data){
		if(length($rsa_line) >= 66){
			chomp($rsa_line);

			my $res_num = trim(substr($rsa_line,6,6));
			my $res_name = substr($rsa_line,14,3);
			my $rsa = trim(substr($rsa_line,61,5));


			if($wild_res eq res_cod3_to_res_cod1($res_name) and $wild_res_pos eq $res_num){
				print "$rsa\n";
				last;
			}
		}
	}
}

exit;
# ____________________________________________________________________________________________________________________
sub trim{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

sub res_cod3_to_res_cod1{

	my $cod3 = shift;

	if($cod3 eq "ALA"){
		return "A";
	}
	elsif($cod3 eq "VAL"){
		return "V";
	}
	elsif($cod3 eq "LEU"){
		return "L";
	}
	elsif($cod3 eq "GLY"){
		return "G";
	}
	elsif($cod3 eq "SER"){
		return "S";
	}
	elsif($cod3 eq "TRP"){
		return "W";
	}
	elsif($cod3 eq "THR"){
		return "T";
	}
	elsif($cod3 eq "GLN"){
		return "Q";
	}
	elsif($cod3 eq "GLU"){
		return "E";
	}
	elsif($cod3 eq "CYS"){
		return "C";
	}
	elsif($cod3 eq "ARG"){
		return "R";
	}
	elsif($cod3 eq "PRO"){
		return "P";
	}
	elsif($cod3 eq "ASP"){
		return "D";
	}
	elsif($cod3 eq "PHE"){
		return "F";
	}
	elsif($cod3 eq "ILE"){
		return "I";
	}
	elsif($cod3 eq "HIS"){
		return "H";
	}
	elsif($cod3 eq "ASN"){
		return "N";
	}
	elsif($cod3 eq "MET"){
		return "M";
	}
	elsif($cod3 eq "TYR"){
		return "Y";
	}
	elsif($cod3 eq "LYS"){
		return "K";
	}
	return "ERROR";
}
