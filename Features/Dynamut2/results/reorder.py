
import pandas as pd

# -------------------------------------------------------------------------------------------------------------


forward = pd.read_csv('for_output.csv')
header = list(forward.columns.values)
# Strips the newline character

backward = pd.read_csv('output.csv')
selection = backward.loc[:,header]
selection.to_csv('back_output.csv', index = False)
