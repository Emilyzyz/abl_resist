# *************************************** #
# * ----------------------------------- * #
# *   Douglas Eduardo Valente Pires     * #
# *   douglas.pires@minas.fiocruz.br    * #
# * ----------------------------------- * #
# *   Last modification :: 29/08/2018   * #
# * ----------------------------------- * #
# *************************************** #

import sys
import io
from Bio.PDB import PDBParser, ResidueDepth

pdb_file = sys.argv[1]
residue = sys.argv[2]
chain = sys.argv[3]

p = PDBParser()
s = p.get_structure("X", pdb_file)
model = s[0]
model_chain = model[chain]

rd = ResidueDepth(model_chain, pdb_file)

res_depth, ca_depth = rd[(chain, (' ', int(residue), ' '))]

print (res_depth)
