#!/usr/bin/perl -w
# ********************************************************
# *   ----------------------------------------------     *
# * Douglas Eduardo Valente Pires - dpires@dcc.ufmg.br   *
# * www.dcc.ufmg.br/~dpires                              *
# * Last modification :: 05/05/2015                      *
# *   ----------------------------------------------     *
# ********************************************************

# * Modified by Emily Yunzhuo Zhou on 03/08/2021 *
#use strict;
use warnings;
use System::Timeout qw(system);

sub trim;
sub distance;
sub res_cod3_to_res_cod1;



# -------------------------------------------------------------------------------------------------
# Pharmacophore count vector for each residue: Hydro,Pos,Neg,Acc,Don,Aro,Sulf

%fingerprint = ();
$fingerprint{"A"} = "1,0,0,1,1,0,0,2";
$fingerprint{"V"} = "3,0,0,1,1,0,0,2";
$fingerprint{"L"} = "4,0,0,1,1,0,0,2";
$fingerprint{"G"} = "0,0,0,1,1,0,0,2";
$fingerprint{"S"} = "0,0,0,1,2,0,0,3";
$fingerprint{"W"} = "10,0,0,1,2,9,0,2";
$fingerprint{"T"} = "1,0,0,1,2,0,0,3";
$fingerprint{"Q"} = "2,0,0,2,2,0,0,3";
$fingerprint{"E"} = "2,0,2,3,1,0,0,3";
$fingerprint{"C"} = "1,0,0,1,1,0,1,3";
$fingerprint{"R"} = "3,2,0,1,4,0,0,3";
$fingerprint{"P"} = "3,0,0,1,1,0,0,2";
$fingerprint{"D"} = "1,0,2,3,1,0,0,3";
$fingerprint{"F"} = "7,0,0,1,1,6,0,2";
$fingerprint{"I"} = "4,0,0,1,1,0,0,2";
$fingerprint{"H"} = "4,2,0,1,3,5,0,2";
$fingerprint{"N"} = "1,0,0,2,3,0,0,3";
$fingerprint{"M"} = "3,0,0,1,1,0,1,2";
$fingerprint{"Y"} = "7,0,0,1,2,6,0,2";
$fingerprint{"K"} = "3,1,0,1,2,0,0,3";

# -------------------------------------------------------------------------------------------------
# Atom classifications - Pharmacophores

%AtomFeature = ();
# -------------------------------------------------------------------------------------------------
# Hydrophobics

$AtomFeature{"ALACB"}{"Hydro"} = 1;
$AtomFeature{"ARGCB"}{"Hydro"} = 1;
$AtomFeature{"ARGCG"}{"Hydro"} = 1;
$AtomFeature{"ARGCD"}{"Hydro"} = 1;
$AtomFeature{"ASNCB"}{"Hydro"} = 1;
$AtomFeature{"ASPCB"}{"Hydro"} = 1;
$AtomFeature{"CYSCB"}{"Hydro"} = 1;
$AtomFeature{"GLNCB"}{"Hydro"} = 1;
$AtomFeature{"GLNCG"}{"Hydro"} = 1;
$AtomFeature{"GLUCB"}{"Hydro"} = 1;
$AtomFeature{"GLUCG"}{"Hydro"} = 1;
$AtomFeature{"HISCB"}{"Hydro"} = 1;
$AtomFeature{"HISCG"}{"Hydro"} = 1;
$AtomFeature{"HISCD2"}{"Hydro"} = 1;
$AtomFeature{"HISCE1"}{"Hydro"} = 1;
$AtomFeature{"ILECB"}{"Hydro"} = 1;
$AtomFeature{"ILECG1"}{"Hydro"} = 1;
$AtomFeature{"ILECG2"}{"Hydro"} = 1;
$AtomFeature{"ILECD1"}{"Hydro"} = 1;
$AtomFeature{"LEUCB"}{"Hydro"} = 1;
$AtomFeature{"LEUCG"}{"Hydro"} = 1;
$AtomFeature{"LEUCD1"}{"Hydro"} = 1;
$AtomFeature{"LEUCD2"}{"Hydro"} = 1;
$AtomFeature{"LYSCB"}{"Hydro"} = 1;
$AtomFeature{"LYSCG"}{"Hydro"} = 1;
$AtomFeature{"LYSCD"}{"Hydro"} = 1;
$AtomFeature{"METCB"}{"Hydro"} = 1;
$AtomFeature{"METCG"}{"Hydro"} = 1;
$AtomFeature{"METCE"}{"Hydro"} = 1;
$AtomFeature{"PHECB"}{"Hydro"} = 1;
$AtomFeature{"PHECG"}{"Hydro"} = 1;
$AtomFeature{"PHECD1"}{"Hydro"} = 1;
$AtomFeature{"PHECD2"}{"Hydro"} = 1;
$AtomFeature{"PHECE1"}{"Hydro"} = 1;
$AtomFeature{"PHECE2"}{"Hydro"} = 1;
$AtomFeature{"PHECZ"}{"Hydro"} = 1;
$AtomFeature{"PROCB"}{"Hydro"} = 1;
$AtomFeature{"PROCG"}{"Hydro"} = 1;
$AtomFeature{"PROCD"}{"Hydro"} = 1;
$AtomFeature{"THRCG2"}{"Hydro"} = 1;
$AtomFeature{"TRPCB"}{"Hydro"} = 1;
$AtomFeature{"TRPCG"}{"Hydro"} = 1;
$AtomFeature{"TRPCD1"}{"Hydro"} = 1;
$AtomFeature{"TRPCD2"}{"Hydro"} = 1;
$AtomFeature{"TRPCE2"}{"Hydro"} = 1;
$AtomFeature{"TRPCE3"}{"Hydro"} = 1;
$AtomFeature{"TRPCH2"}{"Hydro"} = 1;
$AtomFeature{"TRPCZ"}{"Hydro"} = 1;
$AtomFeature{"TRPCZ2"}{"Hydro"} = 1;
$AtomFeature{"TRPCZ3"}{"Hydro"} = 1;
$AtomFeature{"TYRCB"}{"Hydro"} = 1;
$AtomFeature{"TYRCG"}{"Hydro"} = 1;
$AtomFeature{"TYRCD1"}{"Hydro"} = 1;
$AtomFeature{"TYRCD2"}{"Hydro"} = 1;
$AtomFeature{"TYRCE1"}{"Hydro"} = 1;
$AtomFeature{"TYRCE2"}{"Hydro"} = 1;
$AtomFeature{"TYRCZ"}{"Hydro"} = 1;
$AtomFeature{"VALCB"}{"Hydro"} = 1;
$AtomFeature{"VALCG1"}{"Hydro"} = 1;
$AtomFeature{"VALCG2"}{"Hydro"} = 1;
# -------------------------------------------------------------------------------------------------
# Acceptors

$AtomFeature{"ALAO"}{"Acc"} = 1;
$AtomFeature{"ARGO"}{"Acc"} = 1;
$AtomFeature{"ASNO"}{"Acc"} = 1;
$AtomFeature{"ASNOD1"}{"Acc"} = 1;
$AtomFeature{"ASPO"}{"Acc"} = 1;
$AtomFeature{"ASPOD1"}{"Acc"} = 1;
$AtomFeature{"ASPOD2"}{"Acc"} = 1;
$AtomFeature{"CYSO"}{"Acc"} = 1;
$AtomFeature{"GLNO"}{"Acc"} = 1;
$AtomFeature{"GLNOE1"}{"Acc"} = 1;
$AtomFeature{"GLUO"}{"Acc"} = 1;
$AtomFeature{"GLUOE1"}{"Acc"} = 1;
$AtomFeature{"GLUOE2"}{"Acc"} = 1;
$AtomFeature{"GLYO"}{"Acc"} = 1;
$AtomFeature{"HISO"}{"Acc"} = 1;
$AtomFeature{"ILEO"}{"Acc"} = 1;
$AtomFeature{"LEUO"}{"Acc"} = 1;
$AtomFeature{"LYSO"}{"Acc"} = 1;
$AtomFeature{"METO"}{"Acc"} = 1;
$AtomFeature{"PHEO"}{"Acc"} = 1;
$AtomFeature{"PROO"}{"Acc"} = 1;
$AtomFeature{"SERO"}{"Acc"} = 1;
$AtomFeature{"THRO"}{"Acc"} = 1;
$AtomFeature{"TRPO"}{"Acc"} = 1;
$AtomFeature{"TYRO"}{"Acc"} = 1;
$AtomFeature{"VALO"}{"Acc"} = 1;
# -------------------------------------------------------------------------------------------------
# Donors

$AtomFeature{"CYSS"}{"Don"} = 1;
$AtomFeature{"ALAN"}{"Don"} = 1;
$AtomFeature{"ARGN"}{"Don"} = 1;
$AtomFeature{"ARGNE"}{"Don"} = 1;
$AtomFeature{"ARGNH1"}{"Don"} = 1;
$AtomFeature{"ARGNH2"}{"Don"} = 1;
$AtomFeature{"ASNN"}{"Don"} = 1;
$AtomFeature{"ASNND2"}{"Don"} = 1;
$AtomFeature{"ASPN"}{"Don"} = 1;
$AtomFeature{"CYSN"}{"Don"} = 1;
$AtomFeature{"GLNN"}{"Don"} = 1;
$AtomFeature{"GLNNE2"}{"Don"} = 1;
$AtomFeature{"GLUN"}{"Don"} = 1;
$AtomFeature{"GLYN"}{"Don"} = 1;
$AtomFeature{"HISN"}{"Don"} = 1;
$AtomFeature{"HISND1"}{"Don"} = 1;
$AtomFeature{"HISNE2"}{"Don"} = 1;
$AtomFeature{"ILEN"}{"Don"} = 1;
$AtomFeature{"LEUN"}{"Don"} = 1;
$AtomFeature{"LYSN"}{"Don"} = 1;
$AtomFeature{"LYSNZ"}{"Don"} = 1;
$AtomFeature{"METN"}{"Don"} = 1;
$AtomFeature{"PHEN"}{"Don"} = 1;
$AtomFeature{"PRON"}{"Don"} = 1;
$AtomFeature{"SERN"}{"Don"} = 1;
$AtomFeature{"SEROG"}{"Don"} = 1;
$AtomFeature{"THRN"}{"Don"} = 1;
$AtomFeature{"THROG1"}{"Don"} = 1;
$AtomFeature{"TRPN"}{"Don"} = 1;
$AtomFeature{"TRPNE1"}{"Don"} = 1;
$AtomFeature{"TYRN"}{"Don"} = 1;
$AtomFeature{"TYROH"}{"Don"} = 1;
$AtomFeature{"VALN"}{"Don"} = 1;
# -------------------------------------------------------------------------------------------------
# Aromatics

$AtomFeature{"HISCG"}{"Aro"} = 1;
$AtomFeature{"HISND1"}{"Aro"} = 1;
$AtomFeature{"HISCD2"}{"Aro"} = 1;
$AtomFeature{"HISCE1"}{"Aro"} = 1;
$AtomFeature{"HISNE2"}{"Aro"} = 1;
$AtomFeature{"PHECG"}{"Aro"} = 1;
$AtomFeature{"PHECD1"}{"Aro"} = 1;
$AtomFeature{"PHECD2"}{"Aro"} = 1;
$AtomFeature{"PHECE1"}{"Aro"} = 1;
$AtomFeature{"PHECE2"}{"Aro"} = 1;
$AtomFeature{"PHECZ"}{"Aro"} = 1;
$AtomFeature{"TRPCG"}{"Aro"} = 1;
$AtomFeature{"TRPCD1"}{"Aro"} = 1;
$AtomFeature{"TRPCD2"}{"Aro"} = 1;
$AtomFeature{"TRPNE1"}{"Aro"} = 1;
$AtomFeature{"TRPCE2"}{"Aro"} = 1;
$AtomFeature{"TRPCE3"}{"Aro"} = 1;
$AtomFeature{"TRPCZ2"}{"Aro"} = 1;
$AtomFeature{"TRPCZ3"}{"Aro"} = 1;
$AtomFeature{"TRPCH2"}{"Aro"} = 1;
$AtomFeature{"TYRCD1"}{"Aro"} = 1;
$AtomFeature{"TYRCD2"}{"Aro"} = 1;
$AtomFeature{"TYRCE1"}{"Aro"} = 1;
$AtomFeature{"TYRCE2"}{"Aro"} = 1;
$AtomFeature{"TYRCG"}{"Aro"} = 1;
$AtomFeature{"TYRCZ"}{"Aro"} = 1;
# -------------------------------------------------------------------------------------------------
# Positives

$AtomFeature{"ARGNH1"}{"Pos"} = 1;
$AtomFeature{"ARGNH2"}{"Pos"} = 1;
$AtomFeature{"HISND1"}{"Pos"} = 1;
$AtomFeature{"HISNE2"}{"Pos"} = 1;
$AtomFeature{"LYSNZ"}{"Pos"} = 1;
# -------------------------------------------------------------------------------------------------
# Negatives

$AtomFeature{"ASPOD1"}{"Neg"} = 1;
$AtomFeature{"ASPOD2"}{"Neg"} = 1;
$AtomFeature{"GLUOE1"}{"Neg"} = 1;
$AtomFeature{"GLUOE2"}{"Neg"} = 1;


# -------------------------------------------------------------------------------------------------
# Input parameters
my $inFile = $ARGV[0];
my $outFile = $ARGV[1];
#my $mutation = $ARGV[2];
#my $res_chain = $ARGV[3];
#my $lig_pos = $ARGV[4];
#my $lig_chain = $ARGV[5];
#my $lig_dist = $ARGV[6];
#my $lig_smi = $ARGV[4];
#my $lig_name = $ARGV[5];
my $affin_wt = $ARGV[2];
my $code_path =  $ARGV[3];

# -------------------------------------------------------------------------------------------------
my $cutoff_step = 1.00;		# Cutoff step
my $cutoff_limit = 10.00;	# Cutoff limit
my $res_env_dist = 10.00;	# Residue environment distance cutoff

my $signType = 2;		# Signature type:
				# 0:mCSM 		1 atom class
				# 1:mCSM-HP	2 atom classes {Hydro,Polar}
				# 2:mCSM-ALL	7 atom classes {Hydro,Pos,Neg,Acc,Don,Aro,Neutral

my %mcsm_all;
$mcsm_all{"Hydro"} = 1;		$mcsm_all{"Pos"} = 1;
$mcsm_all{"Neg"} = 1;		$mcsm_all{"Acc"} = 1;
$mcsm_all{"Don"} = 1;		$mcsm_all{"Aro"} = 1;
$mcsm_all{"Neut"} = 1;

if(scalar(@ARGV) != 4){
print "___________________________________________________________________________________
SINTAX:
	perl mCSM-lig.pl <infile> <outfile> <affin_wt> <code_path>
___________________________________________________________________________________
Where:

<outfile> is the file where the signatures will be stored
<inFile> should have three columns, separated by \t:

	<pdb>;<mut>;<res_chain>;<lig_name>;<lig_smi>

	<pdb>             - path to pdb

	<mut>
		<wild_type_res>   - 1-letter code for the wild-type residue
		<residue_index>   - residue index (same residue index as in the PDB file)
		<mutant_res>      - 1-letter code for the mutant residue

	<res_chain>       - chain id
	<lig_name>        - 3-letter code for ligand
	<lig_smi>					- SMILES for ligand
___________________________________________________________________________________\n";
	exit;
}

# -------------------------------------------------------------------------------------------------

if($cutoff_step < 0.001){
	print "\nError: Cutoff Step too small.\nChoose another please.\n\n";
	exit;
}

if($cutoff_limit > 1000){
	print "\nError: Cutoff Limit too large.\nChoose another please.\n\n";
	exit;
}

# -------------------------------------------------------------------------------------------------
open(OUTFILE,">$outFile") or die "$!Error: $outFile\n";

# Generating the header for the output file

print "---Generating signatures\n";

#print OUTFILE "mCSM,SDM,DUET,RES_DEPTH,";
print OUTFILE "PDB_FILE,LIG.MOL_WEIGHT,LIG.ATOM_COUNT,LIG.LOGP,LIG.NUM_ACCEPTORS,LIG.NUM_DONORS,LIG.NUM_HETEROATOMS,";
print OUTFILE "LIG.NUM_ROTATABLE_BONDS,LIG.NUM_RINGS,LIG.LABUTE_ASA,LIG.TPSA,";
print OUTFILE "Hydro,Pos,Neg,Acc,Don,Aro,Sul,Neut,Affin_wt,Affin_wt_log,";

# mCSM
if($signType == 0){
	for(my $x=$cutoff_limit; $x>=1; $x-=$cutoff_step){
		printf OUTFILE '%.2f'.",", $x;
	}
	for(my $x=$cutoff_limit; $x>=1; $x-=$cutoff_step){
		printf OUTFILE 'Inter-%.2f'.",", $x;
	}
}
# mCSM-HP
elsif($signType == 1){
	for(my $x=$cutoff_limit; $x>=1; $x-=$cutoff_step){
		printf OUTFILE "HH:".'%.2f'.",", $x;
		printf OUTFILE "PP:".'%.2f'.",", $x;
		printf OUTFILE "HP:".'%.2f'.",", $x;
	}
	for(my $x=$cutoff_limit; $x>=1; $x-=$cutoff_step){
		printf OUTFILE "Inter-HH:".'%.2f'.",", $x;
		printf OUTFILE "Inter-PP:".'%.2f'.",", $x;
		printf OUTFILE "Inter-HP:".'%.2f'.",", $x;
	}
}
# mCSM-ALL
elsif($signType == 2){
	for(my $x=$cutoff_limit; $x>=1; $x-=$cutoff_step){
		my @keys = sort(keys(%mcsm_all));
		for($a=0; $a<scalar(@keys); $a++){
			for($b=$a; $b<scalar(@keys); $b++){
				printf OUTFILE $keys[$a].":".$keys[$b]."-".'%.2f'.",",$x;
			}
		}
	}

	for(my $x=$cutoff_limit; $x>=1; $x-=$cutoff_step){
		my @keys = sort(keys(%mcsm_all));
		for($a=0; $a<scalar(@keys); $a++){
			for($b=$a; $b<scalar(@keys); $b++){
				printf OUTFILE "Inter-".$keys[$a].":".$keys[$b]."-".'%.2f'.",",$x;
			}
		}
	}
}

print OUTFILE "\n";

print "---Header generated\n";

# ____________________________________________________________________________________________________________________

open(INFILE_1,"<$inFile") or die "$!Error: $inFile\n";

my @IDs = <INFILE_1>;
close INFILE_1;

my $header = shift @IDs;
chomp($header);

my @columns = split("\t", $header);
my %col_dict;

for(my$i=0; $i<scalar(@columns); $i++){
        $col_dict{$columns[$i]} = $i;
}

# Generating signature for each mutation
foreach my $mutation (@IDs){
	chomp($mutation);

	my @tokens = split("\t", $mutation);
	if(scalar(@tokens) != 5){
		print "\n::Error in input file format. Check sintax please::\n\n";
		exit;
	}
	my $pdbID = $tokens[$col_dict{"pdbID"}];
	my $mut = $tokens[$col_dict{"mut"}];
	my $res_chain = $tokens[$col_dict{"res_chain"}];
	my $lig_name = $tokens[$col_dict{"lig_name"}];
	my $lig_smi = $tokens[$col_dict{"lig_smi"}];

	my $wild_res = substr($mut,0,1);
	my $wild_res_pos = substr($mut,1,length($mut)-2);
	my $mutated_res = substr($mut,-1,1);
	###################################
	print "Processing: $mutation\n";
	###################################
	print OUTFILE "$pdbID.$wild_res.$wild_res_pos.$mutated_res.env.pdb,";



#$mutation = uc($mutation);
	my @sign;
	my @sign_inter;

#my $pdbID = $inFile;



#Calculating stability predictions and residue depth
# mCSM,SDM,DUET,RES_DEPTH,

#print "---Calculating stability predictions and residue depth\n";

#my $stability = `perl $code_path/run_DUET_CGI.pl $inFile $mutation $res_chain | grep Kcal/mol | tr -d '\\n' | sed 's/\\t//g' | sed 's/<font size=4 color="#DB1414">//g' | sed 's/<font size=4 color="#1414C4">//g' | sed 's/<\\/font>//g' | sed 's/(<i>Stabilizing<\\/i>)//g' | sed 's/(<i>Destabilizing<\\/i>)//g' | sed 's/ //g' | sed 's/Kcal\\/mol/\\t/g'`;
#print "DUET:\t\t$stability\n";


#my $res_depth = 0;


#system("grep ^ATOM $inFile > $inFile.tmp_depth.pdb");


#open(TMP,">$inFile.depth3") or die "$!Error: $inFile.depth3\n";

#print TMP "$code_path/res_depth.py $inFile.tmp_depth.pdb $wild_res_pos\n";

#system("120", "python $code_path/res_depth.py $inFile.tmp_depth.pdb $wild_res_pos $res_chain > $inFile.depth2");

#close TMP;


#$res_depth = `cat $inFile.depth2`;

#if($res_depth =~ /No such file or directory/){
#	$res_depth = 0;
#}

#print "\t\t$res_depth\n";

#chomp($res_depth);


#chomp($stability);

#my @tokens = split("\t", $stability);
#my $mcsm = $tokens[0];


#my $sdm = 0;
#my $duet = $mcsm;

#if(defined($tokens[1]) and $tokens[1] ne ""){
#        $sdm = $tokens[1];
#        $duet = $tokens[2];
#}


#print OUTFILE "$mcsm,$sdm,$duet,$res_depth,";

	print "---Calculating ligand properties\n";

#Calculating ligand properties
#LIG.MOL_WEIGHT,LIG.ATOM_COUNT,LIG.LOGP,LIG.NUM_ACCEPTORS,LIG.NUM_DONORS,LIG.NUM_HETEROATOMS,LIG.NUM_ROTATABLE_BONDS,LIG.NUM_RINGS,LIG.LABUTE_ASA,LIG.TPSA
	#my $smiles = `cut -f1 $lig_smi`;
	my $smiles = $lig_smi;
	chomp($smiles);
	#print $smiles;
	my $descriptors = `python $code_path/gen_descriptors.py "$smiles"`;
	chomp($descriptors);
	print OUTFILE "$descriptors,";
#print OUTFILE "$lig_dist,";

	print "---Calculating pharmacophore changes\n";

# Calculating pharmacophore changes for the mutation
# Hydro,Pos,Neg,Acc,Don,Aro,Sul,Neut,
	my @tokens_wild = split(",", $fingerprint{$wild_res});
	my @tokens_mut = split(",", $fingerprint{$mutated_res});
	for(my $i=0; $i<scalar(@tokens_wild); $i++){
		print OUTFILE ($tokens_wild[$i] - $tokens_mut[$i]), ",";
	}
# Affin_wt,Affin_wt_log,
	print OUTFILE $affin_wt, ",", log($affin_wt), ",";

	open(PDB,"<$pdbID") or die "$!Erro ao abrir: $pdbID\n";
	my @pdb = <PDB>;
	close PDB;
# ==================================================================================================
# Extracting residue environment - cutoff distance from geometric center of wild-type residue
	my $wild_res_gc_x = 0;
	my $wild_res_gc_y = 0;
	my $wild_res_gc_z = 0;
	my $counter = 0;

	print "---Calculating geometric center\n";

# Calculating geometric center
	foreach my $line (@pdb){

		if(trim(substr($line,0,6)) eq "ATOM"){
			my $cod = res_cod3_to_res_cod1(trim(substr($line,17,3)));
			my $ind = trim(substr($line,22,5));
			my $chain = substr($line,21,1);
			my $x = trim(substr($line,30,8));
			my $y = trim(substr($line,38,8));
			my $z = trim(substr($line,46,8));

			if($cod eq $wild_res and $ind eq $wild_res_pos and $chain eq $res_chain){
				$wild_res_gc_x += $x;
				$wild_res_gc_y += $y;
				$wild_res_gc_z += $z;
				$counter++;
			}
		}
	}

	$wild_res_gc_x /= $counter;
	$wild_res_gc_y /= $counter;
	$wild_res_gc_z /= $counter;

	my $atom_selected = 0;	my $k = 0;		my $max_index = 0;
	my @coord_x;		my @coord_y;			my @env = ();
	my @coord_z;		my @atom_index;		my @is_lig_atom = ();
	my @res_name;		my @atom_name;

# ==================================================================================================
	my $het_pdb = $pdbID.".".$res_chain.".".$lig_name;
	my $het_mol = $het_pdb.".mol";
	$het_pdb .= ".pdb";

	open(HET,">$het_pdb") or die "$!Error: $het_pdb\n";
	my $het_counter = 0;
	my %het_keys; undef %het_keys;

	print "---Filtering atom in residue environment\n";

# Filtering atoms in residue environment
	foreach my $line (@pdb){
		if(trim(substr($line,0,6)) eq "ATOM" or ((substr($line,0,6) eq "HETATM") and (substr($line,17,3) ne "HOH") and (substr($line,17,3) eq $lig_name))){
			my $res_cod = res_cod3_to_res_cod1(trim(substr($line,17,3)));
			my $res_ind = trim(substr($line,22,4));
			my $name = trim(substr($line,17,3));
			my $x = trim(substr($line,30,8));
			my $y = trim(substr($line,38,8));
			my $z = trim(substr($line,46,8));
			my $chain_id = substr($line,21,1);
			my $atm_name = trim(substr($line,12,4));
		#print $name;
			if(distance($x,$y,$z,$wild_res_gc_x,$wild_res_gc_y,$wild_res_gc_z) <= $res_env_dist){
				$coord_x[$k] = $x;
				$coord_y[$k] = $y;
				$coord_z[$k] = $z;
				$atom_index[$k] = trim(substr($line,6,5));

			#print $lig_name;
				$is_lig_atom[$k] = 0;
				if(substr($line,0,6) eq "HETATM" and $name eq $lig_name){
					$is_lig_atom[$k] = 1;
				}

				$res_name[$k] = substr($line,17,3);
				$atom_name[$k] = trim(substr($line,12,4));

				push @env, $line;

				if($atom_index[$k] > $max_index){
					$max_index = $atom_index[$k];
				}
				$k++;
			}

			if(substr($line,0,6) eq "HETATM" and $name eq $lig_name and $res_chain eq $chain_id){
				print HET $line;

				$het_keys{$het_counter} = $name.$atm_name;

				$het_counter++;
			}
		}
	}
	close HET;
# debug

#for(my $i=0;$i<$k;$i++){
#	print $is_lig_atom[$i];
#}

	print "---Calculating pharmacophores for ligand\n";

# Getting pharmacophores for ligand

	system("sed -i \"s/\\s\+\$//g\" $het_pdb");
	system("sed -i \"s/[0-9]-\$//g\" $het_pdb");

	system("babel -ipdb $het_pdb -omol $het_mol -p 7.4 2> /dev/null");
	system("python $code_path/gen_pharmacophores.py $het_mol > $het_mol.pharm");
	system("sed -i \"s/(//g;s/)//g;s/,//g\" $het_mol.pharm");

	open(PHARM, "<$het_mol.pharm") or die "Error!: $het_mol.pharm";
	my @pharm = <PHARM>;
	close PHARM;

	print "---Parsing pharmacophores for ligand\n";

# Get pharmacophores
	foreach my $line (@pharm){
		chomp($line);
		my @class = split("\t", $line);
		my @atom_ind_list = split(" ", $class[1]);

		if($class[0] =~ m/Hydrophobe/){
			foreach my $ind (@atom_ind_list){
				$AtomFeature{$het_keys{$ind}}{"Hydro"} = 1;
			}
		}
		if($class[0] =~ m/Aromatic/){
			foreach my $ind (@atom_ind_list){
				$AtomFeature{$het_keys{$ind}}{"Aro"} = 1;
			}
		}
		if($class[0] =~ m/PosIonizable/){
			foreach my $ind (@atom_ind_list){
				$AtomFeature{$het_keys{$ind}}{"Pos"} = 1;
			}
		}
		if($class[0] =~ m/NegIonizable/){
			foreach my $ind (@atom_ind_list){
				$AtomFeature{$het_keys{$ind}}{"Neg"} = 1;
			}
		}
		if($class[0] =~ m/Acceptor/){
			foreach my $ind (@atom_ind_list){
				$AtomFeature{$het_keys{$ind}}{"Acc"} = 1;
			}
		}
		if($class[0] =~ m/Donor/){
			foreach my $ind (@atom_ind_list){
				$AtomFeature{$het_keys{$ind}}{"Don"} = 1;
			}
		}
	}


# ==================================================================================================

#--------------------------------------------
# mCSM
	my $edgeCount = 0;
	my $edgeCount_interChain = 0;
# mCSM-HP
	my $edgeCountHH = 0;
	my $edgeCountPP = 0;
	my $edgeCountHP = 0;
	my $edgeCountHH_interChain = 0;
	my $edgeCountPP_interChain = 0;
	my $edgeCountHP_interChain = 0;

# mCSM-ALL
	my %edgeCountTipo2;

	my @keys = sort(keys(%mcsm_all));
	foreach my $key1 (@keys){
		foreach my $key2 (@keys){
			$edgeCountTipo2{$key1.":".$key2} = 0;
			$edgeCountTipo2{$key1.":".$key2.":inter"} = 0;
			if($key1 ne $key2){
				$edgeCountTipo2{$key2.":".$key1} = 0;
				$edgeCountTipo2{$key2.":".$key1.":inter"} = 0;
			}
		}
	}
#--------------------------------------------

	$atom_selected = $max_index;
	my @dist;
	@keys = sort(keys(%mcsm_all));

# Cutoff limits
	my $cutoff_1 = 1.0;
	my $cutoff_2 = $cutoff_limit;

	print "---Starting cutoff scanning\n";


# Pair-wise distance calculation of atoms in environment
	for(my $i=0;$i<$k;$i++){
		for(my $j=$i+1;$j<$k;$j++){
			$dist[$i][$j] = distance($coord_x[$i],$coord_y[$i],$coord_z[$i],
						$coord_x[$j],$coord_y[$j],$coord_z[$j]);
			if($dist[$i][$j] >= $cutoff_1 and $dist[$i][$j] <= $cutoff_2){
				if($is_lig_atom[$i] eq $is_lig_atom[$j]){
					$edgeCount++;
				}
				else{
					$edgeCount_interChain++;
				}

				my $key1 = $res_name[$i].$atom_name[$i];
				my $key2 = $res_name[$j].$atom_name[$j];

				if($signType == 1){
					if($AtomFeature{$key1}{"Hydro"} and $AtomFeature{$key2}{"Hydro"}){
						if($is_lig_atom[$i] eq $is_lig_atom[$j]){
							$edgeCountHH++;
						}
						else{
							$edgeCountHH_interChain++;
						}
					} elsif(!($AtomFeature{$key1}{"Hydro"}) and !($AtomFeature{$key2}{"Hydro"})){
						if($is_lig_atom[$i] eq $is_lig_atom[$j]){
							$edgeCountPP++;
						}
						else{
							$edgeCountPP_interChain++;
						}
					} else{
						if($is_lig_atom[$i] eq $is_lig_atom[$j]){
							$edgeCountHP++;
						}
						else{
							$edgeCountHP_interChain++;
						}
					}
				}
				elsif($signType == 2){
					if(!$AtomFeature{$key1}){
						$AtomFeature{$key1}{"Neut"} = 1;

					} if(!$AtomFeature{$key2}){
						$AtomFeature{$key2}{"Neut"} = 1;

					}

					if($is_lig_atom[$i] eq $is_lig_atom[$j]){
						my @key_set1 = keys %{$AtomFeature{$key1}};
						my @key_set2 = keys %{$AtomFeature{$key2}};

						for(my $x=0; $x<scalar(@key_set1); $x++){
							for(my $y=0; $y<scalar(@key_set2); $y++){
								my $my_key_set1 = $key_set1[$x];
								my $my_key_set2 = $key_set2[$y];

								$edgeCountTipo2{$my_key_set1.":".$my_key_set2}++;
								if($my_key_set1 ne $my_key_set2){
									$edgeCountTipo2{$my_key_set2.":".$my_key_set1}++;
								}
							}
						}
					}
					else{
						my @key_set1 = keys %{$AtomFeature{$key1}};
						my @key_set2 = keys %{$AtomFeature{$key2}};

						for(my $x=0; $x<scalar(@key_set1); $x++){
							for(my $y=0; $y<scalar(@key_set2); $y++){
								my $my_key_set1 = $key_set1[$x];
								my $my_key_set2 = $key_set2[$y];

								$edgeCountTipo2{$my_key_set1.":".$my_key_set2.":inter"}++;
								if($my_key_set1 ne $my_key_set2){
									$edgeCountTipo2{$my_key_set2.":".$my_key_set1.":inter"}++;
								}
							}
						}
					}
				}
			}
		}
	}

	if($signType == 0){     #mCSM
		push @sign,$edgeCount;
		push @sign_inter,$edgeCount_interChain;
	}
	elsif($signType == 1){  #mCSM-HP
		push @sign,$edgeCountHH;
		push @sign,$edgeCountPP;
		push @sign,$edgeCountHP;

		push @sign_inter,$edgeCountHH_interChain;
		push @sign_inter,$edgeCountPP_interChain;
		push @sign_inter,$edgeCountHP_interChain;
	}
	elsif($signType == 2){  #mCSM-ALL
		@keys = sort(keys(%mcsm_all));
		for($a=0; $a<scalar(@keys); $a++){
			for($b=$a; $b<scalar(@keys); $b++){
				push @sign, $edgeCountTipo2{$keys[$a].":".$keys[$b]};
				push @sign_inter, $edgeCountTipo2{$keys[$a].":".$keys[$b].":inter"};
			}
		}
	}

	undef %edgeCountTipo2;

# Perform cutoff scanning
	for(my $cutoff_temp=($cutoff_limit-$cutoff_step);$cutoff_temp>=1;$cutoff_temp-=$cutoff_step){
	# mCSM
		$edgeCount = 0;
		$edgeCount_interChain = 0;
	# mCSM-HP
		$edgeCountHH = 0;
		$edgeCountPP = 0;
		$edgeCountHP = 0;
		$edgeCountHH_interChain = 0;
		$edgeCountPP_interChain = 0;
		$edgeCountHP_interChain = 0;

	# mCSM-ALL
		my %edgeCountTipo2;

		my @keys = sort(keys(%mcsm_all));
		foreach my $key1 (@keys){
			foreach my $key2 (@keys){
				$edgeCountTipo2{$key1.":".$key2} = 0;
				$edgeCountTipo2{$key1.":".$key2.":inter"} = 0;
				if($key1 ne $key2){
					$edgeCountTipo2{$key2.":".$key1} = 0;
					$edgeCountTipo2{$key2.":".$key1.":inter"} = 0;
				}
			}
		}

		my $cutoff_1 = 1.0;
		my $cutoff_2 = ($cutoff_temp + 0.0);
		if($cutoff_2 < 0.0){
			last;
		}
		if($cutoff_2 <= ($cutoff_limit-$cutoff_step)*1.000001){
			for(my $i=0;$i<$k;$i++){
				for(my $j=$i+1;$j<$k;$j++){
					if($dist[$i][$j] >= $cutoff_1 and $dist[$i][$j] <= $cutoff_2){
						if($is_lig_atom[$i] eq $is_lig_atom[$j]){
							$edgeCount++;
						}
						else{
							$edgeCount_interChain++;
						}

						my $key1 = $res_name[$i].$atom_name[$i];
						my $key2 = $res_name[$j].$atom_name[$j];

						if($signType == 1){
							if($AtomFeature{$key1}{"Hydro"} and $AtomFeature{$key2}{"Hydro"}){
								if($is_lig_atom[$i] eq $is_lig_atom[$j]){
									$edgeCountHH++;
								}
								else{
									$edgeCountHH_interChain++;
								}
							} elsif(!($AtomFeature{$key1}{"Hydro"}) and !($AtomFeature{$key2}{"Hydro"})){
								if($is_lig_atom[$i] eq $is_lig_atom[$j]){
									$edgeCountPP++;
								}
								else{
									$edgeCountPP_interChain++;
								}
							} else{
								if($is_lig_atom[$i] eq $is_lig_atom[$j]){
									$edgeCountHP++;
								}
								else{
									$edgeCountHP_interChain++;
								}
							}
						}

						elsif($signType == 2){
							if(!$AtomFeature{$key1}){
								$AtomFeature{$key1}{"Neut"} = 1;
							} if(!$AtomFeature{$key2}){
								$AtomFeature{$key2}{"Neut"} = 1;
							}

							if($is_lig_atom[$i] eq $is_lig_atom[$j]){
								my @key_set1 = keys %{$AtomFeature{$key1}};
								my @key_set2 = keys %{$AtomFeature{$key2}};

								for(my $x=0; $x<scalar(@key_set1); $x++){
									for(my $y=0; $y<scalar(@key_set2); $y++){
										my $my_key_set1 = $key_set1[$x];
										my $my_key_set2 = $key_set2[$y];

										$edgeCountTipo2{$my_key_set1.":".$my_key_set2}++;
										if($my_key_set1 ne $my_key_set2){
											$edgeCountTipo2{$my_key_set2.":".$my_key_set1}++;
										}
									}
								}
							}
							else{
								my @key_set1 = keys %{$AtomFeature{$key1}};
								my @key_set2 = keys %{$AtomFeature{$key2}};

								for(my $x=0; $x<scalar(@key_set1); $x++){
									for(my $y=0; $y<scalar(@key_set2); $y++){
										my $my_key_set1 = $key_set1[$x];
										my $my_key_set2 = $key_set2[$y];

										$edgeCountTipo2{$my_key_set1.":".$my_key_set2.":inter"}++;
										if($my_key_set1 ne $my_key_set2){
											$edgeCountTipo2{$my_key_set2.":".$my_key_set1.":inter"}++;
										}
									}
								}
							}
						}
					}
				}
			}

			if($signType == 0){     #mCSM
				push @sign,$edgeCount;
				push @sign_inter,$edgeCount_interChain;
			}
			elsif($signType == 1){  #mCSM-HP
				push @sign,$edgeCountHH;
				push @sign,$edgeCountPP;
				push @sign,$edgeCountHP;

				push @sign_inter,$edgeCountHH_interChain;
				push @sign_inter,$edgeCountPP_interChain;
				push @sign_inter,$edgeCountHP_interChain;
			}
			elsif($signType == 2){  #mCSM-ALL
				@keys = sort(keys(%mcsm_all));
				for($a=0; $a<scalar(@keys); $a++){
					for($b=$a; $b<scalar(@keys); $b++){
						push @sign, $edgeCountTipo2{$keys[$a].":".$keys[$b]};
						push @sign_inter, $edgeCountTipo2{$keys[$a].":".$keys[$b].":inter"};
					}
				}
			}
		}
	}

	# Printing signatures
	foreach my $value (@sign){
		print OUTFILE $value, ",";
	}
	foreach my $value (@sign_inter){
		print OUTFILE $value, ",";
	}

	undef @sign;
	undef @sign_inter;

	printf OUTFILE "\n";
}
close OUTFILE;

exit;
# -------------------------------------------------------------------------------------------------
sub trim{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

sub distance{
	my($x1,$y1,$z1,$x2,$y2,$z2) = @_;
	my $distance;

	$distance = sqrt(($x1-$x2)**2 + ($y1-$y2)**2 + ($z1-$z2)**2);
	return $distance;
}

sub res_cod3_to_res_cod1{
	my $cod3 = shift;

	if($cod3 eq "ALA"){
		return "A";
	} elsif($cod3 eq "VAL"){
		return "V";
	} elsif($cod3 eq "LEU"){
		return "L";
	} elsif($cod3 eq "GLY"){
		return "G";
	} elsif($cod3 eq "SER"){
		return "S";
	} elsif($cod3 eq "TRP"){
		return "W";
	} elsif($cod3 eq "THR"){
		return "T";
	} elsif($cod3 eq "GLN"){
		return "Q";
	} elsif($cod3 eq "GLU"){
		return "E";
	} elsif($cod3 eq "CYS"){
		return "C";
	} elsif($cod3 eq "ARG"){
		return "R";
	} elsif($cod3 eq "PRO"){
		return "P";
	} elsif($cod3 eq "ASP"){
		return "D";
	} elsif($cod3 eq "PHE"){
		return "F";
	} elsif($cod3 eq "ILE"){
		return "I";
	} elsif($cod3 eq "HIS"){
		return "H";
	} elsif($cod3 eq "ASN"){
		return "N";
	} elsif($cod3 eq "MET"){
		return "M";
	} elsif($cod3 eq "TYR"){
		return "Y";
	} elsif($cod3 eq "LYS"){
		return "K";
	}
	return "ERRO";
}
