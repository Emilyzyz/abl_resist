#!/usr/bin/python

# ********************************************************
# *   ----------------------------------------------     *
# * Douglas Eduardo Valente Pires - dpires@dcc.ufmg.br   *
# * www.dcc.ufmg.br/~dpires                              *
# * Last modification :: 05/05/2015                      *
# *   ----------------------------------------------     *
# ********************************************************

import os
import sys
from rdkit import Chem
from rdkit.Chem import Descriptors

# MAIN
if __name__ == '__main__':

	header = ["MolWt","HeavyAtomCount","MolLogP","NumHAcceptors","NumHDonors","NumHeteroatoms","NumRotatableBonds","RingCount","LabuteASA","TPSA"]

	props = header

	smiles_str = sys.argv[1]
	smiles_str = smiles_str.strip()
	molecule = Chem.MolFromSmiles(smiles_str)

	if molecule:

		prop_output = []

		for prop in props:
			prop_output.append(getattr(Descriptors, prop)(molecule))

	# SAME AS ABOVE, BUT USING A GENERATOR EXPRESSION TO `str` EVERYTHING
	print(','.join(str(x) for x in prop_output))
