#!/usr/bin/python

# ********************************************************
# *   ----------------------------------------------     *
# * Douglas Eduardo Valente Pires - dpires@dcc.ufmg.br   *
# * www.dcc.ufmg.br/~dpires                              *
# * Last modification :: 05/05/2015                      *
# *   ----------------------------------------------     *
# ********************************************************

import os
import sys
from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.Chem import ChemicalFeatures
from rdkit import RDConfig

# MAIN
if __name__ == '__main__':

	fdefName = os.path.join(RDConfig.RDDataDir,'BaseFeatures.fdef')
	factory = ChemicalFeatures.BuildFeatureFactory(fdefName)

	infile = sys.argv[1]
	molecule = Chem.MolFromMolFile(infile)

	if molecule:

		feature_set = factory.GetFeaturesForMol(molecule)

		for feature in feature_set:

			print (feature.GetFamily() + "\t" + str(feature.GetAtomIds()))
