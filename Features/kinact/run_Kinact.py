from bs4 import BeautifulSoup

import csv
import os
import requests
import sys
import time

BASE_URL = 'http://biosig.unimelb.edu.au/kinact'

def submit_prediction(pdb_file, mutation, chain):
    pdb_to_submit = {"wild": open(pdb_file, 'r')}

    params = {
        "mutation": mutation,
        "chain": chain,
        "sequence": """>sp|P00519|ABL1_HUMAN Tyrosine-protein kinase ABL1 OS=Homo sapiens OX=9606 GN=ABL1 PE=1 SV=4
MLEICLKLVGCKSKKGLSSSSSCYLEEALQRPVASDFEPQGLSEAARWNSKENLLAGPSE
NDPNLFVALYDFVASGDNTLSITKGEKLRVLGYNHNGEWCEAQTKNGQGWVPSNYITPVN
SLEKHSWYHGPVSRNAAEYLLSSGINGSFLVRESESSPGQRSISLRYEGRVYHYRINTAS
DGKLYVSSESRFNTLAELVHHHSTVADGLITTLHYPAPKRNKPTVYGVSPNYDKWEMERT
DITMKHKLGGGQYGEVYEGVWKKYSLTVAVKTLKEDTMEVEEFLKEAAVMKEIKHPNLVQ
LLGVCTREPPFYIITEFMTYGNLLDYLRECNRQEVNAVVLLYMATQISSAMEYLEKKNFI
HRDLAARNCLVGENHLVKVADFGLSRLMTGDTYTAHAGAKFPIKWTAPESLAYNKFSIKS
DVWAFGVLLWEIATYGMSPYPGIDLSQVYELLEKDYRMERPEGCPEKVYELMRACWQWNP
SDRPSFAEIHQAFETMFQESSISDEVEKELGKQGVRGAVSTLLQAPELPTKTRTSRRAAE
HRDTTDVPEMPHSKGQGESDPLDHEPAVSPLLPRKERGPPEGGLNEDERLLPKDKKTNLF
SALIKKKKKTAPTPPKRSSSFREMDGQPERRGAGEEEGRDISNGALAFTPLDTADPAKSP
KPSNGAGVPNGALRESGGSGFRSPHLWKKSSTLTSSRLATGEEEGGGSSSKRFLRSCSAS
CVPHGAKDTEWRSVTLPRDLQSTGRQFDSSTFGGHKSEKPALPRKRAGENRSDQVTRGTV
TPPPRLVKKNEEAADEVFKDIMESSPGSSPPNLTPKPLRRQVTVAPASGLPHKEEAGKGS
ALGTPAAAEPVTPTSKAGSGAPGGTSKGPAEESRVRRHKHSSESPGRDKGKLSRLKPAPP
PPPAASAGKAGGKPSQSPSQEAAGEAVLGAKTKATSLVDAVNSDAAKPSQPGEGLKKPVL
PATPKPQSAKPSGTPISPAPVPSTLPSASSALAGDQPSSTAFIPLISTRVSLRKTRQPPE
RIASGAITKGVVLDSTEALCLAISRNSEQMASHSAVLEAGKNLYTFCVSYVDSIQQMRNK
FAFREAINKLENNLRELQICPATAGSGPAATQDFSKLLSSVKEISDIVQR""",
    }

    #protein_seq = {"sequence": sequence}
    url_to_submit = "{}/run_prediction".format(BASE_URL)
    page_output = requests.post(url_to_submit, files=pdb_to_submit, data=params)

    soup = BeautifulSoup(page_output.text, 'html.parser')
    #print(soup)
    #print(soup.prettify())
    submission_id = soup.find_all('meta', attrs={'http-equiv':"refresh"})[0].attrs['content'].split()[-1].split('/')[-1]

    return submission_id

def check_prediction_status(submission_id):
    results_url = "{}/results/{}".format(BASE_URL, submission_id)

    page_output = requests.get(results_url)
    soup = BeautifulSoup(page_output.text, 'html.parser')

    is_waiting_page = soup.find_all('meta', attrs={'http-equiv':"refresh"})
    if is_waiting_page:
        return False

    return True

def retrieve_results(submission_id):
    results_url = "{}/results/{}".format(BASE_URL, submission_id)

    page_output = requests.get(results_url)
    soup = BeautifulSoup(page_output.text, 'html.parser')

    results = dict()
    try:
        results['kinact'] = soup.find("h3",style="color:#DB1414;").text.split()[0]
    except:
        results['kinact'] = 'Non-activating'

    return results

def main():
    pdb_file = sys.argv[1]
    mutation = sys.argv[2]
    chain = sys.argv[3]
    submission_id = submit_prediction(pdb_file, mutation, chain)

    while(True):
        time.sleep(30)
        is_job_ready = check_prediction_status(submission_id)
        if is_job_ready:
            break
        #print("Still processing")

    results = retrieve_results(submission_id)
    print("\n")
    print(pdb_file + " " + mutation)
    print("\t".join(results.keys()))
    print("\t".join(results.values()))

    return True

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("*****************[Instructions]*****************************\n")
        print("\t$ python run_Kinact.py file.pdb mutation chain")
        print("\n")
        print("********************[Example]*******************************\n")
        print("\t$ python run_Kinact.py 1U46.pdb E346K A")
        sys.exit(1)

    main()
