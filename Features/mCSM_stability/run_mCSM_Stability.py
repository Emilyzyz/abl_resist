from bs4 import BeautifulSoup

import csv
import os
import requests
import sys
import time

BASE_URL = 'http://biosig.unimelb.edu.au/mcsm'

def submit_prediction(pdb_file, mutation, chain):
    pdb_to_submit = {"wild": open(pdb_file, 'r')}
    params = {
        "mutation": mutation,
        "chain": chain,

    }

    url_to_submit = "{}/stability_prediction".format(BASE_URL)
    page_output = requests.post(url_to_submit, files=pdb_to_submit, data=params)

    soup = BeautifulSoup(page_output.text, 'html.parser')
    submission_id = soup.find_all('meta', attrs={'http-equiv':"refresh"})[0].attrs['content'].split()[-1].split('/')[-1]

    return submission_id
def check_prediction_status(submission_id):
    results_url = "{}/results_stability_prediction/{}".format(BASE_URL, submission_id)

    page_output = requests.get(results_url)
    soup = BeautifulSoup(page_output.text, 'html.parser')

    is_waiting_page = soup.find_all('meta', attrs={'http-equiv':"refresh"})
    if is_waiting_page:
        return False

    return True

def retrieve_results(submission_id):
    results_url = "{}/results_stability_prediction/{}".format(BASE_URL, submission_id)

    page_output = requests.get(results_url)
    soup = BeautifulSoup(page_output.text, 'html.parser')

    results = dict()
    results['ddg_stability'] = soup.find("font",size="4").text.split()[0]

    return results

def main():
    pdb_file = sys.argv[1]
    mutation = sys.argv[2]
    chain = sys.argv[3]

    submission_id = submit_prediction(pdb_file, mutation, chain)

    while(True):
        time.sleep(5)
        is_job_ready = check_prediction_status(submission_id)
        if is_job_ready:
            break
        #print("Still processing")

    results = retrieve_results(submission_id)
    #print("\t".join(results.keys()))
    #print('ddg_stability\n')
    print("\t".join(results.values()))
    #print(results['pdb_file'+','+'mutation'+','+'ddg_stability'])

    return True

if __name__ == "__main__":
    if len(sys.argv) != 4 :
        print("*****************[Instructions]*****************************\n")
        print("\t$ python run_mCSM_Stability.py file.pdb mutation chain")
        print("\n")
        print("********************[Example]*******************************\n")
        print("\t$ python run_mCSM_Stability.py 1U46.pdb E346K A")
        sys.exit(1)

    main()
