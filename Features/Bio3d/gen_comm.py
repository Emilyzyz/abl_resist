


# -------------------------------------------------------------------------------------------------------------
# Using readlines()
file1 = open('mutation_list.txt', 'r')
Lines = file1.readlines()

count = 0
# Strips the newline character
for line in Lines:
    curr_line = line.split(",")
    pdb_file = str(curr_line[0])
    res_num = str(curr_line[1][1:4])
    chain = str(curr_line[2].strip())
    print("python run_bio3d.py /home/yunzhuoz/Desktop/ABL_resist/Structures/{} {} {}".format(pdb_file, res_num, chain))
