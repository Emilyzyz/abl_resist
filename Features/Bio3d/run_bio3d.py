from subprocess import Popen, PIPE, check_output

import os
import sys

#VIRTUALENV_PATH = '/home/sportelli/anaconda3/envs/steph3.6'
#CODE_FOLDER = '/home/sportelli/Desktop/Important_Code/structural/servers'

class Bio3D(object):
    def __init__(self,pdb_file,mutation,chain):
        self.pdb_file = pdb_file
        # self.wild_type = mutation[0]
        self.resnumber = mutation
        # self.mutant = mutation[-1]
        self.chain = chain

    def run(self):
        dict_return = dict()
        #output = check_output(["{}/bin/Rscript".format(VIRTUALENV_PATH), "{}/run_deformation.R".format(CODE_FOLDER), self.pdb_file, self.resnumber, self.chain, 'calpha'])
        output = check_output(["Rscript", "run_deformation.R", self.pdb_file, self.resnumber, self.chain, 'calpha'])
        dict_return['deformation'] = float(output.decode('utf-8').split('\n')[-1])

        output = check_output(["Rscript", "run_fluctuation.R", self.pdb_file, self.resnumber, self.chain, 'calpha'])
        dict_return['fluctuation'] = float(output.decode('utf-8').split('\n')[-1])

        return dict_return

def main():
    pdb_file = sys.argv[1]
    mutation = sys.argv[2]
    chain = sys.argv[3]

    bio3d_results = Bio3D(pdb_file,mutation,chain).run()
    print("{},{}".format(bio3d_results['deformation'],bio3d_results['fluctuation']))

if __name__ == "__main__":
    main()
