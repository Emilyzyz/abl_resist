#!/usr/bin/perl -w 
# ***********************************
# * ------------------------------- *
# * Douglas Eduardo Valente Pires   *
# * douglas.pires@cpqrr.fiocruz.br  *
# * ------------------------------- *
# * Last modification :: 22/10/2018 *
# * ------------------------------- *
# ***********************************

use strict;
use warnings;

sub trim;
sub distance;
sub res_cod3_to_res_cod1;

# ____________________________________________________________________________________________________________________
# Input parameters
my $infile_mut = $ARGV[0];

if(scalar(@ARGV) != 1){
print "___________________________________________________________________________________
SINTAX:
	perl res_depth.pl <mutation_file>
___________________________________________________________________________________\n";
	exit;
}

# ____________________________________________________________________________________________________________________

open(MUT,"<$infile_mut") or die "$!Erro ao abrir: $infile_mut\n";
my @mut = <MUT>;
close MUT;


# -------------------------------------------------------------------------------------------
# Parse header
my $header = shift @mut;
chomp($header);

my @columns = split("\t", $header);
my %col_dict;

for(my$i=0; $i<scalar(@columns); $i++){
        $col_dict{$columns[$i]} = $i;
}


print "ResDepth\n";

foreach my $line_mut (@mut){
	chomp($line_mut);

	my @tokens = split("\t", $line_mut);
	my $pdb_file = $tokens[$col_dict{"PDB"}];
	my $mutation = $tokens[$col_dict{"MUTATION"}];
	my $chain = $tokens[$col_dict{"CHAIN"}];

	my $wild_res = substr($mutation, 0, 1);
	my $wild_res_pos = substr($mutation, 1, length($mutation)-2);
	my $mutated_res = substr($mutation, length($mutation)-1, 1);

	
	my $depth = `python calc_res_depth/res_depth.py $pdb_file $wild_res_pos $chain`;
	
	print $depth;
}

exit;
# ____________________________________________________________________________________________________________________
