#!/usr/bin/perl -w 
# ********************************************************
# *   ----------------------------------------------     *
# *   ----------------------------------------------     *
# ********************************************************

use strict;
use warnings;

sub trim;
sub distance;

# ____________________________________________________________________________________________________________________
# Input parameters
my $infile_mut = $ARGV[0];
my $infile_pdb = $ARGV[1];

if(scalar(@ARGV) != 2){
print "___________________________________________________________________________________
SINTAX:
	perl dist_mutation_to_interface.pl <mutation_file> <pdb_file>
___________________________________________________________________________________\n";
	exit;
}

# ____________________________________________________________________________________________________________________

open(OUT,">$infile_mut.dist_lig") or die "$!Erro ao abrir: $infile_mut.dist_lig\n";


open(MUT,"<$infile_mut") or die "$!Erro ao abrir: $infile_mut\n";
my @mut = <MUT>;
close MUT;

open(PDB,"<$infile_pdb") or die "$!Erro ao abrir: $infile_pdb\n";
my @pdb = <PDB>;
close PDB;

my %res_ok;
foreach my $mutation (@mut){
        chomp($mutation);

        my $wild_res = substr($mutation, 0, 1);
        my $wild_res_pos = substr($mutation, 1, length($mutation)-2);
        my $mutated_res = substr($mutation, length($mutation)-1, 1);

	$res_ok{$wild_res_pos} = 1;
}


my $k = 0;   
my @coord_x;            my @coord_y;        
my @coord_z;            my @res_num;
my @res_name;   	my @min_dist;        
my @chain;

# ==================================================================================================
foreach my $line (@pdb){
        if(trim(substr($line,0,6)) eq "HETATM"){


	                my $res_cod = res_cod3_to_res_cod1(trim(substr($line,17,3)));
	                my $res_ind = trim(substr($line,22,4));
	                my $x = trim(substr($line,30,8));
	                my $y = trim(substr($line,38,8));
	                my $z = trim(substr($line,46,8));
	
	                $coord_x[$k] = $x;
	                $coord_y[$k] = $y;
	                $coord_z[$k] = $z;
	
       	  		$res_num[$k] = $res_ind;
                	$res_name[$k] = $res_cod;
		
			$chain[$k] = substr($line,21,1);
	
                	$k++;
        }
}

my $k2 = 0;
my @coord_x2;            my @coord_y2;
my @coord_z2;            my @res_num2;
my @res_name2;           my @min_dist2;
my @chain2;

foreach my $line (@pdb){
        if(trim(substr($line,0,6)) eq "ATOM"){
                my $res_cod = res_cod3_to_res_cod1(trim(substr($line,17,3)));
                my $res_ind = trim(substr($line,22,4));
                my $x = trim(substr($line,30,8));
                my $y = trim(substr($line,38,8));
                my $z = trim(substr($line,46,8));

		if(defined($res_ok{$res_ind})){

                	$coord_x2[$k2] = $x;
                	$coord_y2[$k2] = $y;
                	$coord_z2[$k2] = $z;

                	$res_num2[$k2] = $res_ind;
                	$res_name2[$k2] = $res_cod;

                	$chain2[$k2] = substr($line,21,1);

                	$k2++;
		}
        }
}

#print "$k2\t$k\n";
#print "Calculating distances\n";

# ==================================================================================================

for(my $i=0; $i<$k2; $i++){

       #print "$i out of $k2\n";

	for(my $j=0; $j<$k; $j++){

		my $dist = distance($coord_x2[$i],$coord_y2[$i],$coord_z2[$i],$coord_x[$j],$coord_y[$j],$coord_z[$j]);

		my $res_ind1 = $res_num2[$i];
		my $res_ind2 = $res_num[$j];

		#if($chain2[$i] ne $chain[$j]){

			if(!defined($min_dist[$res_ind1])){
				$min_dist[$res_ind1] = $dist;
			}
			else{
				if($min_dist[$res_ind1] > $dist){
					$min_dist[$res_ind1] = $dist;
				}
			}


                        if(!defined($min_dist[$res_ind2])){
                                $min_dist[$res_ind2] = $dist;
                        }
                        else{
                                if($min_dist[$res_ind2] > $dist){
                                        $min_dist[$res_ind2] = $dist;
                                }
                        }
		#}
	}
}

#print "Getting distances for mutations\n";

foreach my $mutation (@mut){
	chomp($mutation);

	my $wild_res = substr($mutation, 0, 1);
	my $wild_res_pos = substr($mutation, 1, length($mutation)-2);
	my $mutated_res = substr($mutation, length($mutation)-1, 1);

	print OUT $mutation, "\t", $min_dist[$wild_res_pos], "\n";
}

close OUT;

exit;
# ____________________________________________________________________________________________________________________
sub trim{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

sub distance{
	my($x1,$y1,$z1,$x2,$y2,$z2) = @_;
	my $distance;
	
	$distance = sqrt(($x1-$x2)**2 + ($y1-$y2)**2 + ($z1-$z2)**2);
	return $distance;
}

sub res_cod3_to_res_cod1{
	my $cod3 = shift;
		
	if($cod3 eq "ALA"){
		return "A";
	} elsif($cod3 eq "VAL"){
		return "V";
	} elsif($cod3 eq "LEU"){
		return "L";
	} elsif($cod3 eq "GLY"){
		return "G";
	} elsif($cod3 eq "SER"){
		return "S";
	} elsif($cod3 eq "TRP"){
		return "W";
	} elsif($cod3 eq "THR"){
		return "T";
	} elsif($cod3 eq "GLN"){
		return "Q";
	} elsif($cod3 eq "GLU"){
		return "E";
	} elsif($cod3 eq "CYS"){
		return "C";
	} elsif($cod3 eq "ARG"){
		return "R";
	} elsif($cod3 eq "PRO"){
		return "P";
	} elsif($cod3 eq "ASP"){
		return "D";
	} elsif($cod3 eq "PHE"){
		return "F";
	} elsif($cod3 eq "ILE"){
		return "I";
	} elsif($cod3 eq "HIS"){
		return "H";
	} elsif($cod3 eq "ASN"){
		return "N";
	} elsif($cod3 eq "MET"){
		return "M";
	} elsif($cod3 eq "TYR"){
		return "Y";
	} elsif($cod3 eq "LYS"){
		return "K";
	}
	return "ERRO";
}
