#install.packages('treeheatr')
library(ggplot2)
library(ggsignif)
library(data.table)
library(magrittr)
library(treeheatr)

d <- fread("filtered_train.csv")
test <- fread("filtered_test.csv")
heat_tree(d,target_lab = 'Phenotype', task = "classification")
          

treeheatr(d,target_lab = 'DDG_exp',task = "regression")