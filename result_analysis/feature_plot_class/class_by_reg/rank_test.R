library(ggplot2)
library(dplyr)
library(hrbrthemes)
library(viridis)
library(data.table)

d <- fread("boxplot.csv")
d <- as.data.frame(d)
        
vals = colnames(d)[c(1:10)] # Set of values to compare with phenotype
comps = c('Susceptible', 'Resistant')  # Comparison for groups / t-test
# sample size
sample_size = d %>% group_by(Phenotype) %>% summarize(num=n())
      
for (val in vals) {
    t <- wilcox.test(d[[val]] ~d$Phenotype)
    print(val)
    print(t$p.value)
    
    dp <- ggplot(d, aes(x=Phenotype, y='ATP_Inter-Neut:Pos-5.00', fill=Phenotype)) + 
      geom_violin(trim=FALSE)+
      geom_boxplot(width=0.1, fill="white")+
      labs(title="Plot of ",x="Phenotype", y = "ATP_Inter-Neut:Pos-5.00")
    dp + theme_classic()
}
        
t <- wilcox.test(d$`Inter-Don:Hydro-4.00`[d$`ATP_Inter-Neut:Pos-5.00`[d$`Hydro:Neut-5.00`>425] > 3] ~d$Phenotype[d$`ATP_Inter-Neut:Pos-5.00`[d$`Hydro:Neut-5.00`>425] > 3])
print('Inter-Don:Hydro-4.00')
print(t$p.value)
hist(d$`Inter-Don:Hydro-4.00`[d$`ATP_Inter-Neut:Pos-5.00`[d$`Hydro:Neut-5.00`[d$Phenotype=='Resistant']>425] > 3])