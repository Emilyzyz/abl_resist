
import pandas as pd

# -------------------------------------------------------------------------------------------------------------
mut_file = open('forward_mutation_list.txt', 'r').readlines()

others_mut_list = []
ATP_mut_list = []

for i in range(144):
    others_mut_list.append(mut_file[i].strip())

for i in range(144,175):
    ATP_mut_list.append(mut_file[i].strip())

ATP_ind = []
for mut in others_mut_list:
    curr_pos = 0
    for atp_mut in ATP_mut_list:
        if mut == atp_mut:
            ATP_ind.append(curr_pos)
            break
        curr_pos += 1

ATP = pd.read_csv('forward_ATP_features_ALL_true.csv')
others = pd.read_csv('forward_features_ALL_true.csv')

ATP_feature_names = ATP.columns[1:]

for col in ATP_feature_names:
    new_atp_feature = []
    for ind in ATP_ind:
        new_atp_feature.append(list(ATP.loc[:,col])[ind])
    others['ATP_' + str(col)] = new_atp_feature

#for i in range(144):
    #curr_mut = others.loc[:,'pdb_lig_mut'][i].split('_')[-1]
    #ATP_ind = ATP.loc[ATP['pdb_lig_mut'] == '2g1t_ATP_' + curr_mut]
    #ATP_features = ATP_mut.iloc[:,1:]
    #print(ATP_features)
    #others[i][list(ATP_features.keys())] = ATP_features
#print(forward.astype(bool).sum(axis=0))
#selection = backward.loc[:,header]
others.to_csv('forward_merged_features_ALL_true.csv', index = False)
