
import pandas as pd

# -------------------------------------------------------------------------------------------------------------


forward = pd.read_csv('forward_merged_features_ALL_true.csv')


backward = pd.read_csv('reverse_merged_features_ALL_true.csv')
print(len(forward.columns))

redun_forward = list(forward.columns[forward.astype(bool).sum(axis=0) < 34])
redun_backward = list(backward.columns[backward.astype(bool).sum(axis=0) < 34])

intersection_set = set.intersection(set(redun_forward), set(redun_backward))
final_redun = list(intersection_set)
#selection = backward.loc[:,header]
#selection.to_csv('back_output.csv', index = False)
print(final_redun)

forward.drop(columns = final_redun, inplace=True)
backward.drop(columns = final_redun, inplace=True)
print(len(forward.columns))

forward.to_csv('forward_cleaned_features_ALL_true.csv', index = False)
backward.to_csv('reverse_cleaned_features_ALL_true.csv', index = False)
