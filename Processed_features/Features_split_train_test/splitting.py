# Used for a non-redundant dataset split according to position #

# -*- coding: utf-8 -*-
## requires a column with residue position: 'pos' ##

import numpy as np
import pandas as pd
import random
#import sys
from sklearn.model_selection import GroupShuffleSplit

full_dataset = pd.read_csv('forward_cleaned_features_ALL_true.csv', sep=',')
train_file = open('forward_train_index.txt', 'r').readlines()

train_id = []
for line in train_file:
    #train_id.append(line.strip()[:-5] + line.strip()[-1] + line.strip()[-4:-1] + line.strip()[-5])
    train_id.append(line.strip())
print(train_id)
full_dataset.loc[full_dataset['pdb_lig_mut'].isin(train_id)].to_csv('forward_train_70_ALL_true.csv', index=False)
full_dataset.loc[~full_dataset['pdb_lig_mut'].isin(train_id)].to_csv('forward_blind_30_ALL_true.csv', index=False)
