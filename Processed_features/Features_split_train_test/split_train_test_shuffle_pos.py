# Used for a non-redundant dataset split according to position #

# -*- coding: utf-8 -*-
## requires a column with residue position: 'pos' ##

import numpy as np
import pandas as pd
import random
#import sys
from sklearn.model_selection import GroupShuffleSplit

full_dataset = pd.read_csv('forward_cleaned_features.csv', sep=',')
mut_file = open('forward_mut_list.txt', 'r').readlines()

mut_pos = []

for i in range(144):
    mut_pos.append(mut_file[i].strip()[1:4])
print(mut_pos)
label = full_dataset['Phenotype']

gss = GroupShuffleSplit(n_splits=20, train_size=.7, random_state=1) #split combinations will stop when criteria are met - last combination saved to files
for train_idx, test_idx in gss.split(full_dataset, label, mut_pos): #groups mutation list according to position (so that all mutations at same position are kept together)
	print(full_dataset.loc[train_idx]['Phenotype'].value_counts())
	#if full_dataset.loc[train_idx]['Phenotype'].value_counts()['Cancer'] >= 38 and full_dataset.loc[train_idx]['Phenotype'].value_counts()['Cancer'] <= 42: # criteria for cancer (optional)
		#if full_dataset.loc[train_idx]['Phenotype'].value_counts()['ASD'] >= 27 and full_dataset.loc[train_idx]['Phenotype'].value_counts()['ASD'] <= 31: # criteria for asd (optional)
			#if full_dataset.loc[train_idx]['Phenotype'].value_counts()['Non-Pathogenic'] >= 13 and full_dataset.loc[train_idx]['Phenotype'].value_counts()['Non-Pathogenic'] <= 17: # criteria for np (optional)
	full_dataset.loc[train_idx].to_csv('forward_train_70.csv', index=False)
	full_dataset.loc[test_idx].to_csv('forward_blind_30.csv', index=False)
	#sys.exit()
