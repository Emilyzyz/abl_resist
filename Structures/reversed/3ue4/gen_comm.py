


# -------------------------------------------------------------------------------------------------------------
# Using readlines()
file1 = open('3UE4_mutations_list.txt', 'r')
Lines = file1.readlines()

count = 0
resdict = {
    "R": "ARG",
    "H": "HIS",
    "K": "LYS",
    'D': 'ASP',
    'E': 'GLU',
    'S':

}

# Strips the newline character
for line in Lines:
    res = line.strip()[4]

    print("python mutate_model.py 3ue4 A{} /3ue4/3ue4.pdb /3ue4/3ue4_{}.pdb 5".format(line.strip()[1:],line.strip()))
