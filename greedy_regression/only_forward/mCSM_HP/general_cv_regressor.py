# ********************************************************
# *               University of Melborune                *
# *   ----------------------------------------------     *
# * Yoochan Myung - ymyung@student.unimelb.edu.au        *
# * Last modification :: 19/08/2019                      *
# *   ----------------------------------------------     *
# ********************************************************
import pandas as pd
import numpy as np
import scipy as sp
import time
import sys
import os
import re
import argparse
from math import sqrt
from scipy import stats

from sklearn.ensemble import GradientBoostingRegressor
from xgboost.sklearn import XGBRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import KFold, RepeatedKFold, GroupKFold
from sklearn.metrics import mean_squared_error
from sklearn import svm
from sklearn.neural_network import MLPRegressor
from sklearn.externals import joblib
from sklearn.tree import export_graphviz

timestr = time.strftime("%Y%m%d_%H%M%S")

def runML(algorithm, fname, training_set, outerblind_set, output_result_dir, label_name, error_type, n_cores,
          num_of_shuffling, random_state, scatter, num_folds, cv_type, group_column):
    result_ML = pd.DataFrame()
    distance_analysis = False
    training_ID = pd.DataFrame(training_set['ID'])

    if '!distance!' in training_set.columns.tolist():
        distance_analysis = True
        mask = (training_set['!distance!'] <= 10.000) & (6.000 <= training_set['!distance!'])
        training_less_6ang_ID = training_set['ID'].loc[training_set['!distance!'] < 6.000]
        training_6ang_10ang_ID = training_set['ID'].loc[mask]
        training_greater_10ang_ID = training_set['ID'].loc[10.000 < training_set['!distance!']]
        training_set = training_set.drop('!distance!', axis=1)

    train_label = np.array(training_set[label_name])
    train_group = None
    if(cv_type == "group"):
        train_group = np.array(training_set[group_column])
        training_set = training_set.drop(group_column, axis=1)

    training_set = training_set.drop('ID', axis=1)
    training_features = training_set.drop(label_name, axis=1)
    headers = list(training_features.columns.values)

    if algorithm == 'GB':
        regressor = GradientBoostingRegressor(n_estimators=300, random_state=1)

    elif (algorithm == 'XGBOOST'):
        regressor = XGBRegressor(objective ='reg:squarederror',n_estimators=300, random_state=1, n_jobs=n_cores)

    elif (algorithm == 'RF'):
        regressor = RandomForestRegressor(n_estimators=300, random_state=1, n_jobs=n_cores)

    elif (algorithm == 'M5P'):
        regressor = ExtraTreesRegressor(n_estimators=300, random_state=1, n_jobs=n_cores)

    elif (algorithm == 'GAUSSIAN'):
        regressor = GaussianProcessRegressor(random_state=1)

    elif (algorithm == 'ADABOOST'):
        regressor = AdaBoostRegressor(n_estimators=300, random_state=1)

    elif (algorithm == 'KNN'):
        regressor = KNeighborsRegressor(n_neighbors=5, n_jobs=n_cores)

    elif (algorithm == 'SVR'):
        regressor = svm.SVR(kernel='rbf')

    elif (algorithm == 'NEURAL'):
        regressor = MLPRegressor(random_state=1)
    elif (algorithm == 'J48'):
        regressor = DecisionTreeRegressor(random_state=1)
    else:
        print("Algorithm Selection ERROR!!")
        sys.exit()

    # 10-fold cross validation
    predicted_n_actual_pd = pd.DataFrame(columns=['ID', 'predicted', 'actual', 'fold'])

    if(cv_type == "random"):
        kf = KFold(n_splits=num_folds, shuffle=True, random_state=random_state)
        splits = kf.split(training_features)
    elif(cv_type == "group"):
        kf = GroupKFold(n_splits=num_folds)
        splits = kf.split(training_features, train_label, train_group)

    fold = 1
    for train, test in splits:
        # train and test number(row number) are based on training_features.
        # For example, if '1' from train or test, it would be '1' in training_features

        train_cv_features, test_cv_features, train_cv_label, test_cv_label = training_features.iloc[train], \
                                                                             training_features.iloc[test], train_label[
                                                                                 train], train_label[test]

        if algorithm == 'GB':
            temp_regressor = GradientBoostingRegressor(n_estimators=300, random_state=1)

        elif (algorithm == 'XGBOOST'):
            temp_regressor = XGBRegressor(objective ='reg:squarederror',n_estimators=300, random_state=1, n_jobs=n_cores)

        elif (algorithm == 'RF'):
            temp_regressor = RandomForestRegressor(n_estimators=300, random_state=1, n_jobs=n_cores)

        elif (algorithm == 'M5P'):
            temp_regressor = ExtraTreesRegressor(n_estimators=300, random_state=1, n_jobs=n_cores)

        elif (algorithm == 'GAUSSIAN'):
            temp_regressor = GaussianProcessRegressor(random_state=1)

        elif (algorithm == 'ADABOOST'):
            temp_regressor = AdaBoostRegressor(n_estimators=300, random_state=1)

        elif (algorithm == 'KNN'):
            temp_regressor = KNeighborsRegressor(n_neighbors=5, n_jobs=n_cores)

        elif (algorithm == 'SVR'):
            temp_regressor = svm.SVR(kernel='rbf')

        elif (algorithm == 'NEURAL'):
            temp_regressor = MLPRegressor(random_state=1)

        elif (algorithm == 'J48'):
            temp_regressor = DecisionTreeRegressor(random_state=1)

        temp_regressor.fit(train_cv_features, train_cv_label)
        temp_prediction = temp_regressor.predict(test_cv_features)
        temp_new_pd = pd.DataFrame(
            np.column_stack([training_features.index[test].tolist(), test_cv_label, temp_prediction]),
            columns=['ID', 'actual', 'predicted'])
        temp_new_pd['fold'] = fold
        predicted_n_actual_pd = predicted_n_actual_pd.append(temp_new_pd, ignore_index=True, sort=True)
        fold += 1

    # Get Pearson's correlation of 10-fold C.V. on both original and 10% outlier removed dataset.
    predicted_n_actual_pd.ID += 1
    predicted_n_actual_pd = predicted_n_actual_pd.sort_values('ID')
    predicted_n_actual_pd['old_ID'] = training_ID['ID'].tolist()

    predicted_n_actual_pd['ID'] = predicted_n_actual_pd['ID'].astype(int)
    predicted_n_actual_pd = predicted_n_actual_pd.sort_values('ID')
    predicted_n_actual_pd['error'] = abs(predicted_n_actual_pd['predicted'] - predicted_n_actual_pd['actual'])

    train_slope, train_intercept, train_r_value, train_p_value, train_std_err = stats.linregress(
        predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])
    predicted_n_actual_pd['dist.to_bestfit'] = abs(
        (train_slope * predicted_n_actual_pd['actual']) - predicted_n_actual_pd['predicted'] + train_intercept) / sqrt(
        pow(train_slope, 2) + 1)

    if error_type == 'Absolute':

        predicted_n_actual_pd = predicted_n_actual_pd.sort_values('error', ascending=False)

    elif error_type == 'BestFit':

        predicted_n_actual_pd = predicted_n_actual_pd.sort_values('dist.to_bestfit', ascending=False)

    # Separate results for distance-based analysis
    ten_percent_of_train = int(round(len(predicted_n_actual_pd['ID']) * 0.1))
    training_non_outliers = predicted_n_actual_pd[ten_percent_of_train:]
    training_outliers = predicted_n_actual_pd[:ten_percent_of_train]
    training_outliers.insert(len(training_outliers.columns), 'outlier', 'True')
    # Pearson's correlation
    training_w_outliers_pearsons = round(
        sp.stats.pearsonr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[0], 3)
    training_w_outliers_pearsons_pvalue = round(
        sp.stats.pearsonr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[1], 5)
    training_wo_outliers_pearsons = round(
        sp.stats.pearsonr(training_non_outliers['actual'], training_non_outliers['predicted'])[0], 3)
    training_wo_outliers_pearsons_pvalue = round(
        sp.stats.pearsonr(training_non_outliers['actual'], training_non_outliers['predicted'])[1], 5)
    # Spearman's rank-order correlation
    training_w_outliers_spearman = round(
        sp.stats.spearmanr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[0], 3)
    training_w_outliers_spearman_pvalue = round(
        sp.stats.spearmanr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[1], 5)
    training_wo_outliers_spearman = round(
        sp.stats.spearmanr(training_non_outliers['actual'], training_non_outliers['predicted'])[0], 3)
    training_wo_outliers_spearman_pvalue = round(
        sp.stats.spearmanr(training_non_outliers['actual'], training_non_outliers['predicted'])[1], 5)
    # Point biserial correlation
    training_w_outliers_point_biserial = round(
        sp.stats.pointbiserialr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[0], 3)
    training_w_outliers_point_biserial_pvalue = round(
        sp.stats.pointbiserialr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[1], 5)
    training_wo_outliers_point_biserial = round(
        sp.stats.pointbiserialr(training_non_outliers['actual'], training_non_outliers['predicted'])[0], 3)
    training_wo_outliers_point_biserial_pvalue = round(
        sp.stats.pointbiserialr(training_non_outliers['actual'], training_non_outliers['predicted'])[1], 5)
    # Kendall's tau
    training_w_outliers_kendalltau = round(
        sp.stats.kendalltau(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[0], 3)
    training_w_outliers_kendalltau_pvalue = round(
        sp.stats.kendalltau(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[1], 5)
    training_wo_outliers_kendalltau = round(
        sp.stats.kendalltau(training_non_outliers['actual'], training_non_outliers['predicted'])[0], 3)
    training_wo_outliers_kendalltau_pvalue = round(
        sp.stats.kendalltau(training_non_outliers['actual'], training_non_outliers['predicted'])[1], 5)
    # MSE
    training_w_outliers_mse = round(
        mean_squared_error(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted']), 2)
    training_wo_outliers_mse = round(
        mean_squared_error(training_non_outliers['actual'], training_non_outliers['predicted']), 2)
    # RMSE
    training_w_outliers_rmse = round(
        sqrt(mean_squared_error(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])), 2)
    training_wo_outliers_rmse = round(
        sqrt(mean_squared_error(training_non_outliers['actual'], training_non_outliers['predicted'])), 2)

    # if outlier, it will have 'true' else 'false'
    # training_outliers = training_outliers.drop(['predicted', 'actual', 'dist.to_bestfit', 'old_ID'], axis=1) # Maybe unnecessary
    overall_training = pd.merge(predicted_n_actual_pd, training_outliers[['ID', 'outlier']], how='left',
                                on=['ID', 'ID'])
    overall_training = overall_training.fillna(False)
    overall_training['dist.to_bestfit'] = overall_training['dist.to_bestfit'].round(2)
    overall_training['predicted'] = overall_training['predicted'].round(2)
    overall_training['error'] = overall_training['error'].round(2)

    # Preparation for Evaluation/Assessment steps
    model = regressor.fit(training_features, train_label)
    # model_filename = os.path.join(output_result_dir,fname + '_' + str(random_state) + '_' + timestr + '_model.sav')
    # model_feature_list = os.path.join(output_result_dir,fname + '_' + str(random_state) + '_' + timestr + '_model_feature_order.sav')
    # joblib.dump(model, model_filename)
    # joblib.dump(training_features.columns, model_feature_list)
    # print(train_features.columns.values)

    if error_type == 'Absolute':

        overall_training = overall_training.sort_values('error', ascending=False)
        overall_training.rename(columns={"outlier": "outlier(Absolute)"}, inplace=True)
        overall_training_result_filename = timestr + '_' + fname + '_' + algorithm + "_" + str(
            random_state) + '_10folds_CV_scatter_plot_Absolute_error.csv'

    elif error_type == 'BestFit':

        overall_training.rename(columns={"outlier": "outlier(BestFit)"}, inplace=True)
        overall_training = overall_training.sort_values('dist.to_bestfit', ascending=False)
        overall_training_result_filename = timestr + '_' + fname + '_' + algorithm + "_" + str(
            random_state) + '_10folds_CV_scatter_plot_BestFit_error.csv'

#    if algorithm in ['ADABOOST','M5P','RF']:
#        model_treename = overall_training_result_filename[:-4]+'_tree.dot'
#        estimator = model.estimators_[5]
#        export_graphviz(estimator, out_file=model_treename,feature_names = training_features.columns, filled=True, rounded=True, precision = 2)
#
#        from subprocess import call
#        call(['dot','-Tpng',model_treename,'-o',model_treename[:-3]+'png','-Gdpi=600'])
#    else:
#       pass


    if distance_analysis:
        # TODO: Add Spearman's, Kendall's tau..
        training_less_6ang = overall_training[overall_training['old_ID'].isin(training_less_6ang_ID.tolist())]
        training_less_6ang_pearson = np.corrcoef(training_less_6ang['actual'], training_less_6ang['predicted']).round(3)[0, 1]
        training_less_6ang_point_biserial = round(sp.stats.pointbiserialr(training_less_6ang['actual'], training_less_6ang['predicted'])[0], 3)
        training_less_6ang_rmse = round(mean_squared_error(training_less_6ang['actual'], training_less_6ang['predicted']), 2)

        training_6ang_10ang = overall_training[overall_training['old_ID'].isin(training_6ang_10ang_ID.tolist())]
        training_6ang_10ang_pearson = np.corrcoef(training_6ang_10ang['actual'], training_6ang_10ang['predicted']).round(3)[0, 1]
        training_6ang_10ang_point_biserial = round(sp.stats.pointbiserialr(training_6ang_10ang['actual'], training_6ang_10ang['predicted'])[0], 3)
        training_6ang_10ang_rmse = round(mean_squared_error(training_6ang_10ang['actual'], training_6ang_10ang['predicted']), 2)

        training_greater_10ang = overall_training[overall_training['old_ID'].isin(training_greater_10ang_ID.tolist())]
        training_greater_10ang_pearson = np.corrcoef(training_greater_10ang['actual'], training_greater_10ang['predicted']).round(3)[0, 1]
        training_greater_10ang_point_biserial = round(sp.stats.pointbiserialr(training_greater_10ang['actual'], training_greater_10ang['predicted'])[0], 3)
        training_greater_10ang_rmse = round(mean_squared_error(training_greater_10ang['actual'], training_greater_10ang['predicted']), 2)
        # TODO: Need to Fix
        # all_less_6ang = pd.concat([training_less_6ang, blind_less_6ang])
        # all_6ang_10ang =pd.concat([training_6ang_10ang, blind_6ang_10ang])
        # all_greater_10ang = pd.concat([training_greater_10ang, blind_greater_10ang])
        # all_less_6ang_pearson = np.corrcoef(all_less_6ang['predicted'], all_less_6ang['actual']).round(3)[0,1]
        # all_6ang_10ang_pearson = np.corrcoef(all_6ang_10ang['predicted'], all_6ang_10ang['actual']).round(3)[0, 1]
        # all_greater_10ang_pearson = np.corrcoef(all_greater_10ang['predicted'], all_greater_10ang['actual']).round(3)[0, 1]

    if not isinstance(outerblind_set,str):

        outerblind_set_ID = pd.DataFrame(outerblind_set['ID'])
        outerblind_label = np.array(outerblind_set[label_name])
        outerblind_features = outerblind_set[headers]
        # outerblind_set = outerblind_set.drop('ID', axis=1)
        # outerblind_features = outerblind_set.drop(label_name, axis=1)

        ## outerblind-Test

        outerblind_prediction = temp_regressor.predict(outerblind_features)
        outerblind_pd = pd.DataFrame(np.column_stack([outerblind_set_ID, outerblind_label, outerblind_prediction]),
                                     columns=['ID', 'actual', 'predicted'])

        outerblind_pd['ID'] = outerblind_pd['ID'].astype(int)
        outerblind_pd['old_ID'] = outerblind_set_ID.sort_values('ID')['ID'].tolist()
        outerblind_pd['error'] = abs(outerblind_pd['predicted'] - outerblind_pd['actual'])

        outerblind_slope, outerblind_intercept, outerblind_r_value, outerblind_p_value, outerblind_std_err = stats.linregress(
            outerblind_pd['actual'], outerblind_pd['predicted'])
        outerblind_pd['dist.to_bestfit'] = abs(
            (outerblind_slope * outerblind_pd['actual']) - outerblind_pd['predicted'] + outerblind_intercept) / sqrt(
            pow(outerblind_slope, 2) + 1)

        if error_type == 'Absolute':

            outerblind_pd = outerblind_pd.sort_values('error', ascending=False)
            outerblindtest_result_filename = timestr + '_' + fname + '_' + algorithm + "_" + str(
                random_state) + '_10folds_CV_outerblind-test_scatter_plot_Absolute_error.csv'

        elif error_type == 'BestFit':

            outerblind_pd = outerblind_pd.sort_values('dist.to_bestfit', ascending=False)
            outerblindtest_result_filename = timestr + '_' + fname + '_' + algorithm + "_" + str(
                random_state) + '_10folds_CV_outerblind-test_scatter_plot_Best-Fit_error.csv'

        ten_percent_of_outerblind = int(round(len(outerblind_pd['ID']) * 0.1))

        outerblind_non_outliers = outerblind_pd[ten_percent_of_outerblind:]
        outerblind_outliers = outerblind_pd[:ten_percent_of_outerblind]
        outerblind_outliers.insert(len(outerblind_outliers.columns), 'outlier', 'True')
        # Pearson's correlation
        outerblind_w_outliers_pearsons = round(
            sp.stats.pearsonr(outerblind_pd['actual'], outerblind_pd['predicted'])[0], 3)
        outerblind_w_outliers_pearsons_pvalue = round(
            sp.stats.pearsonr(outerblind_pd['actual'], outerblind_pd['predicted'])[1], 5)
        outerblind_wo_outliers_pearsons = round(
            sp.stats.pearsonr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[0], 3)
        outerblind_wo_outliers_pearsons_pvalue = round(
            sp.stats.pearsonr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[1], 5)
        # Spearman's rank-order correlation
        outerblind_w_outliers_spearman = round(
            sp.stats.spearmanr(outerblind_pd['actual'], outerblind_pd['predicted'])[0], 3)
        outerblind_w_outliers_spearman_pvalue = round(
            sp.stats.spearmanr(outerblind_pd['actual'], outerblind_pd['predicted'])[1], 5)
        outerblind_wo_outliers_spearman = round(
            sp.stats.spearmanr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[0], 3)
        outerblind_wo_outliers_spearman_pvalue = round(
            sp.stats.spearmanr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[1], 5)
        # Point biserial correlation
        outerblind_w_outliers_point_biserial = round(
            sp.stats.pointbiserialr(outerblind_pd['actual'], outerblind_pd['predicted'])[0], 3)
        outerblind_w_outliers_point_biserial_pvalue = round(
            sp.stats.pointbiserialr(outerblind_pd['actual'], outerblind_pd['predicted'])[1], 5)
        outerblind_wo_outliers_point_biserial = round(
            sp.stats.pointbiserialr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[0], 3)
        outerblind_wo_outliers_point_biserial_pvalue = round(
            sp.stats.pointbiserialr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[1], 5)
        # Kendall's tau
        outerblind_w_outliers_kendalltau = round(
            sp.stats.kendalltau(outerblind_pd['actual'], outerblind_pd['predicted'])[0], 3)
        outerblind_w_outliers_kendalltau_pvalue = round(
            sp.stats.kendalltau(outerblind_pd['actual'], outerblind_pd['predicted'])[1], 5)
        outerblind_wo_outliers_kendalltau = round(
            sp.stats.kendalltau(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[0], 3)
        outerblind_wo_outliers_kendalltau_pvalue = round(
            sp.stats.kendalltau(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[1], 5)
        # MSE
        outerblind_w_outliers_mse = round(mean_squared_error(outerblind_pd['actual'],outerblind_pd['predicted']), 2)
        outerblind_wo_outliers_mse = round(mean_squared_error(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted']), 2)
        # RMSE
        outerblind_w_outliers_rmse = round(sqrt(mean_squared_error(outerblind_pd['actual'],outerblind_pd['predicted'])), 2)
        outerblind_wo_outliers_rmse = round(sqrt(mean_squared_error(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])), 2)

        # if outlier, it will have 'true' else 'false'
        # outerblind_outliers = outerblind_outliers.drop(['predicted', 'actual', 'dist.to_bestfit', 'old_ID'], axis=1) # Maybe unnecessary
        overall_outerblind = pd.merge(outerblind_pd, outerblind_outliers[['ID', 'outlier']], how='left',
                                      on=['ID', 'ID'])
        overall_outerblind = overall_outerblind.fillna(False)
        overall_outerblind['dist.to_bestfit'] = overall_outerblind['dist.to_bestfit'].round(2)
        overall_outerblind['predicted'] = overall_outerblind['predicted'].round(2)
        overall_outerblind['error'] = overall_outerblind['error'].round(2)

        if distance_analysis:
            # TODO: Need to fix and add other correlation terms for distance_analysis.

            result_ML = result_ML.append(
                {'filename': fname,
                 'less_6ang(Pearson)' : training_less_6ang_pearson,
                 '6ang_to_10ang(Pearson)': training_6ang_10ang_pearson,
                 'greater_10ang(Pearson)': training_greater_10ang_pearson,
                 'less_6ang(Point_biserial)' : training_less_6ang_point_biserial,
                 '6ang_to_10ang(Point_biserial)': training_6ang_10ang_point_biserial,
                 'greater_10ang(Point_biserial)': training_greater_10ang_point_biserial,
                 'less_6ang(RMSE)' : training_less_6ang_rmse,
                 '6ang_to_10ang(RMSE)': training_6ang_10ang_rmse,
                 'greater_10ang(RMSE)': training_greater_10ang_rmse,
                 '#of(less_6ang)': len(training_less_6ang),
                 '#of(6ang_to_10ang)': len(training_6ang_10ang),
                 '#of(greater_10ang)': len(training_greater_10ang),
                 'Training(Pearson)': training_w_outliers_pearsons,
                 'Training(Pearson_p)': training_w_outliers_pearsons_pvalue,
                 'outerBlind-test(Pearson)': outerblind_w_outliers_pearsons,
                 'outerBlind-test(Pearson_p)': outerblind_w_outliers_pearsons_pvalue,
                 'Training(Spearman)': training_w_outliers_spearman,
                 'Training(Spearman_p)': training_w_outliers_spearman_pvalue,
                 'outerBlind-test(Spearman)': outerblind_w_outliers_spearman,
                 'outerBlind-test(Spearman_p)': outerblind_w_outliers_spearman_pvalue,
                 'Training(Kendall)': training_w_outliers_kendalltau,
                 'Training(Kendall_p)': training_w_outliers_kendalltau_pvalue,
                 'outerBlind-test(Kendall)': outerblind_w_outliers_kendalltau,
                 'outerBlind-test(Kendall_p)': outerblind_w_outliers_kendalltau_pvalue,
                 'Training(Point_biserial)': training_w_outliers_point_biserial,
                 'Training(Point_biserial_p)': training_w_outliers_point_biserial_pvalue,
                 'outerBlind-test(Point_biserial)': outerblind_w_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p)': outerblind_w_outliers_point_biserial_pvalue,
                 'Training(MSE)': training_w_outliers_mse,
                 'Training(RMSE)': training_w_outliers_rmse,
                 'outerBlind-test(MSE)': outerblind_w_outliers_mse,
                 'outerBlind-test(RMSE)': outerblind_w_outliers_rmse,
                 'Training(Pearson_90%)': training_wo_outliers_pearsons,
                 'Training(Pearson_p_90%)': training_wo_outliers_pearsons_pvalue,
                 'outerBlind-test(Pearson_90%)': outerblind_wo_outliers_pearsons,
                 'outerBlind-test(Pearson_p_90%)': outerblind_wo_outliers_pearsons_pvalue,
                 'Training(Spearman_90%)': training_wo_outliers_spearman,
                 'Training(Spearman_p_90%)': training_wo_outliers_spearman_pvalue,
                 'outerBlind-test(Spearman_90%)': outerblind_wo_outliers_spearman,
                 'outerBlind-test(Spearman_p_90%)': outerblind_wo_outliers_spearman_pvalue,
                 'Training(Kendall_90%)': training_wo_outliers_kendalltau,
                 'Training(Kendall_p_90%)': training_wo_outliers_kendalltau_pvalue,
                 'outerBlind-test(Kendall_90%)': outerblind_wo_outliers_kendalltau,
                 'outerBlind-test(Kendall_p_90%)': outerblind_wo_outliers_kendalltau_pvalue,
                 'Training(Point_biserial_90%)': training_wo_outliers_point_biserial,
                 'Training(Point_biserial_p_90%)': training_wo_outliers_point_biserial_pvalue,
                 'outerBlind-test(Point_biserial_90%)': outerblind_wo_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p_90%)': outerblind_wo_outliers_point_biserial_pvalue,
                 'Training(MSE_90%)': training_wo_outliers_mse,
                 'Training(RMSE_90%)': training_wo_outliers_rmse,
                 'outerBlind-test(MSE_90%)': outerblind_wo_outliers_mse,
                 'outerBlind-test(RMSE_90%)': outerblind_wo_outliers_rmse}, ignore_index=True)

        else:

            result_ML = result_ML.append(
                {'filename': fname,
                 'Training(Pearson)': training_w_outliers_pearsons,
                 'Training(Pearson_p)': training_w_outliers_pearsons_pvalue,
                 'outerBlind-test(Pearson)': outerblind_w_outliers_pearsons,
                 'outerBlind-test(Pearson_p)': outerblind_w_outliers_pearsons_pvalue,
                 'Training(Spearman)': training_w_outliers_spearman,
                 'Training(Spearman_p)': training_w_outliers_spearman_pvalue,
                 'outerBlind-test(Spearman)': outerblind_w_outliers_spearman,
                 'outerBlind-test(Spearman_p)': outerblind_w_outliers_spearman_pvalue,
                 'Training(Kendall)': training_w_outliers_kendalltau,
                 'Training(Kendall_p)': training_w_outliers_kendalltau_pvalue,
                 'outerBlind-test(Kendall)': outerblind_w_outliers_kendalltau,
                 'outerBlind-test(Kendall_p)': outerblind_w_outliers_kendalltau_pvalue,
                 'Training(Point_biserial)': training_w_outliers_point_biserial,
                 'Training(Point_biserial_p)': training_w_outliers_point_biserial_pvalue,
                 'outerBlind-test(Point_biserial)': outerblind_w_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p)': outerblind_w_outliers_point_biserial_pvalue,
                 'Training(MSE)': training_w_outliers_mse,
                 'Training(RMSE)': training_w_outliers_rmse,
                 'outerBlind-test(MSE)': outerblind_w_outliers_mse,
                 'outerBlind-test(RMSE)': outerblind_w_outliers_rmse,
                 'Training(Pearson_90%)': training_wo_outliers_pearsons,
                 'Training(Pearson_p_90%)': training_wo_outliers_pearsons_pvalue,
                 'outerBlind-test(Pearson_90%)': outerblind_wo_outliers_pearsons,
                 'outerBlind-test(Pearson_p_90%)': outerblind_wo_outliers_pearsons_pvalue,
                 'Training(Spearman_90%)': training_wo_outliers_spearman,
                 'Training(Spearman_p_90%)': training_wo_outliers_spearman_pvalue,
                 'outerBlind-test(Spearman_90%)': outerblind_wo_outliers_spearman,
                 'outerBlind-test(Spearman_p_90%)': outerblind_wo_outliers_spearman_pvalue,
                 'Training(Kendall_90%)': training_wo_outliers_kendalltau,
                 'Training(Kendall_p_90%)': training_wo_outliers_kendalltau_pvalue,
                 'outerBlind-test(Kendall_90%)': outerblind_wo_outliers_kendalltau,
                 'outerBlind-test(Kendall_p_90%)': outerblind_wo_outliers_kendalltau_pvalue,
                 'Training(Point_biserial_90%)': training_wo_outliers_point_biserial,
                 'Training(Point_biserial_p_90%)': training_wo_outliers_point_biserial_pvalue,
                 'outerBlind-test(Point_biserial_90%)': outerblind_wo_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p_90%)': outerblind_wo_outliers_point_biserial_pvalue,
                 'Training(MSE_90%)': training_wo_outliers_mse,
                 'Training(RMSE_90%)': training_wo_outliers_rmse,
                 'outerBlind-test(MSE_90%)': outerblind_wo_outliers_mse,
                 'outerBlind-test(RMSE_90%)': outerblind_wo_outliers_rmse}, ignore_index=True)

        if scatter != 'False':
            overall_outerblind.to_csv(outerblindtest_result_filename, index=False)

        if num_of_shuffling == random_state:

            outerblind_100_set_ID = pd.DataFrame(outerblind_set['ID'])
            outerblind_100_label = np.array(outerblind_set[label_name])
            outerblind_100_features = outerblind_set[headers]
            # outerblind_100_set = outerblind_100_set.drop('ID', axis=1)
            # outerblind_100_features = outerblind_100_set.drop(label_name, axis=1)

            ## outerblind_100-Test
            outerblind_100_prediction = regressor.predict(outerblind_100_features)
            outerblind_100_pd = pd.DataFrame(np.column_stack([outerblind_100_set_ID, outerblind_100_label, outerblind_100_prediction]),
                                         columns=['ID', 'actual', 'predicted'])

            outerblind_100_pd['ID'] = outerblind_100_pd['ID'].astype(int)
            outerblind_100_pd['old_ID'] = outerblind_100_set_ID.sort_values('ID')['ID'].tolist()
            outerblind_100_pd['error'] = abs(outerblind_100_pd['predicted'] - outerblind_100_pd['actual'])

            outerblind_100_slope, outerblind_100_intercept, outerblind_100_r_value, outerblind_100_p_value, outerblind_100_std_err = stats.linregress(
                outerblind_100_pd['actual'], outerblind_100_pd['predicted'])
            outerblind_100_pd['dist.to_bestfit'] = abs(
                (outerblind_100_slope * outerblind_100_pd['actual']) - outerblind_100_pd['predicted'] + outerblind_100_intercept) / sqrt(
                pow(outerblind_100_slope, 2) + 1)

            if error_type == 'Absolute':

                outerblind_100_pd = outerblind_100_pd.sort_values('error', ascending=False)
                outerblind_100test_result_filename = timestr + '_' + fname + '_' + algorithm + "_" + str('by_whole_model') + '_10folds_CV_outerblind_100-test_scatter_plot_Absolute_error.csv'

            elif error_type == 'BestFit':

                outerblind_100_pd = outerblind_100_pd.sort_values('dist.to_bestfit', ascending=False)
                outerblind_100test_result_filename = timestr + '_' + fname + '_' + algorithm + "_" + str('by_whole_model') + '_10folds_CV_outerblind_100-test_scatter_plot_Best-Fit_error.csv'

            ten_percent_of_outerblind_100 = int(round(len(outerblind_100_pd['ID']) * 0.1))

            outerblind_100_non_outliers = outerblind_100_pd[ten_percent_of_outerblind_100:]
            outerblind_100_outliers = outerblind_100_pd[:ten_percent_of_outerblind_100]
            outerblind_100_outliers.insert(len(outerblind_100_outliers.columns), 'outlier', 'True')
            # Pearson's correlation
            outerblind_100_w_outliers_pearsons = round(
                sp.stats.pearsonr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[0], 3)
            outerblind_100_w_outliers_pearsons_pvalue = round(
                sp.stats.pearsonr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[1], 5)
            outerblind_100_wo_outliers_pearsons = round(
                sp.stats.pearsonr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[0], 3)
            outerblind_100_wo_outliers_pearsons_pvalue = round(
                sp.stats.pearsonr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[1], 5)
            # Spearman's rank-order correlation
            outerblind_100_w_outliers_spearman = round(
                sp.stats.spearmanr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[0], 3)
            outerblind_100_w_outliers_spearman_pvalue = round(
                sp.stats.spearmanr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[1], 5)
            outerblind_100_wo_outliers_spearman = round(
                sp.stats.spearmanr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[0], 3)
            outerblind_100_wo_outliers_spearman_pvalue = round(
                sp.stats.spearmanr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[1], 5)
            # Point biserial correlation
            outerblind_100_w_outliers_point_biserial = round(
                sp.stats.pointbiserialr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[0], 3)
            outerblind_100_w_outliers_point_biserial_pvalue = round(
                sp.stats.pointbiserialr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[1], 5)
            outerblind_100_wo_outliers_point_biserial = round(
                sp.stats.pointbiserialr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[0], 3)
            outerblind_100_wo_outliers_point_biserial_pvalue = round(
                sp.stats.pointbiserialr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[1], 5)
            # Kendall's tau
            outerblind_100_w_outliers_kendalltau = round(
                sp.stats.kendalltau(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[0], 3)
            outerblind_100_w_outliers_kendalltau_pvalue = round(
                sp.stats.kendalltau(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[1], 5)
            outerblind_100_wo_outliers_kendalltau = round(
                sp.stats.kendalltau(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[0], 3)
            outerblind_100_wo_outliers_kendalltau_pvalue = round(
                sp.stats.kendalltau(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[1], 5)
            # MSE
            outerblind_100_w_outliers_mse = round(mean_squared_error(outerblind_100_pd['actual'], outerblind_100_pd['predicted']), 2)
            outerblind_100_wo_outliers_mse = round(
                mean_squared_error(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted']), 2)
            # RMSE
            outerblind_100_w_outliers_rmse = round(
                sqrt(mean_squared_error(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])), 2)
            outerblind_100_wo_outliers_rmse = round(
                sqrt(mean_squared_error(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])), 2)

            # if outlier, it will have 'true' else 'false'
            # outerblind_100_outliers = outerblind_100_outliers.drop(['predicted', 'actual', 'dist.to_bestfit', 'old_ID'], axis=1) # Maybe unnecessary
            overall_outerblind_100 = pd.merge(outerblind_100_pd, outerblind_100_outliers[['ID', 'outlier']], how='left',
                                          on=['ID', 'ID'])
            overall_outerblind_100 = overall_outerblind_100.fillna(False)
            overall_outerblind_100['dist.to_bestfit'] = overall_outerblind_100['dist.to_bestfit'].round(2)
            overall_outerblind_100['predicted'] = overall_outerblind_100['predicted'].round(2)
            overall_outerblind_100['error'] = overall_outerblind_100['error'].round(2)

            result_ML = result_ML.append(
                {'filename': fname,
                 'Training(Pearson)': np.nan,
                 'Training(Pearson_p)': np.nan,
                 'outerBlind-test(Pearson)': outerblind_100_w_outliers_pearsons,
                 'outerBlind-test(Pearson_p)': outerblind_100_w_outliers_pearsons_pvalue,
                 'Training(Spearman)': np.nan,
                 'Training(Spearman_p)': np.nan,
                 'outerBlind-test(Spearman)': outerblind_100_w_outliers_spearman,
                 'outerBlind-test(Spearman_p)': outerblind_100_w_outliers_spearman_pvalue,
                 'Training(Kendall)': np.nan,
                 'Training(Kendall_p)': np.nan,
                 'outerBlind-test(Kendall)': outerblind_100_w_outliers_kendalltau,
                 'outerBlind-test(Kendall_p)': outerblind_100_w_outliers_kendalltau_pvalue,
                 'Training(Point_biserial)': np.nan,
                 'Training(Point_biserial_p)': np.nan,
                 'outerBlind-test(Point_biserial)': outerblind_100_w_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p)': outerblind_100_w_outliers_point_biserial_pvalue,
                 'Training(MSE)': np.nan,
                 'Training(RMSE)': np.nan,
                 'outerBlind-test(MSE)': outerblind_100_w_outliers_mse,
                 'outerBlind-test(RMSE)': outerblind_100_w_outliers_rmse,
                 'Training(Pearson_90%)': np.nan,
                 'Training(Pearson_p_90%)': np.nan,
                 'outerBlind-test(Pearson_90%)': outerblind_100_wo_outliers_pearsons,
                 'outerBlind-test(Pearson_p_90%)': outerblind_100_wo_outliers_pearsons_pvalue,
                 'Training(Spearman_90%)': np.nan,
                 'Training(Spearman_p_90%)': np.nan,
                 'outerBlind-test(Spearman_90%)': outerblind_100_wo_outliers_spearman,
                 'outerBlind-test(Spearman_p_90%)': outerblind_100_wo_outliers_spearman_pvalue,
                 'Training(Kendall_90%)': np.nan,
                 'Training(Kendall_p_90%)': np.nan,
                 'outerBlind-test(Kendall_90%)': outerblind_100_wo_outliers_kendalltau,
                 'outerBlind-test(Kendall_p_90%)': outerblind_100_wo_outliers_kendalltau_pvalue,
                 'Training(Point_biserial_90%)': np.nan,
                 'Training(Point_biserial_p_90%)': np.nan,
                 'outerBlind-test(Point_biserial_90%)': outerblind_100_wo_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p_90%)': outerblind_100_wo_outliers_point_biserial_pvalue,
                 'Training(MSE_90%)': np.nan,
                 'Training(RMSE_90%)': np.nan,
                 'outerBlind-test(MSE_90%)': outerblind_100_wo_outliers_mse,
                 'outerBlind-test(RMSE_90%)': outerblind_100_wo_outliers_rmse}, ignore_index=True)


            if scatter != 'False':
                overall_outerblind_100.to_csv(outerblind_100test_result_filename, index=False)
    else:

        if distance_analysis:
            # TODO: Need to fix and add other correlation terms for distance_analysis.
            result_ML = result_ML.append(
                {'filename': fname,
                 'less_6ang(Pearson)' : training_less_6ang_pearson,
                 '6ang_to_10ang(Pearson)': training_6ang_10ang_pearson,
                 'greater_10ang(Pearson)': training_greater_10ang_pearson,
                 'less_6ang(Point_biserial)' : training_less_6ang_point_biserial,
                 '6ang_to_10ang(Point_biserial)': training_6ang_10ang_point_biserial,
                 'greater_10ang(Point_biserial)': training_greater_10ang_point_biserial,
                 'less_6ang(RMSE)' : training_less_6ang_rmse,
                 '6ang_to_10ang(RMSE)': training_6ang_10ang_rmse,
                 'greater_10ang(RMSE)': training_greater_10ang_rmse,
                 '#of(less_6ang)': len(training_less_6ang),
                 '#of(6ang_to_10ang)': len(training_6ang_10ang),
                 '#of(greater_10ang)': len(training_greater_10ang),
                 'Training(Pearson)': training_w_outliers_pearsons,
                 'Training(Pearson_p)': training_w_outliers_pearsons_pvalue,
                 'Training(Spearman)': training_w_outliers_spearman,
                 'Training(Spearman_p)': training_w_outliers_spearman_pvalue,
                 'Training(Kendall)': training_w_outliers_kendalltau,
                 'Training(Kendall_p)': training_w_outliers_kendalltau_pvalue,
                 'Training(Point_biserial)': training_w_outliers_point_biserial,
                 'Training(Point_biserial_p)': training_w_outliers_point_biserial_pvalue,
                 'Training(MSE)': training_w_outliers_mse,
                 'Training(RMSE)': training_w_outliers_rmse,
                 'Training(Pearson_90%)': training_wo_outliers_pearsons,
                 'Training(Pearson_p_90%)': training_wo_outliers_pearsons_pvalue,
                 'Training(Spearman_90%)': training_wo_outliers_spearman,
                 'Training(Spearman_p_90%)': training_wo_outliers_spearman_pvalue,
                 'Training(Kendall_90%)': training_wo_outliers_kendalltau,
                 'Training(Kendall_p_90%)': training_wo_outliers_kendalltau_pvalue,
                 'Training(Point_biserial_90%)': training_wo_outliers_point_biserial,
                 'Training(Point_biserial_p_90%)': training_wo_outliers_point_biserial_pvalue,
                 'Training(MSE_90%)': training_wo_outliers_mse,
                 'Training(RMSE_90%)': training_wo_outliers_rmse}, ignore_index=True)


        else:

            result_ML = result_ML.append(
                {'filename': fname,
                 'Training(Pearson)': training_w_outliers_pearsons,
                 'Training(Pearson_p)': training_w_outliers_pearsons_pvalue,
                 'Training(Spearman)': training_w_outliers_spearman,
                 'Training(Spearman_p)': training_w_outliers_spearman_pvalue,
                 'Training(Kendall)': training_w_outliers_kendalltau,
                 'Training(Kendall_p)': training_w_outliers_kendalltau_pvalue,
                 'Training(Point_biserial)': training_w_outliers_point_biserial,
                 'Training(Point_biserial_p)': training_w_outliers_point_biserial_pvalue,
                 'Training(MSE)': training_w_outliers_mse,
                 'Training(RMSE)': training_w_outliers_rmse,
                 'Training(Pearson_90%)': training_wo_outliers_pearsons,
                 'Training(Pearson_p_90%)': training_wo_outliers_pearsons_pvalue,
                 'Training(Spearman_90%)': training_wo_outliers_spearman,
                 'Training(Spearman_p_90%)': training_wo_outliers_spearman_pvalue,
                 'Training(Kendall_90%)': training_wo_outliers_kendalltau,
                 'Training(Kendall_p_90%)': training_wo_outliers_kendalltau_pvalue,
                 'Training(Point_biserial_90%)': training_wo_outliers_point_biserial,
                 'Training(Point_biserial_p_90%)': training_wo_outliers_point_biserial_pvalue,
                 'Training(MSE_90%)': training_wo_outliers_mse,
                 'Training(RMSE_90%)': training_wo_outliers_rmse}, ignore_index=True)


    if scatter != 'False':
        overall_training.to_csv(overall_training_result_filename, index=False)

    # predicted_n_actual_pd.to_csv(training_result_filename+"_predicted", index=False)
    # overall_blind.to_csv(blindtest_result_filename, index=False)

    return result_ML

def runMLTransformed(algorithm, fname, training_set, outerblind_set, output_result_dir, label_name, error_type, n_cores,
          num_of_shuffling, random_state, scatter, num_folds, cv_type, group_column):
    result_ML = pd.DataFrame()
    distance_analysis = False
    training_ID = pd.DataFrame(training_set['ID'])

    if '!distance!' in training_set.columns.tolist():
        distance_analysis = True
        mask = (training_set['!distance!'] <= 10.000) & (6.000 <= training_set['!distance!'])
        training_less_6ang_ID = training_set['ID'].loc[training_set['!distance!'] < 6.000]
        training_6ang_10ang_ID = training_set['ID'].loc[mask]
        training_greater_10ang_ID = training_set['ID'].loc[10.000 < training_set['!distance!']]
        training_set = training_set.drop('!distance!', axis=1)

    train_label = np.array(training_set[label_name])

    if(cv_type == "group"):
        train_group = np.array(training_set[group_column])
        training_set = training_set.drop(group_column, axis=1)

    training_set = training_set.drop('ID', axis=1)
    training_features = training_set.drop(label_name, axis=1)
    headers = list(training_features.columns.values)

    if algorithm == 'GB':
        regressor = GradientBoostingRegressor(n_estimators=300, random_state=1)

    elif (algorithm == 'XGBOOST'):
        regressor = XGBRegressor(objective ='reg:squarederror',n_estimators=300, random_state=1, n_jobs=n_cores)

    elif (algorithm == 'RF'):
        regressor = RandomForestRegressor(n_estimators=300, random_state=1, n_jobs=n_cores)

    elif (algorithm == 'M5P'):
        regressor = ExtraTreesRegressor(n_estimators=300, random_state=1, n_jobs=n_cores)

    elif (algorithm == 'GAUSSIAN'):
        regressor = GaussianProcessRegressor(random_state=1)

    elif (algorithm == 'ADABOOST'):
        regressor = AdaBoostRegressor(n_estimators=300, random_state=1)

    elif (algorithm == 'KNN'):
        regressor = KNeighborsRegressor(n_neighbors=5, n_jobs=n_cores)

    elif (algorithm == 'SVR'):
        regressor = svm.SVR(kernel='rbf')

    elif (algorithm == 'NEURAL'):
        regressor = MLPRegressor(random_state=1)

    elif (algorithm == 'J48'):
        regressor = DecisionTreeRegressor(random_state=1)

    else:
        print("Algorithm Selection ERROR!!")
        sys.exit()

    # 10-fold cross validation
    predicted_n_actual_pd = pd.DataFrame(columns=['ID', 'predicted', 'actual', 'fold'])
    #Adding options for the cv:
    kf = None
    splits = None

    if(cv_type == "random"):
        kf = KFold(n_splits=num_folds, shuffle=True, random_state=random_state)
        splits = kf.split(training_features)
    elif(cv_type == "group"):
        kf = GroupKFold(n_splits=num_folds)
        splits = kf.split(training_features, train_label, train_group)

    fold = 1 # for indexing

    for train, test in splits:
        # train and test number(row number) are based on training_features.
        # For example, if '1' from train or test, it would be '1' in training_features

        train_cv_features, test_cv_features, train_cv_label, test_cv_label = training_features.iloc[train], \
                                                                             training_features.iloc[test], train_label[
                                                                                 train], train_label[test]

        if algorithm == 'GB':
            temp_regressor = GradientBoostingRegressor(n_estimators=300, random_state=1)

        elif (algorithm == 'XGBOOST'):
            temp_regressor = XGBRegressor(objective ='reg:squarederror',n_estimators=300, random_state=1, n_jobs=n_cores)

        elif (algorithm == 'RF'):
            temp_regressor = RandomForestRegressor(n_estimators=300, random_state=1, n_jobs=n_cores)

        elif (algorithm == 'M5P'):
            temp_regressor = ExtraTreesRegressor(n_estimators=300, random_state=1, n_jobs=n_cores)

        elif (algorithm == 'GAUSSIAN'):
            temp_regressor = GaussianProcessRegressor(random_state=1)

        elif (algorithm == 'ADABOOST'):
            temp_regressor = AdaBoostRegressor(n_estimators=300, random_state=1)

        elif (algorithm == 'KNN'):
            temp_regressor = KNeighborsRegressor(n_neighbors=5, n_jobs=n_cores)

        elif (algorithm == 'SVR'):
            temp_regressor = svm.SVR(kernel='rbf')

        elif (algorithm == 'NEURAL'):
            temp_regressor = MLPRegressor(random_state=1)

        elif (algorithm == 'J48'):
            temp_regressor = DecisionTreeRegressor(random_state=1)

        temp_regressor.fit(train_cv_features, train_cv_label)
        temp_prediction = temp_regressor.predict(test_cv_features)
        temp_new_pd = pd.DataFrame(
            np.column_stack([training_features.index[test].tolist(), test_cv_label, temp_prediction]),
            columns=['ID', 'actual', 'predicted'])
        temp_new_pd['fold'] = fold
        predicted_n_actual_pd = predicted_n_actual_pd.append(temp_new_pd, ignore_index=True, sort=True)
        fold += 1

    # Get Pearson's correlation of 10-fold C.V. on both original and 10% outlier removed dataset.
    predicted_n_actual_pd.ID += 1
    predicted_n_actual_pd = predicted_n_actual_pd.sort_values('ID')
    predicted_n_actual_pd['old_ID'] = training_ID['ID'].tolist()

    predicted_n_actual_pd['ID'] = predicted_n_actual_pd['ID'].astype(int)
    predicted_n_actual_pd = predicted_n_actual_pd.sort_values('ID')

    train_slope, train_intercept, train_r_value, train_p_value, train_std_err = stats.linregress(
        predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])
    predicted_n_actual_pd['predicted'] = (predicted_n_actual_pd['predicted'] - train_intercept) / train_slope
    predicted_n_actual_pd['error'] = abs(predicted_n_actual_pd['predicted'] - predicted_n_actual_pd['actual'])
    predicted_n_actual_pd = predicted_n_actual_pd.sort_values('error', ascending=False)

    # Separate results for distance-based analysis
    ten_percent_of_train = int(round(len(predicted_n_actual_pd['ID']) * 0.1))
    training_non_outliers = predicted_n_actual_pd[ten_percent_of_train:]
    training_outliers = predicted_n_actual_pd[:ten_percent_of_train]
    training_outliers.insert(len(training_outliers.columns), 'outlier', 'True')
    # Pearson's correlation
    training_w_outliers_pearsons = round(
        sp.stats.pearsonr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[0], 3)
    training_w_outliers_pearsons_pvalue = round(
        sp.stats.pearsonr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[1], 5)
    training_wo_outliers_pearsons = round(
        sp.stats.pearsonr(training_non_outliers['actual'], training_non_outliers['predicted'])[0], 3)
    training_wo_outliers_pearsons_pvalue = round(
        sp.stats.pearsonr(training_non_outliers['actual'], training_non_outliers['predicted'])[1], 5)
    # Spearman's rank-order correlation
    training_w_outliers_spearman = round(
        sp.stats.spearmanr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[0], 3)
    training_w_outliers_spearman_pvalue = round(
        sp.stats.spearmanr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[1], 5)
    training_wo_outliers_spearman = round(
        sp.stats.spearmanr(training_non_outliers['actual'], training_non_outliers['predicted'])[0], 3)
    training_wo_outliers_spearman_pvalue = round(
        sp.stats.spearmanr(training_non_outliers['actual'], training_non_outliers['predicted'])[1], 5)
    # Point biserial correlation
    training_w_outliers_point_biserial = round(
        sp.stats.pointbiserialr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[0], 3)
    training_w_outliers_point_biserial_pvalue = round(
        sp.stats.pointbiserialr(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[1], 5)
    training_wo_outliers_point_biserial = round(
        sp.stats.pointbiserialr(training_non_outliers['actual'], training_non_outliers['predicted'])[0], 3)
    training_wo_outliers_point_biserial_pvalue = round(
        sp.stats.pointbiserialr(training_non_outliers['actual'], training_non_outliers['predicted'])[1], 5)
    # Kendall's tau
    training_w_outliers_kendalltau = round(
        sp.stats.kendalltau(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[0], 3)
    training_w_outliers_kendalltau_pvalue = round(
        sp.stats.kendalltau(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted'])[1], 5)
    training_wo_outliers_kendalltau = round(
        sp.stats.kendalltau(training_non_outliers['actual'], training_non_outliers['predicted'])[0], 3)
    training_wo_outliers_kendalltau_pvalue = round(
        sp.stats.kendalltau(training_non_outliers['actual'], training_non_outliers['predicted'])[1], 5)
    # MSE
    training_w_outliers_mse = round(
        mean_squared_error(predicted_n_actual_pd['actual'], predicted_n_actual_pd['predicted']), 2)
    training_wo_outliers_mse = round(
        mean_squared_error(training_non_outliers['actual'], training_non_outliers['predicted']), 2)
    # RMSE
    training_w_outliers_rmse = round(
        sqrt(mean_squared_error(predicted_n_actual_pd['actual'],predicted_n_actual_pd['predicted'])), 2)
    training_wo_outliers_rmse = round(
        sqrt(mean_squared_error(training_non_outliers['actual'], training_non_outliers['predicted'])), 2)

    # if outlier, it will have 'true' else 'false'
    # training_outliers = training_outliers.drop(['predicted', 'actual', 'dist.to_bestfit', 'old_ID'], axis=1) # Maybe unnecessary
    overall_training = pd.merge(predicted_n_actual_pd, training_outliers[['ID', 'outlier']], how='left',
                                on=['ID', 'ID'])
    overall_training = overall_training.fillna(False)
    overall_training['predicted'] = overall_training['predicted'].round(2)
    overall_training['error'] = overall_training['error'].round(2)

    # Preparation for Evaluation/Assessment steps
    model = regressor.fit(training_features, train_label)
    # model_filename = os.path.join(output_result_dir,fname + '_' + str(ktimes) + '_' + timestr + '_model.sav')
    # model_feature_list = os.path.join(output_result_dir,fname + '_' + str(ktimes) + '_' + timestr + '_model_feature_order.sav')
    # joblib.dump(model, model_filename)
    # joblib.dump(training_features.columns, model_feature_list)
    # print(train_features.columns.values)

    overall_training = overall_training.sort_values('error', ascending=False)
    overall_training.rename(columns={"outlier": "outlier(Absolute)"}, inplace=True)
    overall_training_result_filename = timestr + '_' + fname + '_' + algorithm + "_" + str(random_state) + '_10folds_CV_scatter_plot_Absolute_error_Transformed.csv'

#    if algorithm in ['ADABOOST','M5P','RF']:
#        model_treename = overall_training_result_filename[:-4]+'_tree.dot'
#        estimator = model.estimators_[5]
#        export_graphviz(estimator, out_file=model_treename,feature_names = training_features.columns, filled=True, rounded=True, precision = 2)
#
#        from subprocess import call
#        call(['dot','-Tpng',model_treename,'-o',model_treename[:-3]+'png','-Gdpi=600'])
#    else:
#        pass

    if distance_analysis:
        # TODO: Add Spearman's, Kendall's tau..
        training_less_6ang = overall_training[overall_training['old_ID'].isin(training_less_6ang_ID.tolist())]
        training_less_6ang_pearson = np.corrcoef(training_less_6ang['actual'], training_less_6ang['predicted']).round(3)[0, 1]
        training_less_6ang_point_biserial = round(sp.stats.pointbiserialr(training_less_6ang['actual'], training_less_6ang['predicted'])[0], 3)
        training_less_6ang_rmse = round(mean_squared_error(training_less_6ang['actual'], training_less_6ang['predicted']), 2)

        training_6ang_10ang = overall_training[overall_training['old_ID'].isin(training_6ang_10ang_ID.tolist())]
        training_6ang_10ang_pearson = np.corrcoef(training_6ang_10ang['actual'], training_6ang_10ang['predicted']).round(3)[0, 1]
        training_6ang_10ang_point_biserial = round(sp.stats.pointbiserialr(training_6ang_10ang['actual'], training_6ang_10ang['predicted'])[0], 3)
        training_6ang_10ang_rmse = round(mean_squared_error(training_6ang_10ang['actual'], training_6ang_10ang['predicted']), 2)

        training_greater_10ang = overall_training[overall_training['old_ID'].isin(training_greater_10ang_ID.tolist())]
        training_greater_10ang_pearson = np.corrcoef(training_greater_10ang['actual'], training_greater_10ang['predicted']).round(3)[0, 1]
        training_greater_10ang_point_biserial = round(sp.stats.pointbiserialr(training_greater_10ang['actual'], training_greater_10ang['predicted'])[0], 3)
        training_greater_10ang_rmse = round(mean_squared_error(training_greater_10ang['actual'], training_greater_10ang['predicted']), 2)
        # TODO: Need to Fix
        # all_less_6ang = pd.concat([training_less_6ang, blind_less_6ang])
        # all_6ang_10ang =pd.concat([training_6ang_10ang, blind_6ang_10ang])
        # all_greater_10ang = pd.concat([training_greater_10ang, blind_greater_10ang])
        # all_less_6ang_pearson = np.corrcoef(all_less_6ang['predicted'], all_less_6ang['actual']).round(3)[0,1]
        # all_6ang_10ang_pearson = np.corrcoef(all_6ang_10ang['predicted'], all_6ang_10ang['actual']).round(3)[0, 1]
        # all_greater_10ang_pearson = np.corrcoef(all_greater_10ang['predicted'], all_greater_10ang['actual']).round(3)[0, 1]

    if not isinstance(outerblind_set,str):

        outerblind_set_ID = pd.DataFrame(outerblind_set['ID'])
        outerblind_label = np.array(outerblind_set[label_name])
        outerblind_features = outerblind_set[headers]
        # outerblind_set = outerblind_set.drop('ID', axis=1)
        # outerblind_features = outerblind_set.drop(label_name, axis=1)

        ## outerblind-Test

        outerblind_prediction = temp_regressor.predict(outerblind_features)
        outerblind_pd = pd.DataFrame(np.column_stack([outerblind_set_ID, outerblind_label, outerblind_prediction]),
                                     columns=['ID', 'actual', 'predicted'])

        outerblind_pd['ID'] = outerblind_pd['ID'].astype(int)
        outerblind_pd['old_ID'] = outerblind_set_ID.sort_values('ID')['ID'].tolist()

        outerblind_slope, outerblind_intercept, outerblind_r_value, outerblind_p_value, outerblind_std_err = stats.linregress(
            outerblind_pd['actual'], outerblind_pd['predicted'])
        outerblind_pd['predicted'] = (outerblind_pd['predicted'] - train_intercept) / train_slope
        outerblind_pd['error'] = abs(outerblind_pd['predicted'] - outerblind_pd['actual'])
        outerblind_pd = outerblind_pd.sort_values('error', ascending=False)
        outerblindtest_result_filename = timestr + '_' + fname + '_' + algorithm + "_" + str(random_state) + '_10folds_CV_outerblind-test_scatter_plot_Absolute_error_Transformed.csv'

        ten_percent_of_outerblind = int(round(len(outerblind_pd['ID']) * 0.1))

        outerblind_non_outliers = outerblind_pd[ten_percent_of_outerblind:]
        outerblind_outliers = outerblind_pd[:ten_percent_of_outerblind]
        outerblind_outliers.insert(len(outerblind_outliers.columns), 'outlier', 'True')
        # Pearson's correlation
        outerblind_w_outliers_pearsons = round(
            sp.stats.pearsonr(outerblind_pd['actual'], outerblind_pd['predicted'])[0], 3)
        outerblind_w_outliers_pearsons_pvalue = round(
            sp.stats.pearsonr(outerblind_pd['actual'], outerblind_pd['predicted'])[1], 5)
        outerblind_wo_outliers_pearsons = round(
            sp.stats.pearsonr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[0], 3)
        outerblind_wo_outliers_pearsons_pvalue = round(
            sp.stats.pearsonr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[1], 5)
        # Spearman's rank-order correlation
        outerblind_w_outliers_spearman = round(
            sp.stats.spearmanr(outerblind_pd['actual'], outerblind_pd['predicted'])[0], 3)
        outerblind_w_outliers_spearman_pvalue = round(
            sp.stats.spearmanr(outerblind_pd['actual'], outerblind_pd['predicted'])[1], 5)
        outerblind_wo_outliers_spearman = round(
            sp.stats.spearmanr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[0], 3)
        outerblind_wo_outliers_spearman_pvalue = round(
            sp.stats.spearmanr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[1], 5)
        # Point biserial correlation
        outerblind_w_outliers_point_biserial = round(
            sp.stats.pointbiserialr(outerblind_pd['actual'], outerblind_pd['predicted'])[0], 3)
        outerblind_w_outliers_point_biserial_pvalue = round(
            sp.stats.pointbiserialr(outerblind_pd['actual'], outerblind_pd['predicted'])[1], 5)
        outerblind_wo_outliers_point_biserial = round(
            sp.stats.pointbiserialr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[0], 3)
        outerblind_wo_outliers_point_biserial_pvalue = round(
            sp.stats.pointbiserialr(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[1], 5)
        # Kendall's tau
        outerblind_w_outliers_kendalltau = round(
            sp.stats.kendalltau(outerblind_pd['actual'], outerblind_pd['predicted'])[0], 3)
        outerblind_w_outliers_kendalltau_pvalue = round(
            sp.stats.kendalltau(outerblind_pd['actual'], outerblind_pd['predicted'])[1], 5)
        outerblind_wo_outliers_kendalltau = round(
            sp.stats.kendalltau(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[0], 3)
        outerblind_wo_outliers_kendalltau_pvalue = round(
            sp.stats.kendalltau(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])[1], 5)
        # MSE
        outerblind_w_outliers_mse = round(mean_squared_error(outerblind_pd['actual'],outerblind_pd['predicted']), 2)
        outerblind_wo_outliers_mse = round(
            mean_squared_error(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted']), 2)
        # RMSE
        outerblind_w_outliers_rmse = round(
            sqrt(mean_squared_error(outerblind_pd['actual'], outerblind_pd['predicted'])), 2)
        outerblind_wo_outliers_rmse = round(
            sqrt(mean_squared_error(outerblind_non_outliers['actual'], outerblind_non_outliers['predicted'])), 2)

        # if outlier, it will have 'true' else 'false'
        # outerblind_outliers = outerblind_outliers.drop(['predicted', 'actual', 'dist.to_bestfit', 'old_ID'], axis=1) # Maybe unnecessary
        overall_outerblind = pd.merge(outerblind_pd, outerblind_outliers[['ID', 'outlier']], how='left',
                                      on=['ID', 'ID'])
        overall_outerblind = overall_outerblind.fillna(False)
        overall_outerblind['predicted'] = overall_outerblind['predicted'].round(2)
        overall_outerblind['error'] = overall_outerblind['error'].round(2)

        if distance_analysis:
            # TODO: Need to fix and add other correlation terms for distance_analysis.

            result_ML = result_ML.append(
                {'filename': fname,
                 'less_6ang(Pearson)' : training_less_6ang_pearson,
                 '6ang_to_10ang(Pearson)': training_6ang_10ang_pearson,
                 'greater_10ang(Pearson)': training_greater_10ang_pearson,
                 'less_6ang(Point_biserial)' : training_less_6ang_point_biserial,
                 '6ang_to_10ang(Point_biserial)': training_6ang_10ang_point_biserial,
                 'greater_10ang(Point_biserial)': training_greater_10ang_point_biserial,
                 'less_6ang(RMSE)' : training_less_6ang_rmse,
                 '6ang_to_10ang(RMSE)': training_6ang_10ang_rmse,
                 'greater_10ang(RMSE)': training_greater_10ang_rmse,
                 '#of(less_6ang)': len(training_less_6ang),
                 '#of(6ang_to_10ang)': len(training_6ang_10ang),
                 '#of(greater_10ang)': len(training_greater_10ang),
                 'Training(Pearson)': training_w_outliers_pearsons,
                 'Training(Pearson_p)': training_w_outliers_pearsons_pvalue,
                 'outerBlind-test(Pearson)': outerblind_w_outliers_pearsons,
                 'outerBlind-test(Pearson_p)': outerblind_w_outliers_pearsons_pvalue,
                 'Training(Spearman)': training_w_outliers_spearman,
                 'Training(Spearman_p)': training_w_outliers_spearman_pvalue,
                 'outerBlind-test(Spearman)': outerblind_w_outliers_spearman,
                 'outerBlind-test(Spearman_p)': outerblind_w_outliers_spearman_pvalue,
                 'Training(Kendall)': training_w_outliers_kendalltau,
                 'Training(Kendall_p)': training_w_outliers_kendalltau_pvalue,
                 'outerBlind-test(Kendall)': outerblind_w_outliers_kendalltau,
                 'outerBlind-test(Kendall_p)': outerblind_w_outliers_kendalltau_pvalue,
                 'Training(Point_biserial)': training_w_outliers_point_biserial,
                 'Training(Point_biserial_p)': training_w_outliers_point_biserial_pvalue,
                 'outerBlind-test(Point_biserial)': outerblind_w_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p)': outerblind_w_outliers_point_biserial_pvalue,
                 'Training(MSE)': training_w_outliers_mse,
                 'Training(RMSE)': training_w_outliers_rmse,
                 'outerBlind-test(MSE)': outerblind_w_outliers_mse,
                 'outerBlind-test(RMSE)': outerblind_w_outliers_rmse,
                 'Training(Pearson_90%)': training_wo_outliers_pearsons,
                 'Training(Pearson_p_90%)': training_wo_outliers_pearsons_pvalue,
                 'outerBlind-test(Pearson_90%)': outerblind_wo_outliers_pearsons,
                 'outerBlind-test(Pearson_p_90%)': outerblind_wo_outliers_pearsons_pvalue,
                 'Training(Spearman_90%)': training_wo_outliers_spearman,
                 'Training(Spearman_p_90%)': training_wo_outliers_spearman_pvalue,
                 'outerBlind-test(Spearman_90%)': outerblind_wo_outliers_spearman,
                 'outerBlind-test(Spearman_p_90%)': outerblind_wo_outliers_spearman_pvalue,
                 'Training(Kendall_90%)': training_wo_outliers_kendalltau,
                 'Training(Kendall_p_90%)': training_wo_outliers_kendalltau_pvalue,
                 'outerBlind-test(Kendall_90%)': outerblind_wo_outliers_kendalltau,
                 'outerBlind-test(Kendall_p_90%)': outerblind_wo_outliers_kendalltau_pvalue,
                 'Training(Point_biserial_90%)': training_wo_outliers_point_biserial,
                 'Training(Point_biserial_p_90%)': training_wo_outliers_point_biserial_pvalue,
                 'outerBlind-test(Point_biserial_90%)': outerblind_wo_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p_90%)': outerblind_wo_outliers_point_biserial_pvalue,
                 'Training(MSE_90%)': training_wo_outliers_mse,
                 'Training(RMSE_90%)': training_wo_outliers_rmse,
                 'outerBlind-test(MSE_90%)': outerblind_wo_outliers_mse,
                 'outerBlind-test(RMSE_90%)': outerblind_wo_outliers_rmse}, ignore_index=True)

        else:

            result_ML = result_ML.append(
                {'filename': fname,
                 'Training(Pearson)': training_w_outliers_pearsons,
                 'Training(Pearson_p)': training_w_outliers_pearsons_pvalue,
                 'outerBlind-test(Pearson)': outerblind_w_outliers_pearsons,
                 'outerBlind-test(Pearson_p)': outerblind_w_outliers_pearsons_pvalue,
                 'Training(Spearman)': training_w_outliers_spearman,
                 'Training(Spearman_p)': training_w_outliers_spearman_pvalue,
                 'outerBlind-test(Spearman)': outerblind_w_outliers_spearman,
                 'outerBlind-test(Spearman_p)': outerblind_w_outliers_spearman_pvalue,
                 'Training(Kendall)': training_w_outliers_kendalltau,
                 'Training(Kendall_p)': training_w_outliers_kendalltau_pvalue,
                 'outerBlind-test(Kendall)': outerblind_w_outliers_kendalltau,
                 'outerBlind-test(Kendall_p)': outerblind_w_outliers_kendalltau_pvalue,
                 'Training(Point_biserial)': training_w_outliers_point_biserial,
                 'Training(Point_biserial_p)': training_w_outliers_point_biserial_pvalue,
                 'outerBlind-test(Point_biserial)': outerblind_w_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p)': outerblind_w_outliers_point_biserial_pvalue,
                 'Training(MSE)': training_w_outliers_mse,
                 'Training(RMSE)': training_w_outliers_rmse,
                 'outerBlind-test(MSE)': outerblind_w_outliers_mse,
                 'outerBlind-test(RMSE)': outerblind_w_outliers_rmse,
                 'Training(Pearson_90%)': training_wo_outliers_pearsons,
                 'Training(Pearson_p_90%)': training_wo_outliers_pearsons_pvalue,
                 'outerBlind-test(Pearson_90%)': outerblind_wo_outliers_pearsons,
                 'outerBlind-test(Pearson_p_90%)': outerblind_wo_outliers_pearsons_pvalue,
                 'Training(Spearman_90%)': training_wo_outliers_spearman,
                 'Training(Spearman_p_90%)': training_wo_outliers_spearman_pvalue,
                 'outerBlind-test(Spearman_90%)': outerblind_wo_outliers_spearman,
                 'outerBlind-test(Spearman_p_90%)': outerblind_wo_outliers_spearman_pvalue,
                 'Training(Kendall_90%)': training_wo_outliers_kendalltau,
                 'Training(Kendall_p_90%)': training_wo_outliers_kendalltau_pvalue,
                 'outerBlind-test(Kendall_90%)': outerblind_wo_outliers_kendalltau,
                 'outerBlind-test(Kendall_p_90%)': outerblind_wo_outliers_kendalltau_pvalue,
                 'Training(Point_biserial_90%)': training_wo_outliers_point_biserial,
                 'Training(Point_biserial_p_90%)': training_wo_outliers_point_biserial_pvalue,
                 'outerBlind-test(Point_biserial_90%)': outerblind_wo_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p_90%)': outerblind_wo_outliers_point_biserial_pvalue,
                 'Training(MSE_90%)': training_wo_outliers_mse,
                 'Training(RMSE_90%)': training_wo_outliers_rmse,
                 'outerBlind-test(MSE_90%)': outerblind_wo_outliers_mse,
                 'outerBlind-test(RMSE_90%)': outerblind_wo_outliers_rmse}, ignore_index=True)

        if scatter != 'False':
            overall_outerblind.to_csv(outerblindtest_result_filename, index=False)

        if num_of_shuffling == random_state:

            outerblind_100_set_ID = pd.DataFrame(outerblind_set['ID'])
            outerblind_100_label = np.array(outerblind_set[label_name])
            outerblind_100_features = outerblind_set[headers]
            # outerblind_100_set = outerblind_100_set.drop('ID', axis=1)
            # outerblind_100_features = outerblind_100_set.drop(label_name, axis=1)

            ## outerblind_100-Test
            outerblind_100_prediction = regressor.predict(outerblind_100_features)
            outerblind_100_pd = pd.DataFrame(np.column_stack([outerblind_100_set_ID, outerblind_100_label, outerblind_100_prediction]),
                                         columns=['ID', 'actual', 'predicted'])

            outerblind_100_pd['ID'] = outerblind_100_pd['ID'].astype(int)
            outerblind_100_pd['old_ID'] = outerblind_100_set_ID.sort_values('ID')['ID'].tolist()

            outerblind_100_slope, outerblind_100_intercept, outerblind_100_r_value, outerblind_100_p_value, outerblind_100_std_err = stats.linregress(
                outerblind_100_pd['actual'], outerblind_100_pd['predicted'])
            outerblind_100_pd['predicted'] = (outerblind_100_pd['predicted'] - train_intercept) / train_slope
            outerblind_100_pd['error'] = abs(outerblind_100_pd['predicted'] - outerblind_100_pd['actual'])
            outerblind_100_pd = outerblind_100_pd.sort_values('error', ascending=False)
            outerblind_100test_result_filename = timestr + '_' + fname + '_' + algorithm + "_" + str('by_whole_model') + '_10folds_CV_outerblind_100-test_scatter_plot_Absolute_error_Transformed.csv'

            ten_percent_of_outerblind_100 = int(round(len(outerblind_100_pd['ID']) * 0.1))

            outerblind_100_non_outliers = outerblind_100_pd[ten_percent_of_outerblind_100:]
            outerblind_100_outliers = outerblind_100_pd[:ten_percent_of_outerblind_100]
            outerblind_100_outliers.insert(len(outerblind_100_outliers.columns), 'outlier', 'True')
            # Pearson's correlation
            outerblind_100_w_outliers_pearsons = round(
                sp.stats.pearsonr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[0], 3)
            outerblind_100_w_outliers_pearsons_pvalue = round(
                sp.stats.pearsonr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[1], 5)
            outerblind_100_wo_outliers_pearsons = round(
                sp.stats.pearsonr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[0], 3)
            outerblind_100_wo_outliers_pearsons_pvalue = round(
                sp.stats.pearsonr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[1], 5)
            # Spearman's rank-order correlation
            outerblind_100_w_outliers_spearman = round(
                sp.stats.spearmanr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[0], 3)
            outerblind_100_w_outliers_spearman_pvalue = round(
                sp.stats.spearmanr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[1], 5)
            outerblind_100_wo_outliers_spearman = round(
                sp.stats.spearmanr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[0], 3)
            outerblind_100_wo_outliers_spearman_pvalue = round(
                sp.stats.spearmanr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[1], 5)
            # Point biserial correlation
            outerblind_100_w_outliers_point_biserial = round(
                sp.stats.pointbiserialr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[0], 3)
            outerblind_100_w_outliers_point_biserial_pvalue = round(
                sp.stats.pointbiserialr(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[1], 5)
            outerblind_100_wo_outliers_point_biserial = round(
                sp.stats.pointbiserialr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[0], 3)
            outerblind_100_wo_outliers_point_biserial_pvalue = round(
                sp.stats.pointbiserialr(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[1], 5)
            # Kendall's tau
            outerblind_100_w_outliers_kendalltau = round(
                sp.stats.kendalltau(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[0], 3)
            outerblind_100_w_outliers_kendalltau_pvalue = round(
                sp.stats.kendalltau(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])[1], 5)
            outerblind_100_wo_outliers_kendalltau = round(
                sp.stats.kendalltau(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[0], 3)
            outerblind_100_wo_outliers_kendalltau_pvalue = round(
                sp.stats.kendalltau(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])[1], 5)
            # MSE
            outerblind_100_w_outliers_mse = round(mean_squared_error(outerblind_100_pd['actual'], outerblind_100_pd['predicted']), 2)
            outerblind_100_wo_outliers_mse = round(
                mean_squared_error(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted']), 2)
            # RMSE
            outerblind_100_w_outliers_rmse = round(
                sqrt(mean_squared_error(outerblind_100_pd['actual'], outerblind_100_pd['predicted'])), 2)
            outerblind_100_wo_outliers_rmse = round(
                sqrt(mean_squared_error(outerblind_100_non_outliers['actual'], outerblind_100_non_outliers['predicted'])), 2)

            # if outlier, it will have 'true' else 'false'
            # outerblind_100_outliers = outerblind_100_outliers.drop(['predicted', 'actual', 'dist.to_bestfit', 'old_ID'], axis=1) # Maybe unnecessary
            overall_outerblind_100 = pd.merge(outerblind_100_pd, outerblind_100_outliers[['ID', 'outlier']], how='left',
                                          on=['ID', 'ID'])
            overall_outerblind_100 = overall_outerblind_100.fillna(False)
            overall_outerblind_100['predicted'] = overall_outerblind_100['predicted'].round(2)
            overall_outerblind_100['error'] = overall_outerblind_100['error'].round(2)

            result_ML = result_ML.append(
                {'filename': fname,
                 'Training(Pearson)': np.nan,
                 'Training(Pearson_p)': np.nan,
                 'outerBlind-test(Pearson)': outerblind_100_w_outliers_pearsons,
                 'outerBlind-test(Pearson_p)': outerblind_100_w_outliers_pearsons_pvalue,
                 'Training(Spearman)': np.nan,
                 'Training(Spearman_p)': np.nan,
                 'outerBlind-test(Spearman)': outerblind_100_w_outliers_spearman,
                 'outerBlind-test(Spearman_p)': outerblind_100_w_outliers_spearman_pvalue,
                 'Training(Kendall)': np.nan,
                 'Training(Kendall_p)': np.nan,
                 'outerBlind-test(Kendall)': outerblind_100_w_outliers_kendalltau,
                 'outerBlind-test(Kendall_p)': outerblind_100_w_outliers_kendalltau_pvalue,
                 'Training(Point_biserial)': np.nan,
                 'Training(Point_biserial_p)': np.nan,
                 'outerBlind-test(Point_biserial)': outerblind_100_w_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p)': outerblind_100_w_outliers_point_biserial_pvalue,
                 'Training(MSE)': np.nan,
                 'Training(RMSE)': np.nan,
                 'outerBlind-test(MSE)': outerblind_100_w_outliers_mse,
                 'outerBlind-test(RMSE)': outerblind_100_w_outliers_rmse,
                 'Training(Pearson_90%)': np.nan,
                 'Training(Pearson_p_90%)': np.nan,
                 'outerBlind-test(Pearson_90%)': outerblind_100_wo_outliers_pearsons,
                 'outerBlind-test(Pearson_p_90%)': outerblind_100_wo_outliers_pearsons_pvalue,
                 'Training(Spearman_90%)': np.nan,
                 'Training(Spearman_p_90%)': np.nan,
                 'outerBlind-test(Spearman_90%)': outerblind_100_wo_outliers_spearman,
                 'outerBlind-test(Spearman_p_90%)': outerblind_100_wo_outliers_spearman_pvalue,
                 'Training(Kendall_90%)': np.nan,
                 'Training(Kendall_p_90%)': np.nan,
                 'outerBlind-test(Kendall_90%)': outerblind_100_wo_outliers_kendalltau,
                 'outerBlind-test(Kendall_p_90%)': outerblind_100_wo_outliers_kendalltau_pvalue,
                 'Training(Point_biserial_90%)': np.nan,
                 'Training(Point_biserial_p_90%)': np.nan,
                 'outerBlind-test(Point_biserial_90%)': outerblind_100_wo_outliers_point_biserial,
                 'outerBlind-test(Point_biserial_p_90%)': outerblind_100_wo_outliers_point_biserial_pvalue,
                 'Training(MSE_90%)': np.nan,
                 'Training(RMSE_90%)': np.nan,
                 'outerBlind-test(MSE_90%)': outerblind_100_wo_outliers_mse,
                 'outerBlind-test(RMSE_90%)': outerblind_100_wo_outliers_rmse}, ignore_index=True)


            if scatter != 'False':
                overall_outerblind_100.to_csv(outerblind_100test_result_filename, index=False)
    else:

        if distance_analysis:
            # TODO: Need to fix and add other correlation terms for distance_analysis.
            result_ML = result_ML.append(
                {'filename': fname,
                 'less_6ang(Pearson)' : training_less_6ang_pearson,
                 '6ang_to_10ang(Pearson)': training_6ang_10ang_pearson,
                 'greater_10ang(Pearson)': training_greater_10ang_pearson,
                 'less_6ang(Point_biserial)' : training_less_6ang_point_biserial,
                 '6ang_to_10ang(Point_biserial)': training_6ang_10ang_point_biserial,
                 'greater_10ang(Point_biserial)': training_greater_10ang_point_biserial,
                 'less_6ang(RMSE)' : training_less_6ang_rmse,
                 '6ang_to_10ang(RMSE)': training_6ang_10ang_rmse,
                 'greater_10ang(RMSE)': training_greater_10ang_rmse,
                 '#of(less_6ang)': len(training_less_6ang),
                 '#of(6ang_to_10ang)': len(training_6ang_10ang),
                 '#of(greater_10ang)': len(training_greater_10ang),
                 'Training(Pearson)': training_w_outliers_pearsons,
                 'Training(Pearson_p)': training_w_outliers_pearsons_pvalue,
                 'Training(Spearman)': training_w_outliers_spearman,
                 'Training(Spearman_p)': training_w_outliers_spearman_pvalue,
                 'Training(Kendall)': training_w_outliers_kendalltau,
                 'Training(Kendall_p)': training_w_outliers_kendalltau_pvalue,
                 'Training(Point_biserial)': training_w_outliers_point_biserial,
                 'Training(Point_biserial_p)': training_w_outliers_point_biserial_pvalue,
                 'Training(MSE)': training_w_outliers_mse,
                 'Training(RMSE)': training_w_outliers_rmse,
                 'Training(Pearson_90%)': training_wo_outliers_pearsons,
                 'Training(Pearson_p_90%)': training_wo_outliers_pearsons_pvalue,
                 'Training(Spearman_90%)': training_wo_outliers_spearman,
                 'Training(Spearman_p_90%)': training_wo_outliers_spearman_pvalue,
                 'Training(Kendall_90%)': training_wo_outliers_kendalltau,
                 'Training(Kendall_p_90%)': training_wo_outliers_kendalltau_pvalue,
                 'Training(Point_biserial_90%)': training_wo_outliers_point_biserial,
                 'Training(Point_biserial_p_90%)': training_wo_outliers_point_biserial_pvalue,
                 'Training(MSE_90%)': training_wo_outliers_mse,
                 'Training(RMSE_90%)': training_wo_outliers_rmse}, ignore_index=True)


        else:

            result_ML = result_ML.append(
                {'filename': fname,
                 'Training(Pearson)': training_w_outliers_pearsons,
                 'Training(Pearson_p)': training_w_outliers_pearsons_pvalue,
                 'Training(Spearman)': training_w_outliers_spearman,
                 'Training(Spearman_p)': training_w_outliers_spearman_pvalue,
                 'Training(Kendall)': training_w_outliers_kendalltau,
                 'Training(Kendall_p)': training_w_outliers_kendalltau_pvalue,
                 'Training(Point_biserial)': training_w_outliers_point_biserial,
                 'Training(Point_biserial_p)': training_w_outliers_point_biserial_pvalue,
                 'Training(MSE)': training_w_outliers_mse,
                 'Training(RMSE)': training_w_outliers_rmse,
                 'Training(Pearson_90%)': training_wo_outliers_pearsons,
                 'Training(Pearson_p_90%)': training_wo_outliers_pearsons_pvalue,
                 'Training(Spearman_90%)': training_wo_outliers_spearman,
                 'Training(Spearman_p_90%)': training_wo_outliers_spearman_pvalue,
                 'Training(Kendall_90%)': training_wo_outliers_kendalltau,
                 'Training(Kendall_p_90%)': training_wo_outliers_kendalltau_pvalue,
                 'Training(Point_biserial_90%)': training_wo_outliers_point_biserial,
                 'Training(Point_biserial_p_90%)': training_wo_outliers_point_biserial_pvalue,
                 'Training(MSE_90%)': training_wo_outliers_mse,
                 'Training(RMSE_90%)': training_wo_outliers_rmse}, ignore_index=True)


    if scatter != 'False':
        overall_training.to_csv(overall_training_result_filename, index=False)

    # predicted_n_actual_pd.to_csv(training_result_filename+"_predicted", index=False)
    # overall_blind.to_csv(blindtest_result_filename, index=False)

    return result_ML


def dataset_norm(original_dataset, label_name):
    label = original_dataset.pop(label_name)
    original_dataset_stat = original_dataset.describe().transpose()
    normed_original_dataset = (original_dataset - original_dataset_stat['mean']) / original_dataset_stat['std']
    normed_original_dataset = normed_original_dataset.dropna(axis='columns')
    normed_original_dataset[label_name] = label

    return normed_original_dataset


def main(algorithm, input_csv, outerblindtest_csv, output_result_dir, exclusion_list, label_name, error_type, n_cores,
         num_shuffle, norm, scatter, linTransform, num_folds, cv_type, group_column):
    regex = re.compile(r"\[|\]|<", re.IGNORECASE)
    fname = os.path.split(input_csv.name)[1]
    original_dataset = pd.read_csv(input_csv, sep=',', quotechar='\'', header=0)
    original_dataset = original_dataset.fillna(False)
    original_dataset.columns = [regex.sub("_", col) if any(x in str(col) for x in set(('[', ']', '<'))) else col for col in original_dataset.columns.values]

    print("filename : {} |  algorithm : {} | Exclusion List: {}".format(fname,algorithm,exclusion_list))

    result_ML = pd.DataFrame()
    result_of_blinds = pd.DataFrame()

    if linTransform != 'False':

        result_ML_output_name = timestr + '_' + fname + '_' + algorithm + "_" + error_type + '_10folds_result_Transformed.csv'
    else:

        result_ML_output_name = timestr + '_' + fname + '_' + algorithm + "_" + error_type + '_10folds_result.csv'

    if outerblindtest_csv != 'False':
        outerblindtest_set = pd.read_csv(outerblindtest_csv, header=0)
        outerblindtest_set = outerblindtest_set.fillna(False)
        outerblindtest_set.columns = [regex.sub("_", col) if any(x in str(col) for x in set(('[', ']', '<'))) else col for col in outerblindtest_set.columns.values]
    else:
        outerblindtest_set = "False"

    if original_dataset.columns[0] != 'ID':
        print("'ID' column should be given as 1st column.")
        sys.exit()

    if exclusion_list != 'False':
        ex_list = list(exclusion_list.strip().split(','))
        original_dataset = original_dataset.drop(ex_list, axis=1)

        # if outerblindtest_csv != 'False': # This might be unnecessary because only corresponding features will be selected from ML.
        #     outerblindtest_set = outerblindtest_set[original_dataset.columns.tolist()]

    if norm == "True":
        original_dataset = dataset_norm(original_dataset, label_name)

    for each in range(1, int(num_of_shuffling) + 1):

        if linTransform != 'False':

            each_result_ML = runMLTransformed(algorithm, fname, original_dataset, outerblindtest_set, output_result_dir, label_name,
                                   error_type, n_cores, num_of_shuffling, each, scatter, num_folds, cv_type, group_column)
            result_ML = result_ML.append([each_result_ML], ignore_index=True)  # for general results

        else:

            each_result_ML = runML(algorithm, fname, original_dataset, outerblindtest_set, output_result_dir, label_name,
                                   error_type, n_cores, num_of_shuffling, each, scatter, num_folds, cv_type, group_column)
            result_ML = result_ML.append([each_result_ML], ignore_index=True)  # for general results

        if outerblindtest_csv !='False':
            print(result_ML[['filename', 'Training(Pearson)','Training(MSE)', 'Training(RMSE)','outerBlind-test(Pearson)','outerBlind-test(MSE)','outerBlind-test(RMSE)']])
        else:
            print(result_ML[['filename', 'Training(Pearson)','Training(MSE)', 'Training(RMSE)']])

    if outerblindtest_csv != 'False':

        # grab num_of_shuffling rows and put describe
        #temp_result_ML = pd.concat([result_ML[0:num_of_shuffling], result_ML[0:num_of_shuffling].describe().round(3)], sort=True)
        #result_ML = pd.concat([temp_result_ML, result_ML.iloc[[num_of_shuffling]]], sort=True)

        result_ML = pd.concat([result_ML, result_ML.describe().round(3).loc[['mean','stdev']]], sort=True)
        if '!distance!' in original_dataset.columns.tolist():
            # TODO:FIx

            sorting_order = ['filename', 'Training(Pearson)','Training(MSE)','Training(RMSE)','outerBlind-test(Pearson)','outerBlind-test(MSE)',
                             'outerBlind-test(RMSE)', 'Training(Pearson_90%)', 'Training(MSE_90%)', 'Training(RMSE_90%)',
                             'outerBlind-test(Pearson_90%)','outerBlind-test(MSE_90%)', 'outerBlind-test(RMSE_90%)','Training(Pearson_p)',
                             'outerBlind-test(Pearson_p)','less_6ang(Pearson)','6ang_to_10ang(Pearson)','greater_10ang(Pearson)',
                             'less_6ang(Point_biserial)','6ang_to_10ang(Point_biserial)','greater_10ang(Point_biserial)',
                             'less_6ang(RMSE)','6ang_to_10ang(RMSE)','greater_10ang(RMSE)',
                             '#of(less_6ang)','#of(6ang_to_10ang)','#of(greater_10ang)','Training(Spearman)',
                             'Training(Spearman_p)', 'outerBlind-test(Spearman)', 'outerBlind-test(Spearman_p)',
                             'Training(Kendall)', 'Training(Kendall_p)', 'outerBlind-test(Kendall)',
                             'outerBlind-test(Kendall_p)', 'Training(Point_biserial)', 'Training(Point_biserial_p)',
                             'outerBlind-test(Point_biserial)', 'outerBlind-test(Point_biserial_p)',
                             'Training(Pearson_p_90%)','outerBlind-test(Pearson_p_90%)',
                             'Training(Spearman_90%)','Training(Spearman_p_90%)', 'outerBlind-test(Spearman_90%)','outerBlind-test(Spearman_p_90%)',
                             'Training(Kendall_90%)', 'Training(Kendall_p_90%)', 'outerBlind-test(Kendall_90%)',
                             'outerBlind-test(Kendall_p_90%)', 'Training(Point_biserial_90%)',
                             'Training(Point_biserial_p_90%)', 'outerBlind-test(Point_biserial_90%)','outerBlind-test(Point_biserial_p_90%)']

        else:
            sorting_order = ['filename', 'Training(Pearson)','Training(MSE)', 'Training(RMSE)','outerBlind-test(Pearson)', 'outerBlind-test(MSE)',
                             'outerBlind-test(RMSE)', 'Training(Pearson_90%)','Training(MSE_90%)', 'Training(RMSE_90%)','outerBlind-test(Pearson_90%)',
                             'outerBlind-test(MSE_90%)', 'outerBlind-test(RMSE_90%)',
                             'Training(Pearson_p)','outerBlind-test(Pearson_p)', 'Training(Spearman)',
                             'Training(Spearman_p)', 'outerBlind-test(Spearman)', 'outerBlind-test(Spearman_p)',
                             'Training(Kendall)', 'Training(Kendall_p)', 'outerBlind-test(Kendall)',
                             'outerBlind-test(Kendall_p)', 'Training(Point_biserial)', 'Training(Point_biserial_p)',
                             'outerBlind-test(Point_biserial)', 'outerBlind-test(Point_biserial_p)',
                             'Training(Pearson_p_90%)','outerBlind-test(Pearson_p_90%)','Training(Spearman_90%)',
                             'Training(Spearman_p_90%)', 'outerBlind-test(Spearman_90%)','outerBlind-test(Spearman_p_90%)',
                             'Training(Kendall_90%)', 'Training(Kendall_p_90%)', 'outerBlind-test(Kendall_90%)',
                             'outerBlind-test(Kendall_p_90%)', 'Training(Point_biserial_90%)',
                             'Training(Point_biserial_p_90%)', 'outerBlind-test(Point_biserial_90%)',
                             'outerBlind-test(Point_biserial_p_90%)']

    else:

        # grap num_of_shuffling rows and put describe
        result_ML = pd.concat([result_ML, result_ML.describe().round(3).loc[['mean','std']]], sort=True)

        if '!distance!' in original_dataset.columns.tolist():
            # TODO:FIx
            sorting_order = ['filename', 'Training(Pearson)', 'Training(MSE)','Training(RMSE)', 'Training(Pearson_90%)', 'Training(MSE_90%)', 'Training(RMSE_90%)','Training(Pearson_p)',
                             'less_6ang(Pearson)','6ang_to_10ang(Pearson)','greater_10ang(Pearson)',
                             'less_6ang(Point_biserial)','6ang_to_10ang(Point_biserial)','greater_10ang(Point_biserial)',
                             'less_6ang(RMSE)','6ang_to_10ang(RMSE)','greater_10ang(RMSE)',
                             '#of(less_6ang)','#of(6ang_to_10ang)','#of(greater_10ang)','Training(Spearman)',
                             'Training(Spearman_p)','Training(Kendall)', 'Training(Kendall_p)','Training(Point_biserial)',
                             'Training(Point_biserial_p)','Training(Pearson_p_90%)', 'Training(Spearman_90%)', 'Training(Spearman_p_90%)',
                             'Training(Kendall_90%)', 'Training(Kendall_p_90%)','Training(Point_biserial_90%)',
                             'Training(Point_biserial_p_90%)']

        else:

            sorting_order = ['filename', 'Training(Pearson)', 'Training(MSE)', 'Training(RMSE)', 'Training(Pearson_90%)','Training(MSE_90%)',
                             'Training(RMSE_90%)','Training(Pearson_p)', 'Training(Spearman)',
                             'Training(Spearman_p)', 'Training(Kendall)', 'Training(Kendall_p)',
                             'Training(Point_biserial)','Training(Point_biserial_p)',
                             'Training(Pearson_p_90%)','Training(Spearman_90%)', 'Training(Spearman_p_90%)', 'Training(Kendall_90%)',
                             'Training(Kendall_p_90%)', 'Training(Point_biserial_90%)','Training(Point_biserial_p_90%)']

    #final_result = pd.DataFrame()
    #final_result = pd.concat([result_ML, result_ML.describe().round(3).loc[['mean','std']]], sort=True)
    #final_result.to_csv(os.path.join(output_result_dir, result_ML_output_name), index=True, columns=sorting_order)
    result_ML.to_csv(os.path.join(output_result_dir, result_ML_output_name), index=True, columns=sorting_order)


if __name__ == "__main__":
	#ex 1) python general_cv_regressor.py M5P training_random.csv . label BestFit 16 10 -outerblindtest_csv test_random.csv -cv_type group
	#ex 2) python general_cv_regressor.py M5P training_group.csv . label BestFit 16 10 -outerblindtest_csv test_group.csv -cv_type group -group_col cluster_id
    parser = argparse.ArgumentParser(description="ex1) python general_cv_regressor.py M5P training_random.csv . label BestFit 16 10 -outerblindtest_csv test_random.csv -cv_type group\nex2) python general_cv_regressor.py M5P training_group.csv . label BestFit 16 10 -outerblindtest_csv test_group.csv -cv_type group -group_col cluster_id")
    parser.add_argument("algorithm", help="Choose algorithm among ['GB', 'XGBOOST', 'RF', 'M5P' ,'GAUSSIAN', 'ADABOOST', 'KNN', 'SVC', 'NEURAL', 'J48']")
    parser.add_argument("input_csv", help="Choose input CSV(comma-separated values) format file",
                        type=argparse.FileType('rt'))
    parser.add_argument("output_result_dir", help="Choose folder to save result(CSV)")
    parser.add_argument("label_name", help="Type the name of label")
    parser.add_argument("error_type", help="Absolute for Absolute Error and BestFit for distance to Best-Fit curve")
    parser.add_argument("n_cores", help="Choose the number of cores to use", type=int)
    parser.add_argument("num_shuffle", help="Choose the number of shuffling", type=int)
    # options
    parser.add_argument("-outerblindtest_csv", help="Choose input CSV(comma-separated values) format file",
                        default='False')
    parser.add_argument("-ex_list", help="Put name of feature to exclude in your calculation ex) Hydro,Pos",
                        default='False')
    parser.add_argument("-norm", help="True for normalisation",
                        default='False')
    parser.add_argument("-scatter", help="True for saving predicted values into .csv file format", default='False')
    parser.add_argument("-transform", help="True for adjusting a linear transformation", default='False')

    parser.add_argument("-num_folds", help="Choose the number of folds", type=int, default=10)
    parser.add_argument("-cv_type", help="Choose the type of the cross-validation proceedure. Possible options are: ['random', 'group']", default='random')    # optional
    parser.add_argument("-group_col", help="Choose the name of the column representing the group for the training set split into k validation folds. Only available when chossing groupkfold or stratgroupkfold.", default=None)    # optional


    args = parser.parse_args()
    # required
    algorithm = args.algorithm
    input_csv = args.input_csv
    output_result_dir = args.output_result_dir
    label_name = args.label_name
    error_type = args.error_type
    n_cores = args.n_cores
    num_of_shuffling = args.num_shuffle
    # options
    ex_list = args.ex_list
    outerblindtest_csv = args.outerblindtest_csv
    norm = args.norm
    scatter = args.scatter
    linTransform = args.transform

    num_folds = args.num_folds
    cv_type = args.cv_type
    group_column = args.group_col

    if not os.path.exists(output_result_dir):
        os.makedirs(output_result_dir)

    if((cv_type != "random") and (cv_type != "group")):
        raise ValueError("cv_type should be 'random' or 'group'.")

    if((cv_type == "group") and (group_column == None)):
        raise ValueError("If you choose 'group' for cv_type, you must specify the group_col.")

    main(algorithm, input_csv, outerblindtest_csv, output_result_dir, ex_list, label_name, error_type, n_cores,
        num_of_shuffling, norm, scatter, linTransform, num_folds, cv_type, group_column)
