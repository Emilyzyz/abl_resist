from bs4 import BeautifulSoup

import csv
import os
import requests
import sys
import time

BASE_URL = 'http://biosig.unimelb.edu.au/mcsm_lig'

def submit_prediction(pdb_file, mutation, chain, lig_id, affin_wt):
    pdb_to_submit = {"wild": open(pdb_file, 'r')}
    params = {
        "mutation": mutation,
        "chain": chain,
        "lig_id": lig_id,
        "affin_wt": affin_wt,
        
    }

    url_to_submit = "{}/prediction".format(BASE_URL)
    page_output = requests.post(url_to_submit, data=params, files=pdb_to_submit)

    soup = BeautifulSoup(page_output.text, 'html.parser')
    submission_id = soup.find_all('meta', attrs={'http-equiv':"refresh"})[0].attrs['content'].split()[-1].split('/')[-1]

    return submission_id

def check_prediction_status(submission_id):
    results_url = "{}/results_prediction/{}".format(BASE_URL, submission_id)
    
    page_output = requests.get(results_url)
    soup = BeautifulSoup(page_output.text, 'html.parser')

    is_waiting_page = soup.find_all('meta', attrs={'http-equiv':"refresh"})
    if is_waiting_page:
        return False

    return True

def retrieve_results(submission_id):
    results_url = "{}/results_prediction/{}".format(BASE_URL, submission_id)
    
    page_output = requests.get(results_url)
    soup = BeautifulSoup(page_output.text, 'html.parser')

    results = dict()
    results['mCSM_lig'] = soup.find("font",size="4").text.split()[0]

    return results

def main():
    pdb_file = sys.argv[1]
    mutation = sys.argv[2]
    chain = sys.argv[3]
    lig_id = sys.argv[4]
    affin_wt = sys.argv[5]

    submission_id = submit_prediction(pdb_file, mutation, chain, lig_id, affin_wt)
    # print(submission_id)

    while(True):
        time.sleep(5)
        is_job_ready = check_prediction_status(submission_id)
        if is_job_ready:
            break
        #print("Still processing")

    results = retrieve_results(submission_id)
    print(pdb_file+","+mutation+","+chain+","+results['mCSM_lig'])

    return True

if __name__ == "__main__":
    if len(sys.argv) != 6 :
        print("*****************[Instructions]*****************************\n")
        print("\t$ python run_mCSM_lig.py file.pdb mutation chain lig_id affin_wt")
        print("\n")
        print("********************[Example]*******************************\n")
        print("\t$ python run_mCSM_lig.py 1U46.pdb E346K A SAM 10")
        sys.exit(1)
        
    main()
