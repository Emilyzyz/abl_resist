# ********************************************************
# *   ----------------------------------------------     *
# * Douglas Eduardo Valente Pires - dpires@dcc.ufmg.br   *
# * www.dcc.ufmg.br/~dpires                              *
# * Last modification :: 17/06/2014                      *
# *   ----------------------------------------------     *
# ********************************************************

use warnings;
use strict;

use LWP::UserAgent;
use File::Slurp;

my $ua = LWP::UserAgent->new;
$ua->timeout(1800);

my $url = "http://biosig.unimelb.edu.au/duet/stability_prediction";

my $file = shift;
my $mutation = shift;
my $chain = shift;
my $run = "single";

my %args;

my $file_field = "wild";
my $mutation_field = "mutation";
my $chain_field = "chain";
my $run_field = "run";

my $buf ;
my $buf_ref = $args{'buf'} || \$buf ;

my $response = $ua->post( $url,
			Content_Type => 'multipart/form-data',
			Content => [ $file_field => ["$file"] , $mutation_field => $mutation, $chain_field => $chain, $run_field => $run , "pdb_code" => " "]
			);
print $response->content;
