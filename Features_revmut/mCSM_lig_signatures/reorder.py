
import pandas as pd

# -------------------------------------------------------------------------------------------------------------


forward = pd.read_csv('for_output_HP.csv')
header = list(forward.columns.values)
# Strips the newline character

backward = pd.read_csv('output_HP.csv')
selection = backward.loc[:,header]
selection.to_csv('back_output_HP.csv', index = False)
