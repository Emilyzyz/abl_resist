#!/usr/bin/python

# ********************************************************
# *               University of Cambridge                *
# *   ----------------------------------------------     *
# *                                                      *
# * Douglas Eduardo Valente Pires - dpires@dcc.ufmg.br   *
# * www.dcc.ufmg.br/~dpires                              *
# * Last modification :: 22/07/2014                      *
# *   ----------------------------------------------     *
# ********************************************************

import os
import sys
from rdkit.Chem import AllChem as Chem

# MAIN
if __name__ == '__main__':

	# Get input smiles
	smiles_str = sys.argv[1]
	path = sys.argv[2]
	
	
	smiles_str = smiles_str.rstrip('\n')
	molecule = Chem.MolFromSmiles(smiles_str)
	fingerprint = ""
	
	if molecule:
	
		# Load toxicophores
		toxicophores = []
		tox_file = path + "/toxicophores.smarts"
		
		with open(tox_file) as tox_info:
			smarts_list = tox_info.readlines()
		
		for smarts in smarts_list:
			smarts = smarts.rstrip('\n')
			toxicophores.append(Chem.MolFromSmarts(smarts))

		# Check for toxicophore occurrence (fingerprint calculation)
		for toxic in toxicophores:
			if molecule.HasSubstructMatch(toxic):
				fingerprint = fingerprint + "1,"
			else:
				fingerprint = fingerprint + "0,"

		print fingerprint
