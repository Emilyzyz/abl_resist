#!/usr/bin/perl -w
# ********************************************************
# *               University of Cambridge                *
# *   ----------------------------------------------     *
# *                                                      *
# * Douglas Eduardo Valente Pires - dpires@dcc.ufmg.br   *
# * www.dcc.ufmg.br/~dpires                              *
# * Last modification :: 16/07/2014                      *
# *   ----------------------------------------------     *
# ********************************************************


# Version 5 - Toxicophore fingerprint


use warnings;
use strict;

sub trim;
sub getAllShortestPaths;
sub uniq;

# -------------------------------------------------------------------------------------------------
# Input parameters
my $infile = $ARGV[0];
my $outfile = $ARGV[1];
my $code_path = $ARGV[2];
my $cutoff_step = 2;
my $cutoff_limit = 6;


if(scalar(@ARGV) != 3){
print "___________________________________________________________________________________
SINTAX:
	perl pkCSM.pl <infile> <outfile> <code_path>
___________________________________________________________________________________
Where:

<outfile> is the file where the signatures will be stored
<infile> should have two columns, separated by \" \": <smiles>	<class/property>

___________________________________________________________________________________\n";
	exit;
}

if($cutoff_step < 0.01){
	print "\nError: Cutoff Step too small.\nChoose another please.\n\n";
	exit;
}
if($cutoff_limit > 100){
	print "\nError: Cutoff Limit too large.\nChoose another please.\n\n";
	exit;
}
# -------------------------------------------------------------------------------------------------

open(OUTFILE,">$outfile") or die "$!Error: $outfile\n";

open(IN,"<$infile") or die "$!Error: $infile\n";
my @in_data = <IN>;
close IN;

my $rand = int(rand(100000));

# Ignores Header
my $header = shift(@in_data);

# Possible pharmacophore classes
my %pkcsm_pharmacophores;
$pkcsm_pharmacophores{"Hydrophobe"} = 1;	$pkcsm_pharmacophores{"Aromatic"} = 1;
$pkcsm_pharmacophores{"Acceptor"} = 1;		$pkcsm_pharmacophores{"Donor"} = 1;
$pkcsm_pharmacophores{"PosIonizable"} = 1;	$pkcsm_pharmacophores{"NegIonizable"} = 1;

# Print file header

print OUTFILE "HeavyAtomCount,MolLogP,NumHeteroatoms,NumRotatableBonds,RingCount,TPSA,LabuteASA,MolWt,FCount,";

for(my $i=1; $i<=36; $i++){
	print OUTFILE "Tox_$i,";
}

#pkCSM-V6
# Print header for complementary descriptors
print OUTFILE "BalabanJ,BertzCT,Chi0,Chi0n,Chi0v,Chi1,Chi1n,Chi1v,Chi2n,Chi2v,Chi3n,Chi3v,Chi4n,Chi4v,HallKierAlpha,Kappa1,Kappa2,Kappa3,NHOHCount,NOCount,PEOE_VSA1,PEOE_VSA10,PEOE_VSA11,PEOE_VSA12,PEOE_VSA13,PEOE_VSA14,PEOE_VSA2,PEOE_VSA3,PEOE_VSA4,PEOE_VSA5,PEOE_VSA6,PEOE_VSA7,PEOE_VSA8,PEOE_VSA9,SMR_VSA1,SMR_VSA10,SMR_VSA2,SMR_VSA3,SMR_VSA4,SMR_VSA5,SMR_VSA6,SMR_VSA7,SMR_VSA8,SMR_VSA9,SlogP_VSA1,SlogP_VSA10,SlogP_VSA11,SlogP_VSA12,SlogP_VSA2,SlogP_VSA3,SlogP_VSA4,SlogP_VSA5,SlogP_VSA6,SlogP_VSA7,SlogP_VSA8,SlogP_VSA9,VSA_EState1,VSA_EState10,VSA_EState2,VSA_EState3,VSA_EState4,VSA_EState5,VSA_EState6,VSA_EState7,VSA_EState8,VSA_EState9,fr_Al_COO,fr_Al_OH,fr_Al_OH_noTert,fr_ArN,fr_Ar_COO,fr_Ar_N,fr_Ar_NH,fr_Ar_OH,fr_COO,fr_COO2,fr_C_O,fr_C_O_noCOO,fr_C_S,fr_HOCCN,fr_Imine,fr_NH0,fr_NH1,fr_NH2,fr_N_O,fr_Ndealkylation1,fr_Ndealkylation2,fr_Nhpyrrole,fr_SH,fr_aldehyde,fr_alkyl_carbamate,fr_alkyl_halide,fr_allylic_oxid,fr_amide,fr_amidine,fr_aniline,fr_aryl_methyl,fr_azide,fr_azo,fr_barbitur,fr_benzene,fr_benzodiazepine,fr_bicyclic,fr_diazo,fr_dihydropyridine,fr_epoxide,fr_ester,fr_ether,fr_furan,fr_guanido,fr_halogen,fr_hdrzine,fr_hdrzone,fr_imidazole,fr_imide,fr_isocyan,fr_isothiocyan,fr_ketone,fr_ketone_Topliss,fr_lactam,fr_lactone,fr_methoxy,fr_morpholine,fr_nitrile,fr_nitro,fr_nitro_arom,fr_nitro_arom_nonortho,fr_nitroso,fr_oxazole,fr_oxime,fr_para_hydroxylation,fr_phenol,fr_phenol_noOrthoHbond,fr_phos_acid,fr_phos_ester,fr_piperdine,fr_piperzine,fr_priamide,fr_prisulfonamd,fr_pyridine,fr_quatN,fr_sulfide,fr_sulfonamd,fr_sulfone,fr_term_acetylene,fr_tetrazole,fr_thiazole,fr_thiocyan,fr_thiophene,fr_unbrch_alkane,fr_urea,";

my @pharm_keys = sort(keys %pkcsm_pharmacophores);
foreach my $pharm_key (@pharm_keys){
        print OUTFILE "$pharm_key\_Count,";
}

for(my $x=$cutoff_limit; $x>=1; $x-=$cutoff_step){
	my @keys = sort(keys(%pkcsm_pharmacophores));
	for($a=0; $a<scalar(@keys); $a++){
		for($b=$a; $b<scalar(@keys); $b++){
			printf OUTFILE $keys[$a].":".$keys[$b]."-".'%.2f'.",",$x;
		}
	}
}
print OUTFILE "\n";

# -------------------------------------------------------------------------------------------------

# For each compound (smiles)
foreach my $line (@in_data){
	chomp($line);
	my @tokens = split("\t", $line);

	if(scalar(@tokens) < 1){
		print "\n::Error in input file format. Check sintax please::\n\n";
		exit;
	}

	my $smiles = trim(shift @tokens);
# 	my $class = trim(shift @tokens);

	# ----------------------------------------------------------------------------------------------
	# Generate graph representation (pajek file format)
	my $smiles_file = "tmp_pkCSM.$rand.smi";
	my $mol2_file = "tmp_pkCSM.$rand.mol2";
	my $pajek_file = "tmp_pkCSM.$rand.net";

	system("echo \"$smiles\" > tmp_pkCSM.$rand.smi");
	system("obabel -p7.4 -ismi $smiles_file -omol2 -O $mol2_file");

	open(PAJEK,">$pajek_file") or die "$!Error: $pajek_file\n";

	open(MOL2,"<$mol2_file") or die "$!Error: $mol2_file\n";

	my $read_node = 0;
	my $read_edge = 0;
	my $num_nodes = 0;

	my @nodes;	undef @nodes;
	my @edges;	undef @edges;

	while(my $info = <MOL2>){
		if($info =~ m/<TRIPOS>ATOM/){
			$read_node = 1;
			$read_edge = 0;
			next;
		}
		if($info =~ m/<TRIPOS>BOND/){
			$read_edge = 1;
			$read_node = 0;
			next;
		}

		if($read_node == 1 and substr($info,8,1) ne "H"){
			$num_nodes = trim(substr($info,0,7));
			my $lab = trim(substr($info,47,5));

			my $node_pajek = sprintf '%s%3d%s%5s%s', "      ", $num_nodes," \"", $lab,"\"     0.0000     0.0000     0.0000 ic       Orange bc       Orange x_fact 2.000 y_fact 2.000\n";
			push @nodes, $node_pajek;
		}
		if($read_edge == 1){
			chomp($info);
			$info = trim($info);
			my @tokens = split(/\s+/, $info);
			my $i = $tokens[1];
			my $j = $tokens[2];

			if($i <= $num_nodes and $j <= $num_nodes){
				my $edge_pajek = sprintf '%10d%10d%s', $i, $j,"     1.000\n";
				push @edges, $edge_pajek;
			}
		}
	}

	print PAJEK "*Vertices $num_nodes\n";
	print PAJEK @nodes;
	print PAJEK "*Edges\n";
	print PAJEK @edges;

	close MOL2;
	close PAJEK;


	# ----------------------------------------------------------------------------------------------
	# Get atom mapping/pharmacophores
	my @pharm = `python $code_path/gen_pharmacophores.py $mol2_file "$smiles" | sed "s/[\(|\)|,]//g" | sed "s/Lumped//g"`;

	my $descriptors = `python $code_path/gen_descriptors.py "$smiles"`;
	chomp($descriptors);
	print OUTFILE "$descriptors,";

	my $F_count = `echo "$smiles"| tail -n1 | cut -f1 | sed "s/F[a-z]/X/g" | sed 's/\(.\)/\1\\n/g' | grep F | wc -l`;
	chomp($F_count);
	print OUTFILE "$F_count,";

	my $toxicophore_fingerprint = `python $code_path/gen_toxicophores.py "$smiles" $code_path`;
	chomp($toxicophore_fingerprint);
	print OUTFILE $toxicophore_fingerprint;


	#pkCSM-V6
	# Print complementary descriptors
	my $complementary_desc = `python $code_path/gen_complementary_descriptors_v6.py "$smiles"`;
        chomp($complementary_desc);
        print OUTFILE "$complementary_desc,";


	my @tokens_desc = split(",", $descriptors);
	my $HeavyAtomCount = $tokens_desc[0];

	my %AtomPharm;
	undef %AtomPharm;

	foreach my $line (@pharm){
		chomp($line);
		my @class = split("\t", $line);
		my @atom_ind_list = split(" ", $class[1]);

		if($class[0] =~ m/Hydrophobe/){
			foreach my $ind (@atom_ind_list){
				$AtomPharm{$ind}{"Hydrophobe"} = 1;
			}
		}
		if($class[0] =~ m/Aromatic/){
			foreach my $ind (@atom_ind_list){
				$AtomPharm{$ind}{"Aromatic"} = 1;
			}
		}
		if($class[0] =~ m/PosIonizable/){
			foreach my $ind (@atom_ind_list){
				$AtomPharm{$ind}{"PosIonizable"} = 1;
			}
		}
		if($class[0] =~ m/NegIonizable/){
			foreach my $ind (@atom_ind_list){
				$AtomPharm{$ind}{"NegIonizable"} = 1;
			}
		}
		if($class[0] =~ m/Acceptor/){
			foreach my $ind (@atom_ind_list){
				$AtomPharm{$ind}{"Acceptor"} = 1;
			}
		}
		if($class[0] =~ m/Donor/){
			foreach my $ind (@atom_ind_list){
				$AtomPharm{$ind}{"Donor"} = 1;
			}
		}
	}
	# ----------------------------------------------------------------------------------------------
	# Calculate pharmacophore count

	my %PharmCount;
	foreach my $key (@pharm_keys){
		$PharmCount{$key} = 0;
	}

	for(my $i=0; $i<$HeavyAtomCount; $i++){
		foreach my $key (@pharm_keys){
			if(defined($AtomPharm{$i}{$key})){
				$PharmCount{$key}++;
			}
		}
	}

	foreach my $key (@pharm_keys){
		print OUTFILE $PharmCount{$key}, ",";
	}
	# ----------------------------------------------------------------------------------------------

	my %edgeCount;
	undef %edgeCount;

	my @keys = sort(keys(%pkcsm_pharmacophores));
	foreach my $key1 (@keys){
		foreach my $key2 (@keys){
			$edgeCount{$key1.":".$key2} = 0;
			if($key1 ne $key2){
				$edgeCount{$key2.":".$key1} = 0;
			}
		}
	}

	# ----------------------------------------------------------------------------------------------
	# Calculate shortest paths matrix

	my $dist_file = getAllShortestPaths($pajek_file);

	open(DIST,"<$dist_file") or die "$!Error: $dist_file\n";
	my @dist_data = <DIST>;
	close DIST;

	my @dist;		undef @dist;

	foreach my $distance_info (@dist_data){
		chomp($distance_info);
		my @dist_tokens = split(/\s+/, $distance_info);

		my $a = $dist_tokens[0];
		my $b = $dist_tokens[1];
		my $distance = $dist_tokens[2];

		$dist[$a][$b] = $distance;
		$dist[$b][$a] = $distance;
	}

	# ----------------------------------------------------------------------------------------------
	# Generate signature

	# Cutoff limits
	my $cutoff_1 = 1.000;
	my $cutoff_2 = $cutoff_limit;

	for(my $cutoff_temp=$cutoff_limit;$cutoff_temp>=1;$cutoff_temp-=$cutoff_step){

		for(my $i=1;$i<=$num_nodes;$i++){
			for(my $j=$i+1;$j<=$num_nodes;$j++){

				if($dist[$i][$j] >= $cutoff_1 and $dist[$i][$j] <= $cutoff_temp){

					my @key_set1 = keys %{$AtomPharm{$i}};
					my @key_set2 = keys %{$AtomPharm{$j}};

					my @pairs;	undef @pairs;

					for(my $x=0; $x<scalar(@key_set1); $x++){
						for(my $y=0; $y<scalar(@key_set2); $y++){
							my $my_key_set1 = $key_set1[$x];
							my $my_key_set2 = $key_set2[$y];

							push @pairs, $my_key_set1.":".$my_key_set2;
						}
					}
					my @uniq_pairs = uniq(@pairs);

					foreach my $pair (@uniq_pairs){
						$edgeCount{$pair}++;
						my @toks = split(":", $pair);
					}
				}
			}
		}

		for(my $a=0; $a<scalar(@keys); $a++){
			for(my $b=$a; $b<scalar(@keys); $b++){
				print OUTFILE $edgeCount{$keys[$a].":".$keys[$b]}, ",";
			}
		}

		foreach my $key1 (@keys){
			foreach my $key2 (@keys){
				$edgeCount{$key1.":".$key2} = 0;
				if($key1 ne $key2){
					$edgeCount{$key2.":".$key1} = 0;
				}
			}
		}
	}

	printf OUTFILE "\n";
}

# Remove temporary files
system("rm tmp_pkCSM.$rand.*");

close OUTFILE;


system("sed -i 's/,\$//g' $outfile");

exit;

# -------------------------------------------------------------------------------------------------

sub trim{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

sub getAllShortestPaths{

	my $pajek_file = shift;

	open(SCRIPT, ">$pajek_file.gen_dist.R");
	print SCRIPT "library(\"igraph\")\n";
	print SCRIPT "g = read.graph(file = (\"$pajek_file\"), format=c(\"pajek\"))\n";
	print SCRIPT "x <- shortest.paths(g, mode=c(\"all\"), weights=NULL, algorithm=c(\"johnson\"))\n";
	print SCRIPT "sink(\"$pajek_file.matrix.tmp\")\n";
	print SCRIPT "for(i in seq(1,dim(x)[1],1)){\n";
	print SCRIPT "\tfor(j in seq(1,dim(x)[2],1)){\n";
	print SCRIPT "\t\tif(i <= j){\n";
	print SCRIPT "\t\t\tcat(i, j, x[i,j], \"\\n\")\n";
	print SCRIPT "\t\t}\n";
	print SCRIPT "\t}\n";
	print SCRIPT "}\n";
	print SCRIPT "sink()\n";
	close(SCRIPT);

	system("R CMD BATCH $pajek_file.gen_dist.R");
	system("mv $pajek_file.matrix.tmp $pajek_file.dist");
	system("rm $pajek_file.gen_dist.R");

	return "$pajek_file.dist";
}

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}
