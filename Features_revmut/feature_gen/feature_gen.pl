#!/usr/bin/perl -w
# ***********************************
# * ------------------------------- *
# * Douglas Eduardo Valente Pires   *
# * douglas.pires@minas.fiocruz.br  *
# * ------------------------------- *
# * Last modification :: 22/10/2018 *
# * ------------------------------- *
# ***********************************

use strict;
use warnings;


# -------------------------------------------------------------------------------------------
# ::PURPOSE::
#
# Generates all features classes for a given set of mutations.
#
# -------------------------------------------------------------------------------------------
# Input parameter
my $mutation_file = $ARGV[0];	 # Tab-separated: <PDB> <MUTATION> <CHAIN> <MT_PDB_optional>
my $outfile = $ARGV[1];		 # Outfile

if(scalar(@ARGV) != 2){
print "_________________________________________________________________________________________________________________
  SINTAX:
	perl feature_gen.pl <mutation_file> <outfile>

	Where <mutation_file> is a tab-separated file:
	<PDB> <MUTATION> <CHAIN> <MT_PDB_optional>

	Please use the following columns names as header (column names will be considered):
	PDB MUTATION CHAIN UNIPROT MT_PDB
_________________________________________________________________________________________________________________\n";
	exit;
}

# -------------------------------------------------------------------------------------------
# Parse header
my $header = `head -n1 $mutation_file`;
chomp($header);

my @columns = split("\t", $header);
my %col_dict;

for(my$i=0; $i<scalar(@columns); $i++){
        $col_dict{$columns[$i]} = $i;
}

# -------------------------------------------------------------------------------------------
# Blosum62, PAM30 and special amunoacids
#system("perl calc_special_subs_matrix/special_aminoacids_subs_matrix.pl $mutation_file > $outfile.special_blosum_pam");

# -------------------------------------------------------------------------------------------
# RSA
system("perl calc_rsa/gen_rsa_mutation.pl $mutation_file > $outfile.rsa");

# -------------------------------------------------------------------------------------------
# Depth
system("perl calc_res_depth/res_depth.pl $mutation_file > $outfile.depth");

# -------------------------------------------------------------------------------------------
# Arpeggio contacts
system("perl calc_arpeggio/run_arpeggio_mutation.pl $mutation_file > $outfile.contacts"); #only use contacts file for extraction with Arpeggio_analyzer_v2.py - DO NOT USE outfile.contacts [wrong values given]

# -------------------------------------------------------------------------------------------
# Arpeggio contacts (diff)
if(defined($col_dict{"MT_PDB"})){
         system("perl calc_arpeggio/run_arpeggio_mutation_diff.pl $mutation_file > $outfile.contacts_diff"); #only use contacts file for extraction with Arpeggio_analyzer_v2.py - DO NOT USE outfile.contacts_diff [wrong values given]
 }

exit;
# -------------------------------------------------------------------------------------------
