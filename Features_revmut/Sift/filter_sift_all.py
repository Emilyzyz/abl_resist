#######################################################################################################################

#This script is used to extract sift results from saturation mutagenesis table after:

# only one row starting with 'pos' is retained, and positions are renamed as e.g. 'M1' not '1M' and % seq positions
# (first column after position) removed

# Written by S. Portelli & Y. Myung on 02/04/2019
# Modified by Emily Yunzhuo Zhou on 3/1/2021
#######################################################################################################################


import pandas as pd

inputcsv = pd.read_csv("table.csv",delimiter=",",header=0)

mutations = open('mutation_list/2G1T_mutations_list.txt', 'r')
Lines = mutations.readlines()

mut_list= ["pos","A","C","D","E","F","G","H","I","K","L","M","N","P","Q","R","S","T","V","W","Y"]

dict = {}

for index,row in inputcsv.iterrows():
    # print(row.tolist())

    for mut in range(1,21):
        dict[row[0][3] + str(row[0][0:3] + mut_list[mut])] = row[mut]

#sorted_df = pd.DataFrame(sorted_results)
#print(Lines[0].rstrip('\n'))
for i in range(len(Lines)):
    print(Lines[i].rstrip('\n'), dict[Lines[i].rstrip('\n')])
#sorted_df.to_csv("sift_4wa9.csv",index=False)
