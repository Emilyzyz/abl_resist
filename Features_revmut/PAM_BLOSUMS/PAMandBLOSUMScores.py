#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
import sys
from Bio.SubsMat import MatrixInfo as sm #package that implemens BLOSUM and PAM Matrices

def score_match(pair, matrix):
    if pair not in matrix:
        return matrix[(tuple(reversed(pair)))]
    else:
        return matrix[pair]

def score_pairwise(seq1, seq2, matrix, gap_s, gap_e):
    score = 0
    gap = False
    for i in range(len(seq1)):
        pair = (seq1[i], seq2[i])
        if not gap:
            if '-' in pair:
                gap = True
                score += gap_s
            else:
                score += score_match(pair, matrix)
        else:
            if '-' not in pair:
                gap = False
                score += score_match(pair, matrix)
            else:
                score += gap_e
    return score

def score_mut(matrix):
    return score_pairwise(AA1, AA2, matrix,0,0)

if len(sys.argv) !=3:
    print("\t   __________________________________________________________________________________________________________________\n\
           SINTAX:\n\
           python PAMandBLOSUMScores.py <infile> <outfile> \n\
           ____________________________________________________________________________________________________________________\n\
           Where:\n\
           \n\
           \t<infile> should have one column, separated by "","" containing the mutation codes with the following example sintax: R133Q\n\
           \tWhere:\n\
           \t\tR is the 1-letter code for the wild-type residue and Q is 1-letter code for the mutant residue.\n\
           \t<outfile> is the output file that will be created with the substitution matrices scores.\n\
           ____________________________________________________________________________________________________________________\n")
else:
    # Input Parameters
    in_file = sys.argv[1]           #   Input file 
    out_file = sys.argv[2]

    file = open(out_file,"w+")                  
    
    # Generating header for the output file
    header = []
    header.extend(str("blosum100,blosum30,blosum35,blosum40,blosum45,blosum50,blosum55,blosum60,blosum62,blosum65,blosum70,blosum75,blosum80,blosum85,blosum90,blosum95,pam120,pam180,pam250,pam30,pam300,pam60,pam90\n"))
    
    file.writelines(header)
    
    data = pd.read_csv(in_file, delimiter = ",", header = None)
    print("Database contains", data.index.size,"registers")
    
    
    for index, row in data.iterrows():
        AA1 = row[0][0]
        AA2 = row[0][len(row[0])-1]
        
        scores_list = []
        # Calculating BLOSUM and PAM scores
        scores_list.extend([score_mut(sm.blosum100), score_mut(sm.blosum30), score_mut(sm.blosum35), score_mut(sm.blosum40), score_mut(sm.blosum45), score_mut(sm.blosum50)])
        scores_list.extend([score_mut(sm.blosum55), score_mut(sm.blosum60), score_mut(sm.blosum62), score_mut(sm.blosum65), score_mut(sm.blosum70), score_mut(sm.blosum75)])
        scores_list.extend([score_mut(sm.blosum80), score_mut(sm.blosum85), score_mut(sm.blosum90), score_mut(sm.blosum95)])                    
        scores_list.extend([score_mut(sm.pam120), score_mut(sm.pam180), score_mut(sm.pam250), score_mut(sm.pam30), score_mut(sm.pam300), score_mut(sm.pam60), score_mut(sm.pam90)])
        
        scores = str(scores_list).strip('[] ').replace('\'','').replace(' ','') # removing characters
        file.writelines(scores)
        file.write("\n")
          
    file.close()
    lines_number = pd.read_csv(out_file, delimiter = ",")
    print(len(lines_number.index), "lines were recorded!\n")    # Just to compare with the lines read before
