# ********************************************************
# *               University of Melbourne                *
# *   ----------------------------------------------     *
# * Yoochan Myung - ymyung@student.unimelb.edu.au        *
# * Last modification :: 25/09/2019                      *
# *   ----------------------------------------------     *
# * Emily Yunzhuo Zhou - modify forward to backward
# ********************************************************
import pandas as pd
import numpy as np
import argparse
import csv
import sys
import time
import os

from terminalplot import plot
from terminalplot import get_terminal_size
import collections
from sklearn import preprocessing
import group_kfold_regressor

def backwardGreedyFeature(training_csv, algorithm, output_result_dir, label_name, scoring , n_cores, outerblind_set, cutoff, num_folds, cv_type, group_column):
    input_filename = os.path.split(training_csv)[1]

    ## DataFrame
    training_set = pd.read_csv(training_csv, header=0)
    training_set = training_set.fillna(False)
    training_set_ID= training_set.pop('ID')
    train_label = training_set[label_name]

    train_group = None
    if(cv_type == "group"):
        train_group = training_set[group_column]
        training_set = training_set.drop(group_column, axis=1)

    training_features = training_set.drop(label_name, axis=1)
    training_features_original = training_features.copy()

    greedy_features = pd.DataFrame()
    total_score_book = collections.OrderedDict()
    scored_feature = collections.OrderedDict()
    score_book = pd.DataFrame(columns=['training','blind-test','diff'])
    score_book_cutoff = pd.DataFrame(columns=['training','blind-test','diff'])

    total_feature_list = pd.DataFrame()
    scatter_plot = pd.DataFrame()
    stop_signal = False
    greedy_elim_feature_num = 1
    running_counts = 1

##################test with all features, and define it manually#################
    #prev_diff = 0.353
    full_set = pd.concat([training_set_ID, training_features, train_group, train_label], axis=1)
    train_all = group_kfold_regressor.runML(algorithm, 'all', full_set, outerblind_set, output_result_dir, label_name,'BestFit', n_cores,1,1,'False', num_folds, cv_type, group_column)
    #init_diff = round(abs(train_all['Training({})'.format(scoring)][0] - float(train_all['Blindtest({})'.format(scoring)][0])),3)
    #prev_diff = init_diff
##################################################################################
    while stop_signal != True:

        num_of_features = len(training_features.columns)

        for x in range(0,num_of_features):

            # name of feature
            feature_name = training_features.iloc[:,x].name

            if num_of_features > 1:
                temp_feature_set = pd.concat([training_set_ID, training_features.drop(feature_name, axis=1), train_group, train_label], axis=1)
            else:
                total_feature_list.index +=1
                total_feature_list = total_feature_list.append({'feature_name':feature_name,'training':0,'blind-test':0},ignore_index=True)
                total_feature_list.to_csv(timestr+'_greedy_fe_'+ input_filename + '_' + algorithm +'_total_feature.csv',index=True)
                stop_signal = True


            #if len(greedy_features) > 0 :
                #temp_feature_set = pd.concat([training_set_ID, greedy_features, training_features.iloc[:,x], train_group, train_label], axis=1)
            #else:
                #temp_feature_set = pd.concat([training_set_ID, training_features.iloc[:,x], train_group, train_label], axis=1)

            # only for C.V.
            if isinstance(outerblind_set,str):
                scored_feature[feature_name] = round(float(group_kfold_regressor.runML(algorithm, feature_name, temp_feature_set,'False', output_result_dir, label_name,'BestFit', n_cores,1,1,'False', num_folds, cv_type, group_column)['Training({})'.format(scoring)][0]),3)

            else:
                # for both C.V. and blind-test dataset.
                diff_cutoff = cutoff
                train_blindtest_metrics = group_kfold_regressor.runML(algorithm, feature_name, temp_feature_set, outerblind_set, output_result_dir, label_name,'BestFit', n_cores,1,1,'False', num_folds, cv_type, group_column)
                score_book.loc[feature_name,'training'] = round(float(train_blindtest_metrics['Training({})'.format(scoring)][0]),3)
                score_book.loc[feature_name,'blind-test'] = round(float(train_blindtest_metrics['Blindtest({})'.format(scoring)][0]),3)
                score_book.loc[feature_name,'diff'] = round(abs(train_blindtest_metrics['Training({})'.format(scoring)][0] - float(train_blindtest_metrics['Blindtest({})'.format(scoring)][0])),3)
                score_book.sort_values(by=['training'],inplace=True, ascending=False)
                print("=====[1]  Eliminated features=====")
                if len(greedy_features.columns) < 1:
                    print("No Eliminated Feature")
                else:
                    print("{}".format(greedy_features.columns))
                print("=====[2] Greedy test=====")
                print("{}".format(score_book))


                print("=====[3] Features satisfy the user given cutoff condition( {}, def=.1)=====".format(cutoff))
                if score_book.loc[feature_name,'diff'] <= diff_cutoff:
                    scored_feature[feature_name] =  score_book.loc[feature_name,'training']
                    # print("The amount of features passed cutoff : ",len(scored_feature))

                if len(scored_feature.items()) < 1:
                    print("No Features eliminated yet")
                else:
                    print("{}".format(list(scored_feature.items())))
                    print("")
            ## time cost
            print("progress : ", str(running_counts)+"/" + str(int(((len(training_features_original.columns)*(len(training_features_original.columns)+1))*0.5))))
            print(feature_name)
            print(scored_feature)
            running_counts +=1
#############################if no feature satisfy the cut-off, eliminate the min diff#####################
        if len(scored_feature) < 1:
            feature_min_diff = score_book.loc[score_book['diff'] == min(score_book['diff'])].index.values[0]
            scored_feature[feature_min_diff] =  score_book.loc[feature_min_diff,'training']
#############################trial!##########################################################
        if not isinstance(outerblind_set,str) and len(scored_feature) < 1:
            print("Your Job is Done")
            stop_signal = True
            pass

        else:

            if scoring == 'RMSE' or scoring == 'MSE':
                scored_feature = sorted(scored_feature.items(), key=lambda t: t[1], reverse=True)
            else:
                scored_feature = sorted(scored_feature.items(), key=lambda t: t[1])
            highest_feature_name = scored_feature[-1][0].strip()
            highest_feature_score = scored_feature[-1][1]
            if isinstance(outerblind_set,str):
                total_feature_list = total_feature_list.append({'feature_name':highest_feature_name,'training':highest_feature_score},ignore_index=True)
            else:
                total_feature_list = total_feature_list.append({'feature_name':highest_feature_name,'training':score_book.loc[highest_feature_name,'training'],'blind-test':score_book.loc[highest_feature_name,'blind-test']},ignore_index=True)

            #prev_diff = score_book.loc[highest_feature_name,'training'] - score_book.loc[highest_feature_name,'blind-test']

            greedy_features = pd.concat([greedy_features,training_features[highest_feature_name]],axis=1)
            training_features = training_features.drop(highest_feature_name, axis=1)
            scored_feature = collections.OrderedDict()
            scatter_plot = scatter_plot.append({'feature_id': int(greedy_elim_feature_num), scoring :highest_feature_score}, ignore_index=True)
            total_score_book[highest_feature_name] = highest_feature_score
            get_terminal_size()
            plot(scatter_plot['feature_id'].tolist(), scatter_plot[scoring].tolist())

            greedy_elim_feature_num +=1

            if len(training_features.columns) == 0:
                stop_signal = True

            score_book = pd.DataFrame()
            scatter_plot.feature_id = scatter_plot.feature_id.astype(int)
            scatter_plot = scatter_plot[['feature_id',scoring]]
            #total_feature_list = total_feature_list[['feature_name','training''blind-test']]
            total_feature_list.index +=1
            scatter_plot.to_csv(timestr+'_greedy_fe_'+ input_filename + '_' + algorithm +'_scatter_plot.csv',index=False)
            total_feature_list.to_csv(timestr+'_greedy_fe_'+ input_filename + '_' + algorithm +'_total_feature.csv',index=True)
        print("=====[4] Greedy-based eliminated features=====")
        print("{}".format(list(total_score_book)))
        print("============================================")
        # with open(timestr+'_greedy_fs_'+ input_filename  + '_'+ algorithm +'_result.csv', 'w') as feature_writer:
        #     wr = csv.writer(feature_writer)
        #     wr.writerow(total_feature_list)

    # score_book.to_csv(timestr+'_greedy_fs'+ input_filename + '_' + algorithm +'_score_book.csv',index=False)
    return True

if __name__ == '__main__':
    timestr = time.strftime("%Y%m%d_%H%M%S")
    #ex 1) python general_greedy_regressor.py M5P training_random.csv . label Pearson 10 -blindtest test_random.csv
    #ex 2) python general_greedy_regressor.py M5P training_group.csv . label Pearson 10 -blindtest test_group.csv -cv_type group -group_col cluster_id

    parser = argparse.ArgumentParser(description="ex 1) python group_greedy_elim_regressor.py M5P training_random.csv . label Pearson 10 -blindtest test_random.csv\nex 2) python general_greedy_regressor.py M5P training_group.csv . label Pearson 10 -blindtest test_group.csv -cv_type group -group_col cluster_id")
    parser.add_argument("algorithm", help="Choose algorithm [GB,XGBOOST,RF,M5P,GAUSSIAN,ADABOOST,KNN,SVC,NEURAL]")
    parser.add_argument("input_csv", help="Choose input CSV(comma-separated values) format file")
    parser.add_argument("output_result_dir", help="Choose folder to save result(CSV)")
    parser.add_argument("label_name", help="Type the name of label")
    parser.add_argument("scoring", help="Choose one metric for greedy_feature selection ['Pearson','MSE','RMSE']")
    parser.add_argument("n_cores", help="Choose the number of cores to use", type=int)
    parser.add_argument("-cutoff", help="Set cutoff value for the difference between training and blind-test performance, default=0.1", default=0.1, type=float)
    parser.add_argument("-blindtest", help="Choose input CSV(comma-separated values) format file", default='False')    # optional

    parser.add_argument("-num_folds", help="Choose the number of folds", type=int, default=10)  # optional
    parser.add_argument("-cv_type", help="Choose the type of the cross-validation proceedure. Possible options are: ['random', 'group']", default='random')    # optional
    parser.add_argument("-group_col", help="Choose the name of the column representing the group for the training set split into k validation folds. Only available when chossing groupkfold or stratgroupkfold.", default=None)    # optional

    args = parser.parse_args()
    # required
    algorithm = args.algorithm
    training_csv = args.input_csv
    output_result_dir = args.output_result_dir
    label_name = args.label_name
    scoring = args.scoring
    n_cores = int(args.n_cores)
    # optional
    cutoff = args.cutoff
    blindtest = args.blindtest
    num_folds = args.num_folds
    cv_type = args.cv_type
    group_column = args.group_col

    if blindtest != 'False':
        outerblindtest_set = pd.read_csv(blindtest, header=0)
        outerblindtest_set = outerblindtest_set.fillna(False)

    else:
        outerblindtest_set = "False"

    if((cv_type != "random") and (cv_type != "group")):
        raise ValueError("cv_type should be 'random' or 'group'.")

    if((cv_type == "group") and (group_column == None)):
        raise ValueError("If you choose 'group' for cv_type, you must specify the group_col.")

    backwardGreedyFeature(training_csv, algorithm, output_result_dir, label_name, scoring, n_cores, outerblindtest_set, cutoff, num_folds, cv_type, group_column)
