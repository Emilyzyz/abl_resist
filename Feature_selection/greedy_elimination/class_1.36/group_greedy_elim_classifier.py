# ********************************************************
# *               University of Melbourne                *
# *   ----------------------------------------------     *
# * Yoochan Myung - ymyung@student.unimelb.edu.au        *
# * Last modification :: 21/01/2020                      *
# *   ----------------------------------------------     *
# ********************************************************
import pandas as pd
import numpy as np
import argparse
import csv
import sys
import time
import os

from terminalplot import plot
from terminalplot import get_terminal_size
import collections
from sklearn import preprocessing
import group_kfold_classifier

def backwardGreedyFeature(training_csv, algorithm, output_result_dir, label_name, scoring , n_cores, outerblind_set, cutoff, num_folds, cv_type, group_column):
    input_filename = os.path.split(training_csv)[1]

    ## DataFrame
    training_set = pd.read_csv(training_csv, quotechar='\"', header=0)
    training_set = training_set.fillna(False)
    training_set_ID= training_set.pop('ID')
    train_label = training_set[label_name]

    train_group = None
    if(cv_type == "group"):
        train_group = training_set[group_column]
        training_set = training_set.drop(group_column, axis=1)

    training_features = training_set.drop(label_name, axis=1)
    training_features_original = training_features.copy()

    greedy_features = pd.DataFrame()
    total_score_book = collections.OrderedDict()
    scored_feature = collections.OrderedDict()
    score_book = pd.DataFrame(columns=['training','blind-test','diff'])
    score_book_cutoff = pd.DataFrame(columns=['training','blind-test','diff'])

    total_feature_list = pd.DataFrame()
    scatter_plot = pd.DataFrame()
    stop_signal = False
    greedy_elim_feature_num = 1
    running_counts = 1
    ##################test with all features, and define init_diff#################

    full_set = pd.concat([training_set_ID, training_features, train_group, train_label], axis=1)
    train_all =group_kfold_classifier.runML(algorithm, 'all', full_set, outerblind_set, output_result_dir, label_name, n_cores,1, num_folds, cv_type, group_column)[scoring]
    init_diff = round(float(train_all.loc[0])-float(train_all.loc['blind-test']),3)
    prev_diff = init_diff
    ##################################################################################

    while stop_signal != True:

        num_of_features = len(training_features.columns)

        for x in range(0,num_of_features):

            # name of feature
            feature_name = training_features.iloc[:,x].name

            if num_of_features > 1:
                temp_feature_set = pd.concat([training_set_ID, training_features.drop(feature_name, axis=1), train_group, train_label], axis=1)
            else:
                total_feature_list.index +=1
                total_feature_list = total_feature_list.append({'feature_name':feature_name,'training':0,'blind-test':0},ignore_index=True)
                total_feature_list.to_csv(timestr+'_greedy_fe_'+ input_filename + '_' + algorithm +'_total_feature.csv',index=True)
                stop_signal = True

            # only for C.V.
            if isinstance(outerblind_set,str):
                scored_feature[feature_name] = float(group_kfold_classifier.runML(algorithm, feature_name, temp_feature_set, 'False', output_result_dir, label_name, n_cores, 1, num_folds, cv_type, group_column)[scoring].loc[0])
            else:
                # for both C.V. and blind-test dataset.
                diff_cutoff = cutoff
                train_blindtest_metrics = group_kfold_classifier.runML(algorithm, feature_name, temp_feature_set, outerblind_set, output_result_dir, label_name, n_cores, 1, num_folds, cv_type, group_column)[scoring]
                temp_training_score = round(float(train_blindtest_metrics.loc[0]),3)
                temp_blindtest_score = round(float(train_blindtest_metrics.loc['blind-test']),3)
                temp_diff_score = abs(round(temp_training_score - temp_blindtest_score,3))
                score_book.loc[feature_name,'training'] = temp_training_score
                score_book.loc[feature_name,'blind-test'] = temp_blindtest_score
                score_book.loc[feature_name,'diff'] = temp_diff_score
                score_book.sort_values(by=['training'],inplace=True, ascending=False)
                print("=====[1] Eliminated features=====")
                if len(greedy_features.columns) < 1:
                    print("No Eliminated Feature")
                else:
                    print("{}".format(greedy_features.columns))
                print("=====[2] Greedy test=====")
                print("{}".format(score_book))

                cutoffb = 0.1
                if prev_diff > 0.1:
                    cutoffb = min(round(1.02*prev_diff,3), init_diff)
                print("=====[3] Features satisfy the user given cutoff condition ({} ,def = 0.1 or 1.02prev_diff {}) =====".format(cutoff,cutoffb))
                if temp_diff_score <= diff_cutoff or temp_diff_score <= cutoffb:
                    scored_feature[feature_name] =  temp_training_score

                if len(scored_feature.items()) < 1:
                    print("No Features eliminated yet")
                else:
                    print('{}'.format(list(scored_feature.items())))
                    print("")
            ## time cost
            print("progress : ", str(running_counts)+"/" + str(int(((len(training_features_original.columns)*(len(training_features_original.columns)+1))*0.5))))
            print(feature_name)
            print(scored_feature)
            running_counts +=1
#############################if no feature satisfies the cut-off, eliminate the min diff#####################
        if len(scored_feature) < 1:
            feature_min_diff = score_book.loc[score_book['diff'] == min(score_book['diff'])].index.values[0]
            scored_feature[feature_min_diff] =  score_book.loc[feature_min_diff,'training']
#############################trial!##########################################################
        if not isinstance(outerblind_set,str) and len(scored_feature) < 1:
            print("Your Job is Done")
            stop_signal = True
            pass

        else:

            scored_feature = sorted(scored_feature.items(), key=lambda t: t[1])
            highest_feature_name = scored_feature[-1][0].strip()
            highest_feature_score = scored_feature[-1][1]
            if isinstance(outerblind_set,str):
                total_feature_list = total_feature_list.append({'feature_name':highest_feature_name,'training':highest_feature_score},ignore_index=True)
            else:
                total_feature_list = total_feature_list.append({'feature_name':highest_feature_name,'training':score_book.loc[highest_feature_name,'training'],'blind-test':score_book.loc[highest_feature_name,'blind-test']},ignore_index=True)

            prev_diff = score_book.loc[highest_feature_name,'training'] - score_book.loc[highest_feature_name,'blind-test']

            greedy_features = pd.concat([greedy_features,training_features[highest_feature_name]],axis=1) # Build output_data containing eliminated feature
            training_features = training_features.drop(highest_feature_name, axis=1) # Remove the eliminated feature for further runs
            scored_feature = collections.OrderedDict() # Reset
            scatter_plot = scatter_plot.append({'feature_id': int(greedy_elim_feature_num), scoring :highest_feature_score}, ignore_index=True)
            total_score_book[highest_feature_name] = highest_feature_score
            get_terminal_size()
            plot(scatter_plot['feature_id'].tolist(), scatter_plot[scoring].tolist())

            greedy_elim_feature_num +=1

            if len(training_features.columns) == 0:
                stop_signal = True

            score_book = pd.DataFrame()
            scatter_plot.feature_id = scatter_plot.feature_id.astype(int)
            scatter_plot = scatter_plot[['feature_id',scoring]]
            total_feature_list.index +=1
            scatter_plot.to_csv(timestr+'_greedy_fe_'+ input_filename + '_' + algorithm +'_scatter_plot.csv',index=False)
            total_feature_list.to_csv(timestr+'_greedy_fe_'+ input_filename + '_' + algorithm +'_total_feature.csv',index=True)
        print("=====[4] Greedy-based eliminated features=====")
        print("{}".format(list(total_score_book))) # This dataframe shows features that satisfy the user given cut-off condition (def = 0.1).
        print("============================================")
    return True


if __name__ == '__main__':
    timestr = time.strftime("%Y%m%d_%H%M%S")
    parser = argparse.ArgumentParser(description='ex) python greedy_classifier.py M5P input.csv . dG bacc 10 -blindtest outertest.csv -cv_type group -group_col cluster_id')
    parser.add_argument("algorithm", help="Choose algorithm [GB,XGBOOST,RF,M5P,GAUSSIAN,ADABOOST,KNN,SVC,NEURAL]")
    parser.add_argument("input_csv", help="Choose input CSV(comma-separated values) format file")
    parser.add_argument("output_result_dir", help="Choose folder to save result(CSV)")
    parser.add_argument("label_name", help="Type the name of label")
    parser.add_argument("scoring", help="Choose one metric for greedy_feature selection ['roc_auc','matthew','bacc','f1']")
    parser.add_argument("n_cores", help="Choose the number of cores to use", type=int)
    parser.add_argument("-cutoff", help="Set cutoff value for the difference between training and blind-test performance, default=0.1", default=0.1, type=float)
    parser.add_argument("-blindtest", help="Choose input CSV(comma-separated values) format file",
                        default='False')    # optional

    parser.add_argument("-num_folds", help="Choose the number of folds", type=int, default=10)  # optional
    parser.add_argument("-cv_type", help="Choose the type of the cross-validation proceedure. Possible options are: ['random', 'group']", default='random')    # optional
    parser.add_argument("-group_col", help="Choose the name of the column representing the group for the training set split into k validation folds. Only available when chossing groupkfold or stratgroupkfold.", default=None)    # optional

    args = parser.parse_args()
    # required
    algorithm = args.algorithm
    training_csv = args.input_csv
    output_result_dir = args.output_result_dir
    label_name = args.label_name
    scoring = args.scoring
    n_cores = int(args.n_cores)
    # optional
    cutoff = args.cutoff
    blindtest = args.blindtest
    num_folds = args.num_folds
    cv_type = args.cv_type
    group_column = args.group_col

    if blindtest != 'False':
        outerblindtest_set = pd.read_csv(blindtest, quotechar='\"', header=0)
        outerblindtest_set = outerblindtest_set.fillna(False)

    else:
        outerblindtest_set = "False"

    if((cv_type != "random") and (cv_type != "group")):
        raise ValueError("cv_type should be 'random' or 'group'.")

    if((cv_type == "group") and (group_column == None)):
        raise ValueError("If you choose 'group' for cv_type, you must specify the group_col.")

    backwardGreedyFeature(training_csv, algorithm, output_result_dir, label_name, scoring, n_cores, outerblindtest_set, cutoff, num_folds, cv_type, group_column)
