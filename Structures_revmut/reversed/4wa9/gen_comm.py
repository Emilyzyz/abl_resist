


# -------------------------------------------------------------------------------------------------------------
# Using readlines()
file1 = open('4WA9_mutations_list.txt', 'r')
Lines = file1.readlines()

count = 0
# Strips the newline character
for line in Lines:
    print("python mutate_model.py 4wa9 B{} /4wa9/4wa9.pdb /4wa9/4wa9_{}.pdb 5".format(line.strip()[1:],line.strip()))
